#include "negator.hpp"

#include <cmath>

#include "connection/connectionmanager.hpp"

Negator::Negator() : Component(3, 3)
{
}

void Negator::resizeInputs(unsigned n)
{
	if (n == getInputSize())
		return;

	if (n < getInputSize())
		ConnectionManager::Instance().disconnectInputs(this, n, getInputSize());

	input_connections.resize(n, Connection());

	if (n == getOutputSize())
		return;

	if (n < getOutputSize())
		ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

	output_connections.resize(n, Connection());

	notifyStateChanged();
}

void Negator::resizeOutputs(unsigned /*unused*/)
{
	throw "Cannot resize outputs in negator";
}

void Negator::update()
{

	size_t inputs_size = this->input_connections.size();

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			for (auto &conn : this->output_connections)
				conn.setSignal(Signal(Signal::State::ERROR));
			return;
		}
	}

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::UNDEFINED) {
			for (auto &conn : this->output_connections)
				conn.setSignal(Signal(Signal::State::UNDEFINED));
			return;
		}
	}

	int operand = 0;

	int exponent = 0;
	for (size_t i = inputs_size - 1; i > 0; i--) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
			operand += std::pow(2, exponent);
		exponent += 1;
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE)
		operand = -operand;

	int result = -operand;

	std::vector<bool> bits = std::vector<bool>(this->output_connections.size());
	if (result >= 0) {
		bits[0] = false;
	} else {
		bits[0] = true;
	}

	for (size_t k = 0; k < this->output_connections.size() - 1; k++) {
		bits[this->output_connections.size() - k - 1] = ((result % 2) != 0);
		result /= 2;
	}

	for (size_t j = 0; j < this->output_connections.size(); j++) {
		this->output_connections[j].setSignal(bits[j] ? Signal::State::TRUE : Signal::State::FALSE);
	}
}

ComponentTypes Negator::getType() const
{
	return ComponentTypes::NEGATOR;
}
