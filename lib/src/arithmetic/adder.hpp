#ifndef ADDER_H
#define ADDER_H

#include "component/component.hpp"

class Adder : public Component
{

public:
	Adder();
	~Adder() override = default;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // ADDER_H
