#include "comparator.hpp"

#include <cmath>

#include "connection/connectionmanager.hpp"
// Class comparator which works for n inputs

Comparator::Comparator() : Component(4, 3)
{
}

void Comparator::resizeOutputs(unsigned /*unused*/)
{
    throw "Comparator must have three outputs!";
}

void Comparator::resizeInputs(unsigned n)
{

	if (n == getInputSize())
		return;

	if (n % 2 != 0) {
        throw "Comprator must have even number of inputs!";
	}

	if (n < getInputSize())
		ConnectionManager::Instance().disconnectInputs(this, n, getInputSize());

	input_connections.resize(n, Connection());
	notifyStateChanged();
}

void Comparator::update()
{

	size_t inputs_size = this->input_connections.size();
//  this should never happen
//	if (inputs_size % 2 != 0) {
//		for (auto &conn : this->output_connections)
//			conn.setSignal(Signal(Signal::State::ERROR));
//		return;
//	}

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			for (auto &conn : this->output_connections)
				conn.setSignal(Signal(Signal::State::ERROR));
			return;
		}
	}

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::UNDEFINED) {
			for (auto &conn : this->output_connections)
				conn.setSignal(Signal(Signal::State::UNDEFINED));
			return;
		}
	}

	int first_operand = 0;
	int second_operand = 0;

	for (size_t i = 1; i < inputs_size / 2; i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
			first_operand += std::pow(2, i - 1);
	}
	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE)
		first_operand = -first_operand;

	for (size_t i = inputs_size / 2 + 1; i < inputs_size; i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
			second_operand += std::pow(2, i - (inputs_size / 2 + 1));
	}
	if (this->input_connections[inputs_size / 2].getSignal().getState() == Signal::State::TRUE)
		second_operand = -second_operand;

	if (first_operand == second_operand) {
		this->output_connections[0].setSignal(Signal(Signal::State::FALSE));
		this->output_connections[1].setSignal(Signal(Signal::State::TRUE));
		this->output_connections[2].setSignal(Signal(Signal::State::FALSE));
		return;
	} else if (first_operand > second_operand) {
		this->output_connections[0].setSignal(Signal(Signal::State::TRUE));
		this->output_connections[1].setSignal(Signal(Signal::State::FALSE));
		this->output_connections[2].setSignal(Signal(Signal::State::FALSE));
		return;
	} else {
		this->output_connections[0].setSignal(Signal(Signal::State::FALSE));
		this->output_connections[1].setSignal(Signal(Signal::State::FALSE));
		this->output_connections[2].setSignal(Signal(Signal::State::TRUE));
	}
}

ComponentTypes Comparator::getType() const
{
	return ComponentTypes::COMPARATOR;
}
