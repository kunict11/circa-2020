#ifndef COMPONENT_H
#define COMPONENT_H

#include <functional>
#include <map>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

#include "componenttype.hpp"
#include "connection/connection.hpp"
#include "connection/connectionsignal.hpp"

class Component
{
public:
	static const unsigned OVERFLOW_THRESHOLD = 6;

	Component(unsigned n_input_slots, unsigned n_output_slots);

	virtual ~Component() = default;

	void notifyStateChanged();

	Signal getInput(unsigned);
	Signal getOutput(unsigned);
	void setInput(unsigned index, Signal::State state);

	size_t getInputSize();
	size_t getOutputSize();

	virtual void resizeInputs(unsigned);
	virtual void resizeOutputs(unsigned);

	void setId(int id);
	int getId() const;

	virtual ComponentTypes getType() const = 0;

	virtual void serialize(std::ostream &os) const;
	virtual void deserialize(std::istream &is, std::map<int, Component *> lookup);

	std::string short_identify();

	bool input_connection_taken(unsigned);
	bool output_connection_taken(unsigned);

	std::vector<Connection> &getInputConnections();
	std::vector<Connection> &getOutputConnections();

protected:
	virtual void update() = 0;

	void notifyStateChanged(std::unordered_map<Component *, unsigned> &);

	std::vector<Connection> input_connections;
	std::vector<Connection> output_connections;

private:
	bool propagateInputs();

	int m_id;
	unsigned m_nInputs;
	unsigned m_nOutputs;

	friend class ConnectionManager;
	friend class ComponentFactory;
};

#endif // COMPONENT_H
