#include "componenttype.hpp"

int ComponentType::toInt(ComponentTypes type)
{
	switch (type) {
	case ComponentTypes::PIN:
		return 0;
	case ComponentTypes::LIGHTBULB:
		return 1;
	case ComponentTypes::WIRE:
		return 2;
	case ComponentTypes::NOTGATE:
		return 3;
	case ComponentTypes::ANDGATE:
		return 4;
	case ComponentTypes::ORGATE:
		return 5;
	case ComponentTypes::XORGATE:
		return 6;
	case ComponentTypes::CONSTANT:
		return 7;
	case ComponentTypes::NANDGATE:
		return 8;
	case ComponentTypes::NORGATE:
		return 9;
	case ComponentTypes::XNORGATE:
		return 10;
	case ComponentTypes::BUFFER:
		return 11;
	case ComponentTypes::MULTIPLEXER:
		return 12;
	case ComponentTypes::DEMULTIPLEXER:
		return 13;
	case ComponentTypes::ENCODER:
		return 14;
	case ComponentTypes::DECODER:
		return 15;
	case ComponentTypes::COMPARATOR:
		return 16;
	case ComponentTypes::ADDER:
		return 17;
	case ComponentTypes::SUBTRACTOR:
		return 18;
	case ComponentTypes::MULTIPLIER:
		return 19;
	case ComponentTypes::DIVIDER:
		return 20;
	case ComponentTypes::NEGATOR:
		return 21;
	case ComponentTypes::SPLITTER:
		return 22;
	case ComponentTypes::SRFLIPFLOP:
		return 23;
	case ComponentTypes::JKFLIPFLOP:
		return 24;
	case ComponentTypes::TFLIPFLOP:
		return 25;
	case ComponentTypes::DFLIPFLOP:
		return 26;
	case ComponentTypes::COMPOSITE:
		return 27;
	case ComponentTypes::CLOCK:
		return 28;
    case ComponentTypes::ROM:
        return 29;
    case ComponentTypes::RAM:
        return 30;
	}
	throw "Unknown component";
}

ComponentTypes ComponentType::fromInt(int t)
{
	switch (t) {
	case 0:
		return ComponentTypes::PIN;
	case 1:
		return ComponentTypes::LIGHTBULB;
	case 2:
		return ComponentTypes::WIRE;
	case 3:
		return ComponentTypes::NOTGATE;
	case 4:
		return ComponentTypes::ANDGATE;
	case 5:
		return ComponentTypes::ORGATE;
	case 6:
		return ComponentTypes::XORGATE;
	case 7:
		return ComponentTypes::CONSTANT;
	case 8:
		return ComponentTypes::NANDGATE;
	case 9:
		return ComponentTypes::NORGATE;
	case 10:
		return ComponentTypes::XNORGATE;
	case 11:
		return ComponentTypes::BUFFER;
	case 12:
		return ComponentTypes::MULTIPLEXER;
	case 13:
		return ComponentTypes::DEMULTIPLEXER;
	case 14:
		return ComponentTypes::ENCODER;
	case 15:
		return ComponentTypes::DECODER;
	case 16:
		return ComponentTypes::COMPARATOR;
	case 17:
		return ComponentTypes::ADDER;
	case 18:
		return ComponentTypes::SUBTRACTOR;
	case 19:
		return ComponentTypes::MULTIPLIER;
	case 20:
		return ComponentTypes::DIVIDER;
	case 21:
		return ComponentTypes::NEGATOR;
	case 22:
		return ComponentTypes::SPLITTER;
	case 23:
		return ComponentTypes::SRFLIPFLOP;
	case 24:
		return ComponentTypes::JKFLIPFLOP;
	case 25:
		return ComponentTypes::TFLIPFLOP;
	case 26:
		return ComponentTypes::DFLIPFLOP;
	case 27:
		return ComponentTypes::COMPOSITE;
	case 28:
		return ComponentTypes::CLOCK;
    case 29:
        return ComponentTypes::ROM;
    case 30:
        return ComponentTypes::RAM;
	}
	throw "Unknown component";
}

void ComponentType::serialize(std::ostream &os, ComponentTypes type)
{
	int t = toInt(type);
	os.write((char *)(&t), sizeof(t));
}

ComponentTypes ComponentType::deserialize(std::istream &is)
{
	int t;
	is.read((char *)(&t), sizeof(t));
	return fromInt(t);
}
