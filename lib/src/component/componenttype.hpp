#ifndef COMPONENTTYPES_H
#define COMPONENTTYPES_H

#include <iostream>

enum class ComponentTypes {
	PIN,
	LIGHTBULB,
	WIRE,
	NOTGATE,
	ANDGATE,
	ORGATE,
	XORGATE,
	CONSTANT,
	NANDGATE,
	NORGATE,
	XNORGATE,
	BUFFER,
	MULTIPLEXER,
	DEMULTIPLEXER,
	ENCODER,
	DECODER,
	COMPARATOR,
	ADDER,
	SUBTRACTOR,
	MULTIPLIER,
	DIVIDER,
	NEGATOR,
	SPLITTER,
	SRFLIPFLOP,
	JKFLIPFLOP,
	TFLIPFLOP,
	DFLIPFLOP,
	COMPOSITE,
    CLOCK,
    ROM,
    RAM
};

class ComponentType
{
public:
	static int toInt(ComponentTypes type);
	static ComponentTypes fromInt(int t);
	static void serialize(std::ostream &os, ComponentTypes type);
	static ComponentTypes deserialize(std::istream &is);
};

#endif // COMPONENTTYPES_H
