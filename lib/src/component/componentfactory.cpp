#include "componentfactory.hpp"

ComponentFactory &ComponentFactory::instance()
{
	static ComponentFactory Instance;
	return Instance;
}

Component *ComponentFactory::make(ComponentTypes type)
{
	switch (type) {
	case ComponentTypes::PIN:
		return makePin();
	case ComponentTypes::LIGHTBULB:
		return makeLightBulb();
	case ComponentTypes::WIRE:
		return makeWire();
	case ComponentTypes::NOTGATE:
		return makeNotGate();
	case ComponentTypes::ANDGATE:
		return makeAndGate();
	case ComponentTypes::ORGATE:
		return makeOrGate();
	case ComponentTypes::XORGATE:
		return makeXorGate();
	case ComponentTypes::CONSTANT:
		return makeConstant(Signal::State::UNDEFINED);
	case ComponentTypes::NANDGATE:
		return makeNandGate();
	case ComponentTypes::NORGATE:
		return makeNorGate();
	case ComponentTypes::XNORGATE:
		return makeXnorGate();
	case ComponentTypes::BUFFER:
		return makeBuffer();
	case ComponentTypes::MULTIPLEXER:
		return makeMultiplexer();
	case ComponentTypes::DEMULTIPLEXER:
		return makeDemultiplexer();
	case ComponentTypes::ENCODER:
		return makeEncoder();
	case ComponentTypes::DECODER:
		return makeDecoder();
	case ComponentTypes::COMPARATOR:
		return makeComparator();
	case ComponentTypes::ADDER:
		return makeAdder();
	case ComponentTypes::SUBTRACTOR:
		return makeSubtractor();
	case ComponentTypes::MULTIPLIER:
		return makeMultiplier();
	case ComponentTypes::DIVIDER:
		return makeDivider();
	case ComponentTypes::NEGATOR:
		return makeNegator();
	case ComponentTypes::SPLITTER:
		return makeSplitter();
	case ComponentTypes::SRFLIPFLOP:
		return makeSRFlipFlop();
	case ComponentTypes::JKFLIPFLOP:
		return makeJKFlipFlop();
	case ComponentTypes::TFLIPFLOP:
		return makeTFlipFlop();
	case ComponentTypes::DFLIPFLOP:
		return makeDFlipFlop();
	case ComponentTypes::COMPOSITE:
		return makeComposite({}, {});
	case ComponentTypes::CLOCK:
		return makeClock();
    case ComponentTypes::ROM:
        return makeROM();
    case ComponentTypes::RAM:
        return makeRAM();
	}
	throw "Not implemented";
}

Pin *ComponentFactory::makePin(Signal::State s)
{
	return new Pin(s);
}

LightBulb *ComponentFactory::makeLightBulb()
{
	return new LightBulb();
}

Wire *ComponentFactory::makeWire()
{
	return new Wire();
}

AndGate *ComponentFactory::makeAndGate()
{
	return new AndGate();
}

OrGate *ComponentFactory::makeOrGate()
{
	return new OrGate();
}

XorGate *ComponentFactory::makeXorGate()
{
	return new XorGate();
}

NandGate *ComponentFactory::makeNandGate()
{
	return new NandGate();
}

NorGate *ComponentFactory::makeNorGate()
{
	return new NorGate();
}

XnorGate *ComponentFactory::makeXnorGate()
{
	return new XnorGate();
}

CompositeComponent *ComponentFactory::makeComposite(std::vector<Pin *> pins, std::vector<LightBulb *> lbs)
{
	auto *cc = new CompositeComponent(pins.size(), lbs.size());

	CopyCache cache;
	for (auto *p : pins) {
		deepCopy(p, cc, cache);
	}
	for (size_t i = 0; i < lbs.size(); i++) {
		cc->setLb(i, static_cast<LightBulb *>(deepCopy(lbs[i], cc, cache)));
	}
	return cc;
}

Constant *ComponentFactory::makeConstant(Signal::State s)
{
	return new Constant(s);
}

Buffer *ComponentFactory::makeBuffer()
{
	return new Buffer();
}

NotGate *ComponentFactory::makeNotGate()
{
	return new NotGate();
}

Multiplexer *ComponentFactory::makeMultiplexer()
{
	return new Multiplexer();
}

Demultiplexer *ComponentFactory::makeDemultiplexer()
{
	return new Demultiplexer();
}

Encoder *ComponentFactory::makeEncoder()
{
	return new Encoder();
}

Decoder *ComponentFactory::makeDecoder()
{
	return new Decoder();
}

Comparator *ComponentFactory::makeComparator()
{
	return new Comparator();
}

Adder *ComponentFactory::makeAdder()
{
	return new Adder();
}

Subtractor *ComponentFactory::makeSubtractor()
{
	return new Subtractor();
}

Multiplier *ComponentFactory::makeMultiplier()
{
	return new Multiplier();
}

Divider *ComponentFactory::makeDivider()
{
	return new Divider();
}

Negator *ComponentFactory::makeNegator()
{
	return new Negator();
}

Splitter *ComponentFactory::makeSplitter()
{
	return new Splitter();
}

SRFlipFlop *ComponentFactory::makeSRFlipFlop()
{
	return new SRFlipFlop();
}

JKFlipFlop *ComponentFactory::makeJKFlipFlop()
{
	return new JKFlipFlop();
}

TFlipFlop *ComponentFactory::makeTFlipFlop()
{
	return new TFlipFlop();
}

DFlipFlop *ComponentFactory::makeDFlipFlop()
{
	return new DFlipFlop();
}

ROM *ComponentFactory::makeROM()
{
    return new ROM();
}

RAM *ComponentFactory::makeRAM()
{
    return new RAM();
}
Clock *ComponentFactory::makeClock()
{
	return new Clock();
}

/*
 * Question: Doesn't shallowCopy do the exact same thing as the factory methods?
 * Answer:   For now it (almost) does, but, when we introduce GUI, the shallowCopy
 *           should also copy everything concerning the GUI (locations and such).
 *           Also for most components it needs to check the number of inputs/outputs
 *           and mimic that as well (not sure how resize works now)
 */
Component *ComponentFactory::shallowCopy(Component *c)
{
	switch (c->getType()) {
	case ComponentTypes::PIN:
		return makePin(static_cast<Pin *>(c)->getState());
	case ComponentTypes::LIGHTBULB:
		return makeLightBulb();
	case ComponentTypes::WIRE:
		return makeWire();
	case ComponentTypes::CONSTANT:
		return makeConstant(static_cast<Constant *>(c)->output_connections[0].getSignal().getState());
	case ComponentTypes::CLOCK:
		return makeClock();
	case ComponentTypes::BUFFER:
		return makeBuffer();
	case ComponentTypes::ANDGATE:
		return makeAndGate();
	case ComponentTypes::ORGATE:
		return makeOrGate();
	case ComponentTypes::NOTGATE:
		return makeNotGate();
	case ComponentTypes::XORGATE:
		return makeXorGate();
	case ComponentTypes::NANDGATE:
		return makeNandGate();
	case ComponentTypes::NORGATE:
		return makeNorGate();
	case ComponentTypes::XNORGATE:
		return makeXnorGate();
	case ComponentTypes::MULTIPLEXER:
		return makeMultiplexer();
	case ComponentTypes::DEMULTIPLEXER:
		return makeDemultiplexer();
	case ComponentTypes::ENCODER:
		return makeEncoder();
	case ComponentTypes::DECODER:
		return makeDecoder();
	case ComponentTypes::COMPARATOR:
		return makeComparator();
	case ComponentTypes::ADDER:
		return makeAdder();
	case ComponentTypes::SUBTRACTOR:
		return makeSubtractor();
	case ComponentTypes::MULTIPLIER:
		return makeMultiplier();
	case ComponentTypes::DIVIDER:
		return makeDivider();
	case ComponentTypes::NEGATOR:
		return makeNegator();
	case ComponentTypes::SPLITTER:
		return makeSplitter();
	case ComponentTypes::SRFLIPFLOP:
		return makeSRFlipFlop();
	case ComponentTypes::JKFLIPFLOP:
		return makeJKFlipFlop();
	case ComponentTypes::TFLIPFLOP:
		return makeTFlipFlop();
	case ComponentTypes::DFLIPFLOP:
		return makeDFlipFlop();
    case ComponentTypes::ROM:
        return makeROM();
    case ComponentTypes::RAM:
        return makeRAM();
	case ComponentTypes::COMPOSITE:
		auto *cc = static_cast<CompositeComponent *>(c);
		auto *newcc = makeComposite(cc->pins, cc->lbs);
		return newcc;
	}
	throw "Unknown component in shallow_copy! (have you created a new built-in component?)";
}

// TODO: Maybe make static functions for the lambdas passed here
//      so that they don't get created every time
Component *ComponentFactory::deepCopy(Component *component, CircuitBoard *into, CopyCache &cache)
{
	return deepCopy(component, into, cache, [](auto /*unused*/) { return true; });
}

/**
 * @brief ComponentFactory::deepCopy
 * @param component - The component being copied
 * @param into      - Where it should be copied
 *                  - There are two scenarios:
 *                  -   (1) The user selects some components on the board
 *                  -       and wants them copied (somewhere on the screen).
 *                  -       In this case the `into` parameter
 *                  -       should be the circuit board.
 *                  -   (2) The user wants to make a composite component.
 *                  -       In this case the `into` parameter should be
 *                  -       a composite component (one created by makeComposite).
 * @param cache     - The components that are already copied, associated with their copies.
 * @param should_copy
 *                  - An outisde predicate (apart from cache) about whether a component should be copied at all.
 * @param new_copy_callback
 * @return          - A callback for a freshly created (shallow) copy
 */

Component *ComponentFactory::deepCopy(
	Component *component, CircuitBoard *into, CopyCache &cache, ShouldCopyPredicate should_copy)
{

	auto it = cache.find(component);
	if (it != cache.end()) {
		return it->second;
	}
	// TODO Add .copy to component instead of this
	auto *new_component = shallowCopy(component);
	try {
		if (component->getType() == ComponentTypes::MULTIPLEXER) {
			auto *mux = static_cast<Multiplexer *>(component);
			new_component->resizeInputs(component->getInputSize() - mux->m_num_of_select_inputs);
		} else if (component->getType() == ComponentTypes::DEMULTIPLEXER) {
			new_component->resizeInputs(component->getInputSize() - 2);
		} else {
			new_component->resizeInputs(component->getInputSize());
			new_component->resizeOutputs(component->getOutputSize());
		}
	} catch (char const *c) {
	}

	if (into != nullptr) {
		into->add(new_component);
	}
	cache[component] = new_component;
	for (unsigned i = 0; i < component->getInputSize(); i++) {
		auto conn = component->input_connections[i];
		if (!conn || !should_copy(conn.to_component)) {
			new_component->input_connections[i].connected = false;
			continue;
		}
		deepCopy(conn.to_component, into, cache, should_copy);
	}
	for (unsigned i = 0; i < component->getOutputSize(); i++) {
		auto conn = component->output_connections[i];
		if (!conn || !should_copy(conn.to_component)) {
			new_component->output_connections[i].connected = false;
			continue;
		}

		auto *nc = deepCopy(conn.to_component, into, cache, should_copy);

		ConnectionManager::Instance().connect(new_component, i, nc, conn.to_index);
	}
	return new_component;
}
