#ifndef XNORGATE_H
#define XNORGATE_H
#include "gate.hpp"

class XnorGate : public Gate
{
public:
	XnorGate(unsigned nInputs = 2);
	~XnorGate() override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // XNORGATE_H
