#include "nandgate.hpp"

NandGate::NandGate(unsigned nInputs) : Gate(nInputs)
{
}

NandGate::~NandGate() = default;

Signal NandGate::op()
{
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::ERROR)
			return Signal(Signal::State::ERROR);
	}

	bool has_undefined = false;
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::FALSE)
			return Signal(Signal::State::TRUE);

		if (input.getSignal().getState() == Signal::State::UNDEFINED)
			has_undefined = true;
	}
	if (has_undefined)
		return Signal(Signal::State::UNDEFINED);

	return Signal(Signal::State::FALSE);
}

ComponentTypes NandGate::getType() const
{
	return ComponentTypes::NANDGATE;
}
