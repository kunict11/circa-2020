#ifndef GATE_H
#define GATE_H

#include "component/component.hpp"

class Gate : public Component
{
public:
	Gate(unsigned n_input_slots);
	~Gate() override;

	void update() override;
	virtual Signal op() = 0;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;
};

#endif // GATE_H
