#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include <string>
#include <vector>

class StringUtil
{
public:
	static std::string join(const std::vector<std::string> &v, const char *delim);
	static std::string parenthesize_if(const std::string &s, bool condition);

private:
	StringUtil();
};

#endif // STRINGUTIL_H
