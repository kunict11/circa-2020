#ifndef NOTNODE_H
#define NOTNODE_H

#include "opnode.hpp"

class NotNode : public OpNode
{
public:
	NotNode(OpNode *input);
	~NotNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	OpNode *getInput() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	OpNode *m_input;
};

#endif // NOTNODE_H
