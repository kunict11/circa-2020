#include "xornode.hpp"

#include <algorithm>
#include <sstream>
#include <utility>

#include "constantnode.hpp"
#include "notnode.hpp"
#include "util/stringutil.hpp"

XorNode::XorNode(std::vector<OpNode *> inputs) : m_inputs(std::move(inputs))
{
}

XorNode::~XorNode()
{
	for (auto *node : m_inputs)
		delete node;
}

std::string XorNode::toString() const
{
	std::vector<std::string> input_formulae;
	for (auto *node : m_inputs) {
		input_formulae.push_back(
			StringUtil::parenthesize_if(node->toString(), getPriority(node->getType()) > getPriority(getType())));
	}
	return StringUtil::join(input_formulae, " ^ ");
}

OpNode *XorNode::simplify()
{
	int ones = 0;
	std::vector<OpNode *> simplified_inputs;
	for (auto *node : m_inputs) {
		OpNode *simplified = node->simplify();
		// Discard all constants
		if (simplified->getType() == OpType::CONSTANT) {
			auto *n = static_cast<ConstantNode *>(simplified);
			if (n->getValue())
				ones++;
			delete simplified;
			continue;
		} else if (simplified->getType() == OpType::AND) {
			// join all nested and gates
			auto *n = static_cast<XorNode *>(simplified);
			for (auto *subnode : n->getInputs())
				simplified_inputs.push_back(subnode->simplify());
			delete simplified;
		} else {
			simplified_inputs.push_back(simplified);
		}
	}

	if (simplified_inputs.empty()) {
		return new ConstantNode(ones % 2 == 1);
	} else if (simplified_inputs.size() == 1) {
		if (ones % 2 == 1)
			return new NotNode(simplified_inputs[0]);
		else
			return simplified_inputs[0];
	}

	// check A ^ A
	std::vector<bool> duplicate(simplified_inputs.size(), false);
	std::vector<OpNode *> filtered_inputs;
	for (unsigned i = 0; i < simplified_inputs.size(); i++) {
		if (duplicate[i])
			continue;
		bool found = false;
		for (unsigned j = 0; j < simplified_inputs.size(); j++) {
			if (!duplicate[j] && i != j && equals(simplified_inputs[i], simplified_inputs[j])) {
				duplicate[i] = true;
				duplicate[j] = true;
				found = true;
				break;
			}
		}
		if (found)
			break;
	}
	for (unsigned i = 0; i < simplified_inputs.size(); i++) {
		if (duplicate[i])
			delete simplified_inputs[i];
		else
			filtered_inputs.push_back(simplified_inputs[i]);
	}

	if (filtered_inputs.empty()) {
		return new ConstantNode(false);
	}

	// If length is 1 there is no need for a gate
	if (filtered_inputs.size() == 1)
		return filtered_inputs[0];
	return new XorNode(filtered_inputs);
}

std::vector<OpNode *> XorNode::getInputs() const
{
	return m_inputs;
}

OpType XorNode::getType() const
{
	return OpType::XOR;
}

bool XorNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<XorNode *>(other);

	// Only works if there are not duplicates
	// But it's called only on simplified and gates for which this is guaranteed
	if (m_inputs.size() != node->getInputs().size())
		return false;
	for (auto *input : m_inputs) {
		bool found = false;
		for (unsigned i = 0; i < node->getInputs().size(); i++) {
			if (equals(input, node->getInputs()[i])) {
				found = true;
				break;
			}
		}
		if (!found)
			return false;
	}
	return true;
}
