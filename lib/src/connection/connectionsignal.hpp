#ifndef SIGNAL_H
#define SIGNAL_H

#include <iostream>

class Signal
{
public:
	enum class State { TRUE, FALSE, ERROR, UNDEFINED };

	Signal(State value = State::UNDEFINED);
    Signal(const Signal &s);
	Signal &operator=(const Signal &other);

	~Signal() = default;

	State getState() const;

	bool operator==(const Signal &s) const;

	bool operator!=(const Signal &s) const;

	Signal operator!() const;

	operator Signal::State() const
	{
		return this->getState();
	}

	friend std::ostream &operator<<(std::ostream &os, const Signal &s);

	static int toInt(Signal::State state);
	static Signal::State fromInt(int s);

	void serialize(std::ostream &os) const;
	void deserialize(std::istream &is);

private:
	State m_state;
};

#endif // SIGNAL_H
