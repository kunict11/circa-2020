#include "decoder.hpp"

#include "connection/connectionmanager.hpp"

Decoder::Decoder() : Component(1, 2)
{
}

Decoder::~Decoder() = default;

void Decoder::resizeOutputs(unsigned /*unused*/)
{
	throw "Cannot resize outputs in decoder!";
}

void Decoder::resizeInputs(unsigned n)
{
	if (n == getInputSize())
		return;

	if (n < getInputSize())
		ConnectionManager::Instance().disconnectInputs(this, n, getInputSize());

	input_connections.resize(n, Connection());

    n = std::pow(2, n);
//  this shouldn't happpen because that would mean that the new input size is equal
//  to the current input size, which was covered by the first case
//	if (n == getOutputSize())
//		return;

	if (n < getOutputSize())
		ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

	output_connections.resize(n, Connection());

	notifyStateChanged();
}

void Decoder::update()
{
	int index = 0;
	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			for (auto &conn_o : this->output_connections) {
				conn_o.setSignal(Signal(Signal::State::ERROR));
			}
			return;
		}
	}

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::UNDEFINED) {
			for (auto &conn_o : this->output_connections) {
				conn_o.setSignal(Signal(Signal::State::UNDEFINED));
			}
			return;
		}
	}

	for (size_t i = 0; i < this->input_connections.size(); i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE) {
			index += std::pow(2, i);
		}
	}

	for (size_t i = 0; i < this->output_connections.size(); i++) {
		if (int(i) == index) {
			this->output_connections[i].setSignal(Signal(Signal::State::TRUE));
		} else {
			this->output_connections[i].setSignal(Signal(Signal::State::FALSE));
		}
	}
}

ComponentTypes Decoder::getType() const
{
	return ComponentTypes::DECODER;
}
