#include "demultiplexer.hpp"

#include <cmath>

#include "connection/connectionmanager.hpp"

Demultiplexer::Demultiplexer() : Component(3, 2)
{
	m_num_of_select_inputs = 1;
}

Demultiplexer::~Demultiplexer() = default;

void Demultiplexer::resizeOutputs(unsigned /*unused*/)
{
	throw "Cannot resize outputs in demultiplexer!";
}

void Demultiplexer::resizeInputs(unsigned n)
{
    unsigned inputs = this->getInputSize();

    if (n == inputs || n == 2)
		return;

    if (n < inputs) {
        ConnectionManager::Instance().moveInput(this, inputs-1, n-1, false);
        ConnectionManager::Instance().disconnectInputs(this, n, this->getInputSize());

        this->input_connections.resize(n);
        m_num_of_select_inputs = n-2;

        n = std::pow(2, n-2);

		if (n == this->getOutputSize())
			return;

		if (n < this->getOutputSize())
			ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

		output_connections.resize(n, Connection());

		this->notifyStateChanged();
		return;
	}

    if (n > inputs) {
        this->input_connections.resize(n);
        ConnectionManager::Instance().moveInput(this, inputs-1, n-1, false);
        ConnectionManager::Instance().disconnectInput(this, inputs-1);
        m_num_of_select_inputs = n-2;

        n = std::pow(2, n-2);
		if (n == getOutputSize())
			return;

		if (n < getOutputSize())
			ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

		output_connections.resize(n, Connection());

		this->notifyStateChanged();
	}
}

void Demultiplexer::update()
{

    // If enable input is ERROR then all outputs are ERROR
	if (this->input_connections[getInputSize() - 1].getSignal().getState() == Signal::State::ERROR) {
		for (auto &conn : this->output_connections) {
			conn.setSignal(Signal(Signal::State::ERROR));
		}
		return;
	}

	// If enable input is TRUE or UNDEFINED (this is how Logisim works)
	if (this->input_connections[getInputSize() - 1].getSignal().getState() == Signal::State::TRUE ||
		this->input_connections[getInputSize() - 1].getSignal().getState() == Signal::State::UNDEFINED) {

        for (size_t i = 0; i < getInputSize() - 1; i++) {
			if (this->input_connections[i].getSignal().getState() == Signal::State::ERROR) {
				for (auto &conn : this->output_connections) {
					conn.setSignal(Signal(Signal::State::ERROR));
				}
				return;
			}
		}

        for (size_t i = 0; i < getInputSize() - 1; i++) {
			if (this->input_connections[i].getSignal().getState() == Signal::State::UNDEFINED) {
				for (auto &conn : this->output_connections) {
					conn.setSignal(Signal(Signal::State::UNDEFINED));
				}
				return;
			}
		}

        if(input_connections[0].getSignal().getState() == Signal::State::FALSE) {
            for (auto &conn : this->output_connections) {
                conn.setSignal(Signal(Signal::State::FALSE));
            }
            return;
        }

		unsigned output = 0;
        for (size_t i = 1; i < getInputSize() - 1; i++) {
			if (input_connections[i].getSignal().getState() == Signal::State::TRUE) {
                output += std::pow(2, getInputSize() - i-2);
			}
		}

		for (size_t i = 0; i < getOutputSize(); i++) {
			if (i == output) {
                output_connections[i].setSignal(Signal::State::TRUE);
			} else {
				output_connections[i].setSignal(Signal::State::FALSE);
			}
		}
	} else {
		for (auto &conn : this->output_connections) {
			conn.setSignal(Signal(Signal::State::UNDEFINED));
		}
	}
}

ComponentTypes Demultiplexer::getType() const
{
	return ComponentTypes::DEMULTIPLEXER;
}
