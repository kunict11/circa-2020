#include "encoder.hpp"

#include <cmath>

#include "connection/connectionmanager.hpp"

Encoder::Encoder() : Component(2, 1)
{
}

Encoder::~Encoder() = default;

void Encoder::resizeInputs(unsigned n)
{
	if (n == getInputSize())
		return;

	if (n < getInputSize())
		ConnectionManager::Instance().disconnectInputs(this, n, getInputSize());

	input_connections.resize(n, Connection());

	n = std::ceil(std::log2(n));

	if (n < getOutputSize())
		ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

	output_connections.resize(n, Connection());

	notifyStateChanged();
}

void Encoder::resizeOutputs(unsigned /*unused*/)
{
	throw "Cannot resize outputs in encoder!";
}

void Encoder::update()
{
	int num = 0;  // Number of true values in inputs
	int pos = -1; // Position of single true value

    // We can't have more then one true value among inputs
	for (size_t i = 0; i < this->input_connections.size(); i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE) {
			num++;
			pos = i;
		}
	}
	if (num > 1) {
		for (auto &conn : this->output_connections) {
			conn.setSignal(Signal(Signal::State::ERROR));
		}
		return;
	}
	if (num < 1) {
		for (auto &conn : this->output_connections) {
			conn.setSignal(Signal(Signal::State::UNDEFINED));
		}
		return;
	}

	// Translate index of true value into binary representation
	std::vector<bool> bits = std::vector<bool>(this->output_connections.size());
	for (size_t k = 0; k < this->output_connections.size(); k++) {
        bits[k] = pos % 2 == 1;
		pos /= 2;
	}
	// Propagate that representation to outputs
	for (size_t j = 0; j < this->output_connections.size(); j++) {
		this->output_connections[j].setSignal(bits[j] ? Signal::State::TRUE : Signal::State::FALSE);
	}
}

ComponentTypes Encoder::getType() const
{
	return ComponentTypes::ENCODER;
}
