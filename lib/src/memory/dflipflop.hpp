#ifndef DFLIPFLOP_H
#define DFLIPFLOP_H

#include "component/component.hpp"

class DFlipFlop : public Component
{
public:
	DFlipFlop();
	~DFlipFlop() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // DFLIPFLOP_H
