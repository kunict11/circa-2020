#include "ram.hpp"

/*!
 * \fn Initialize the RAM component with 2 data inputs + 4  control inputs (select, clock, load,  clear)
 *
*/

RAM::RAM() : Component(6, 1)
{
    rowCount = 2;
    colCount = 2;
    contents.resize(std::pow(2,rowCount));
    resetContents();
}

RAM::~RAM() = default;

void RAM::resizeOutputs(unsigned n)
{
    if(n != 1)
        throw "Resizing outputs is not allowed";
}

std::vector<Signal> &RAM::getContents()
{
    return contents;
}

void RAM::resizeInputs(unsigned n)
{
    if (n > 8)
        throw "Memory size too large";
    if (n < 2)
        throw "At least two inputs are required.";
    Component::resizeInputs(n+4);

    if (n == 7) {
        rowCount = 8;
        colCount = 16;
    } else {
        rowCount = std::sqrt(pow(2, n)) - std::floor(std::sqrt(pow(2, n))) == 0
                    ? std::sqrt(pow(2, n))
                    : std::pow(2, std::floor(std::log2(n)));
        colCount = std::pow(2, n)/rowCount;
    }
    auto previous_size = contents.size();
    auto new_size = std::pow(2,n);
    contents.resize(new_size);
    if(previous_size < new_size) {
        std::fill(std::begin(contents)+previous_size, std::end(contents), Signal(Signal::State::FALSE));
    }
}

void RAM::update()
{
    unsigned n = input_connections.size();
    const unsigned SELECT_INPUT_INDEX = n - 4;
    const unsigned CLOCK_INPUT_INDEX  = n - 3;
    const unsigned LOAD_INPUT_INDEX   = n - 2;
    const unsigned RESET_INPUT_INDEX  = n - 1;

    if (input_connections[RESET_INPUT_INDEX].getSignal().getState() == Signal::State::TRUE)
        resetContents();

    if(std::any_of(std::cbegin(input_connections),
                   std::cend(input_connections),
                   [](const auto& conn){
                    return conn.getSignal().getState() == Signal::State::ERROR;
                   })
       ){
        output_connections[0].setSignal(Signal::State::ERROR);
        return;
    }

    if(std::any_of(std::cbegin(input_connections),
                   std::cend(input_connections),
                   [](const auto& conn){
                    return conn.getSignal().getState() == Signal::State::UNDEFINED;
                   })
       || input_connections[SELECT_INPUT_INDEX].getSignal().getState() == Signal::State::FALSE
       ){
        output_connections[0].setSignal(Signal::State::UNDEFINED);
        return;
    }

    if (input_connections[SELECT_INPUT_INDEX].getSignal().getState() == Signal::State::TRUE) {
        unsigned selectedPos = calculateSelectedPosition();
        if (input_connections[LOAD_INPUT_INDEX].getSignal().getState() == Signal::State::TRUE) {
            output_connections[0].setSignal(contents[selectedPos].getState());
        }
        else if (input_connections[CLOCK_INPUT_INDEX].getSignal().getState() == Signal::State::TRUE
                 && contents[selectedPos].getState() == Signal::State::FALSE) {
            updateContents(selectedPos);
        }
    }
    return;
}

unsigned RAM::calculateSelectedPosition()
{
    unsigned n = input_connections.size() - 4;
    unsigned pos = 0;
    for(unsigned i=0; i<n; i++){
        if(input_connections[i].getSignal().getState() == Signal::State::TRUE)
            pos += pow(2, i);
    }
    return pos;
}

void RAM::updateContents(unsigned pos)
{
    contents[pos] = Signal::State::TRUE;
}

void RAM::resetContents()
{
    std::fill(std::begin(contents), std::end(contents), Signal(Signal::State::FALSE));
}

ComponentTypes RAM::getType() const
{
    return ComponentTypes::RAM;
}

unsigned const RAM::getColCount()
{
    return colCount;
}

unsigned const RAM::getRowCount()
{
    return rowCount;
}

void RAM::serializeContents(std::ostream &os)
{
    size_t contentsSize = contents.size();
    os.write((char *)(&contentsSize), sizeof(contentsSize));

    for(const auto &signal : contents) {
        signal.serialize(os);
    }
}

void RAM::deserializeContents(std::istream &is)
{
    size_t contentsSize;
    is.read((char *)(&contentsSize), sizeof(contentsSize));
    contents.resize(contentsSize);
    for(int i=0; i<contentsSize; i++) {
        contents[i].deserialize(is);
    }
}
