#include "dflipflop.hpp"

DFlipFlop::DFlipFlop() : Component(4, 2)
{
	output_connections[0].setSignal(Signal::State::FALSE);
	output_connections[1].setSignal(Signal::State::TRUE);
}

DFlipFlop::~DFlipFlop() = default;

void DFlipFlop::resizeInputs(unsigned n)
{
	if (n != 4)
		throw "Resize inputs for D flip-flop is not allowed!";
}

void DFlipFlop::resizeOutputs(unsigned n)
{
    if (n != 2)
        throw "Resize outputs for D flip-flop is not allowed!";
}

void DFlipFlop::update()
{
    unsigned SET_INDEX = 0;
    unsigned RESET_INDEX = 1;
    unsigned CLOCK_INDEX = 2;
	for (auto &conn : input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
            output_connections[SET_INDEX].setSignal(Signal::State::ERROR);
            output_connections[RESET_INDEX].setSignal(Signal::State::ERROR);
			return;
		}
	}

    if (input_connections[SET_INDEX].getSignal().getState() == Signal::State::FALSE &&
        input_connections[RESET_INDEX].getSignal().getState() == Signal::State::TRUE) {
		output_connections[0].setSignal(Signal::State::FALSE);
		output_connections[1].setSignal(Signal::State::TRUE);
		return;
	}

    if (input_connections[SET_INDEX].getSignal().getState() == Signal::State::TRUE &&
        input_connections[RESET_INDEX].getSignal().getState() == Signal::State::FALSE) {
		output_connections[0].setSignal(Signal::State::TRUE);
		output_connections[1].setSignal(Signal::State::FALSE);
		return;
	}

    if (input_connections[SET_INDEX].getSignal().getState() == Signal::State::TRUE &&
        input_connections[RESET_INDEX].getSignal().getState() == Signal::State::TRUE) {
		output_connections[0].setSignal(Signal::State::TRUE);
		output_connections[1].setSignal(Signal::State::TRUE);
		return;
	}

    if ((input_connections[SET_INDEX].getSignal().getState() == Signal::State::FALSE &&
         input_connections[RESET_INDEX].getSignal().getState() == Signal::State::FALSE) ||
        input_connections[SET_INDEX].getSignal().getState() == Signal::State::UNDEFINED ||
        input_connections[RESET_INDEX].getSignal().getState() == Signal::State::UNDEFINED) {

        if (input_connections[3].getSignal().getState() == Signal::State::FALSE)
			return;

        if (input_connections[3].getSignal().getState() == Signal::State::TRUE) {
            output_connections[0].setSignal(input_connections[CLOCK_INDEX].getSignal());
            output_connections[1].setSignal(!input_connections[CLOCK_INDEX].getSignal());
		}
	}
}

ComponentTypes DFlipFlop::getType() const
{
	return ComponentTypes::DFLIPFLOP;
}
