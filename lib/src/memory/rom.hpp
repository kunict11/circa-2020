#ifndef ROM_HPP
#define ROM_HPP

#include "component/component.hpp"
#include <cmath>
#include <algorithm>

class ROM : public Component
{
public:
    ROM();
    ~ROM() override;

    void resizeInputs(unsigned n) override;
    void resizeOutputs(unsigned n) override;

    std::vector<Signal>& getContents();
    void updateContents(unsigned pos);
    const unsigned getRowCount();
    const unsigned getColCount();

    void serializeContents(std::ostream &os);
    void deserializeContents(std::istream &is);

    ComponentTypes getType() const override;

protected:
    void update() override;

private:
    unsigned rowCount;
    unsigned colCount;
    std::vector<Signal> contents;
    unsigned calculateSelectedPosition();
};

#endif // ROM_HPP
