#ifndef RAM_HPP
#define RAM_HPP

#include "component/component.hpp"
#include <cmath>
#include <algorithm>
class RAM : public Component
{
public:
    RAM();
    ~RAM() override;

    void resizeInputs(unsigned n) override;
    void resizeOutputs(unsigned n) override;

    std::vector<Signal>& getContents();
    const unsigned getRowCount();
    const unsigned getColCount();

    void serializeContents(std::ostream &os);
    void deserializeContents(std::istream &is);

    ComponentTypes getType() const override;

protected:
    void update() override;

private:
    unsigned rowCount;
    unsigned colCount;
    std::vector<Signal> contents;
    unsigned calculateSelectedPosition();
    void updateContents(unsigned pos);
    void resetContents();
};

#endif // RAM_HPP
