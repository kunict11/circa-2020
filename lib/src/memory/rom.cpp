#include "rom.hpp"

ROM::ROM() : Component(3, 1)
{
    this->rowCount = 2;
    this->colCount = 2;
    this->contents.resize(std::pow(2,this->rowCount));
    std::fill(std::begin(contents), std::end(contents), Signal(Signal::State::FALSE));
}

ROM::~ROM() = default;

void ROM::resizeInputs(unsigned n)
{
    if (n > 8)
        throw "Memory size too large";
    if (n < 2)
        throw "At least two inputs are required.";
    Component::resizeInputs(n);

    if (n == 8) { // hardcoded for now
        rowCount = 8;
        colCount = 16;
    } else {
        this->rowCount = std::sqrt(pow(2, n-1)) - std::floor(std::sqrt(pow(2, n-1))) == 0.0
                            ? std::sqrt(pow(2, n-1))
                            : std::pow(2, std::floor(std::log2(n-1)));
        this->colCount = std::pow(2, n-1)/this->rowCount;
    }
    auto previous_size = contents.size();
    auto new_size = std::pow(2,n-1);
    this->contents.resize(new_size);
    if(previous_size < new_size) {
        std::fill(std::begin(this->contents)+previous_size, std::end(this->contents), Signal(Signal::State::FALSE));
    }
}

void ROM::resizeOutputs(unsigned n)
{
    if(n != 1)
        throw "Resizing outputs is not allowed";
}

void ROM::update()
{
    unsigned n = this->input_connections.size();

    if(std::any_of(std::cbegin(this->input_connections),
                   std::cend(this->input_connections),
                   [](const auto& conn){
                    return conn.getSignal().getState() == Signal::State::ERROR;
                   })
       ){
        output_connections[0].setSignal(Signal::State::ERROR);
        return;
    }

    if(std::any_of(std::cbegin(this->input_connections),
                   std::cend(this->input_connections),
                   [](const auto& conn){
                    return conn.getSignal().getState() == Signal::State::UNDEFINED;
                   })
       || input_connections[n-1].getSignal().getState() == Signal::State::FALSE
       ){
        output_connections[0].setSignal(Signal::State::UNDEFINED);
        return;
    }

    unsigned selectedPos = calculateSelectedPosition();
    output_connections[0].setSignal(this->contents[selectedPos].getState());
    return;
}

std::vector<Signal>& ROM::getContents()
{
    return this->contents;
}

const unsigned ROM::getRowCount()
{
    return rowCount;
}

const unsigned ROM::getColCount()
{
    return colCount;
}

void ROM::updateContents(unsigned pos)
{
    this->contents[pos] = !this->contents[pos];
    return;
}

unsigned ROM::calculateSelectedPosition()
{
    unsigned n = this->input_connections.size()-1;
    unsigned pos = 0;
    for(unsigned i=0; i<n; i++){
        if(this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
            pos += pow(2, i);
    }
    return pos;
}

ComponentTypes ROM::getType() const
{
    return ComponentTypes::ROM;
}

void ROM::serializeContents(std::ostream &os)
{
    size_t contentsSize = contents.size();
    os.write((char *)(&contentsSize), sizeof(contentsSize));

    for(const auto &signal : contents) {
        signal.serialize(os);
    }
}

void ROM::deserializeContents(std::istream &is)
{
    size_t contentsSize;
    is.read((char *)(&contentsSize), sizeof(contentsSize));
    contents.resize(contentsSize);
    for(int i=0; i<contentsSize; i++) {
        contents[i].deserialize(is);
    }
}

