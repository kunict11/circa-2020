#include "pin.hpp"

Pin::Pin(const Signal::State &state) : Component(0, 1), m_state(state)
{
	this->output_connections[0].setSignal(m_state);
}

Pin::~Pin() = default;

Pin::Pin() : Pin(Signal::State::UNDEFINED)
{
}

void Pin::resizeInputs(unsigned /*unused*/)
{
	throw "Pin has no inputs!";
}

void Pin::resizeOutputs(unsigned /*unused*/)
{
	throw "Pin must have just one output!";
}

void Pin::flip()
{
	setState(!Signal(m_state));
}

void Pin::setState(Signal::State s)
{
	m_state = s;
	auto &conn = output_connections[0];
	conn.setSignal(s);
	if (conn.hasChanged()) {
		if (conn)
			conn.to_component->notifyStateChanged();
		conn.flush();
	}
}

void Pin::update()
{
	this->output_connections[0].setSignal(m_state);
}

Signal::State Pin::getState()
{
	return m_state;
}

void Pin::serialize(std::ostream &os) const
{
	Component::serialize(os);
	int s = Signal::toInt(m_state);
	os.write((char *)(&s), sizeof(s));
}

void Pin::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	Component::deserialize(is, lookup);
	int s;
	is.read((char *)(&s), sizeof(s));
	m_state = Signal::fromInt(s);
}

ComponentTypes Pin::getType() const
{
	return ComponentTypes::PIN;
}
