#include "clock.hpp"

Clock::Clock(const Signal::State &state) : Component(0, 1), m_state(state)
{
	this->output_connections[0].setSignal(m_state);
}

Clock::~Clock() = default;

Clock::Clock() : Clock(Signal::State::FALSE)
{
}

void Clock::resizeInputs(unsigned /*unused*/)
{
	throw "Clock has no inputs!";
}

void Clock::resizeOutputs(unsigned /*unused*/)
{
	throw "Clock must have just one output!";
}

void Clock::tick()
{
	setState(!Signal(m_state));
}

void Clock::setState(Signal::State s)
{
	m_state = s;
	auto &conn = output_connections[0];
	conn.setSignal(s);
	if (conn.hasChanged()) {
		if (conn)
			conn.to_component->notifyStateChanged();
		conn.flush();
	}
}

void Clock::update()
{
	this->output_connections[0].setSignal(m_state);
}

Signal::State Clock::getState()
{
	return m_state;
}

void Clock::serialize(std::ostream &os) const
{
	Component::serialize(os);
	int s = Signal::toInt(m_state);
	os.write((char *)(&s), sizeof(s));
}

void Clock::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	Component::deserialize(is, lookup);
	int s;
	is.read((char *)(&s), sizeof(s));
	m_state = Signal::fromInt(s);
}

ComponentTypes Clock::getType() const
{
	return ComponentTypes::CLOCK;
}
