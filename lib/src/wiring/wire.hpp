#ifndef WIRE_H
#define WIRE_H

#include "component/component.hpp"

class Wire : public Component
{
public:
	Wire();
	~Wire() override;

	Signal::State getState() const;

	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is, std::map<int, Component *> lookup) override;

	ComponentTypes getType() const override;

protected:
	void update() override;

private:
	Signal::State m_state;
};

#endif // WIRE_H
