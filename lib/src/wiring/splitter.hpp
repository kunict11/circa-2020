#ifndef SPLITTER_H
#define SPLITTER_H

#include "component/component.hpp"

class Splitter : public Component
{
public:
	Splitter();
	~Splitter() override;

	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // SPLITTER_H
