#ifndef LIGHTBULB_H
#define LIGHTBULB_H

#include "component/component.hpp"

class LightBulb : public Component
{
public:
	LightBulb();
	~LightBulb() override;

	Signal getOutput();
	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // LIGHTBULB_H
