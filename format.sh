#!/bin/bash
find ./app -regex '.*\.\(cpp\|hpp\)' -exec clang-format -style=file -i {} \;
find ./lib -regex '.*\.\(cpp\|hpp\)' -exec clang-format -style=file -i {} \;
find ./test -regex '.*\.\(cpp\|hpp\)' -exec clang-format -style=file -i {} \;
