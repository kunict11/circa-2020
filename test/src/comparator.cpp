#include "arithmetic/comparator.hpp"

#include "catch.hpp"

TEST_CASE("Comparator Constructor", "[comparator][init][unit]")
{

    SECTION("Default", "Make a 4 input comparator")
    {
        Comparator cmp;
        REQUIRE(cmp.getInputSize() == 4);
        REQUIRE(cmp.getOutputSize() == 3);
    }
}

TEST_CASE("Comparator Update", "[comparator][update][unit]")
{
    Comparator cmp;

    REQUIRE(cmp.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(cmp.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(cmp.getOutput(2).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        cmp.setInput(1, Signal::State::ERROR);

        cmp.setInput(0, Signal::State::FALSE);
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::ERROR);

        cmp.setInput(0, Signal::State::TRUE);
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::ERROR);

        cmp.setInput(0, Signal::State::UNDEFINED);
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        cmp.setInput(1, Signal::State::UNDEFINED);

        cmp.setInput(0, Signal::State::FALSE);
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::UNDEFINED);

        cmp.setInput(0, Signal::State::TRUE);
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::UNDEFINED);

        cmp.setInput(0, Signal::State::UNDEFINED);
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Less", "First number is smaller than the second")
    {
        // -1
        cmp.setInput(0, Signal::State::TRUE);
        cmp.setInput(1, Signal::State::TRUE);
        //0
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::TRUE);
    }

    SECTION("Equal", "The numbers are eaqual")
    {
        // 1
        cmp.setInput(0, Signal::State::FALSE);
        cmp.setInput(1, Signal::State::TRUE);
        // 1
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::TRUE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::FALSE);
    }

    SECTION("Greater", "The first number is larger than the second")
    {
        // 1
        cmp.setInput(0, Signal::State::FALSE);
        cmp.setInput(1, Signal::State::TRUE);
        // 0
        cmp.setInput(2, Signal::State::FALSE);
        cmp.setInput(3, Signal::State::FALSE);
        cmp.notifyStateChanged();
        REQUIRE(cmp.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(cmp.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(cmp.getOutput(2).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("Comparator Resize Inputs", "[comparator][resize][unit]")
{
    Comparator cmp;
    cmp.setInput(0, Signal::State::TRUE);
    cmp.setInput(1, Signal::State::TRUE);
    cmp.setInput(2, Signal::State::TRUE);
    cmp.setInput(3, Signal::State::TRUE);

    cmp.resizeInputs(6);
    REQUIRE(cmp.getInputSize() == 6);
    REQUIRE(cmp.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(cmp.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(cmp.getOutput(2).getState() == Signal::State::UNDEFINED);

    cmp.resizeInputs(4);
    REQUIRE(cmp.getInputSize() == 4);
    REQUIRE(cmp.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(cmp.getOutput(1).getState() == Signal::State::TRUE);
    REQUIRE(cmp.getOutput(2).getState() == Signal::State::FALSE);

    REQUIRE_THROWS(cmp.resizeInputs(5));
}

TEST_CASE("Comparator Resize Outputs", "[comparator][resize][unit]")
{
    Comparator cmp;
    REQUIRE_THROWS(cmp.resizeOutputs(2));
}
