#include "plexer/demultiplexer.hpp"

#include "catch.hpp"

TEST_CASE("Demultiplexer constructor", "[demux][init][unit]")
{

    SECTION("Default", "Make a 3 input demultiplexer where one is the select input and one is the enable input")
    {
        Demultiplexer demux;
        REQUIRE(demux.getInputSize() == 3);
        REQUIRE(demux.getOutputSize() == 2);
    }
}

TEST_CASE("Demultiplexer Update", "[demux][update][unit]")
{
    Demultiplexer demux;

    REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "Enable input is error")
    {
        demux.setInput(2, Signal::State::ERROR);

        demux.setInput(0, Signal::State::FALSE);
        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(0, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(0, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Input error", "Input is error")
    {
        demux.setInput(0, Signal::State::ERROR);

        demux.setInput(1, Signal::State::FALSE);
        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Input error", "Select input is error")
    {
        demux.setInput(1, Signal::State::ERROR);

        demux.setInput(0, Signal::State::FALSE);
        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(0, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(0, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(0, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);

        demux.setInput(0, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "Select input is undefined")
    {
        demux.setInput(1, Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::FALSE);
        demux.setInput(2, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Input undefined", "Input is undefined")
    {
        demux.setInput(0, Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::FALSE);
        demux.setInput(2, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Undefined outputs", "Both outputs should be undefined")
    {
        demux.setInput(0, Signal::State::FALSE);
        demux.setInput(1, Signal::State::FALSE);
        demux.setInput(2, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::ERROR);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(0, Signal::State::ERROR);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::FALSE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);

        demux.setInput(1, Signal::State::ERROR);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);
    }
    SECTION("Positive first output", "The first output should be true")
    {
        demux.setInput(0, Signal::State::TRUE);
        demux.setInput(1, Signal::State::FALSE);
        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::FALSE);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::FALSE);
    }

    SECTION("Positive second output", "The second output should be true")
    {
        demux.setInput(0, Signal::State::TRUE);
        demux.setInput(1, Signal::State::TRUE);
        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::TRUE);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::TRUE);
    }

    SECTION("Negative output", "Both outputs should be false")
    {
        demux.setInput(0, Signal::State::FALSE);
        demux.setInput(1, Signal::State::FALSE);
        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::FALSE);

        demux.setInput(2, Signal::State::UNDEFINED);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::FALSE);

        demux.setInput(1, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::FALSE);

        demux.setInput(2, Signal::State::TRUE);
        demux.notifyStateChanged();
        REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(demux.getOutput(1).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("Demultiplexer Resize Inputs", "[demux][resize][unit]")
{
    Demultiplexer demux;
    demux.setInput(0, Signal::State::TRUE);
    demux.setInput(1, Signal::State::TRUE);
    demux.setInput(2, Signal::State::TRUE);

    demux.resizeInputs(4);
    REQUIRE(demux.getInputSize() == 4);
    REQUIRE(demux.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(demux.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(demux.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(demux.getOutput(3).getState() == Signal::State::UNDEFINED);

    demux.resizeInputs(3);
    REQUIRE(demux.getInputSize() == 3);
    REQUIRE(demux.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(demux.getOutput(1).getState() == Signal::State::TRUE);

    REQUIRE_NOTHROW(demux.resizeInputs(3));
    REQUIRE_NOTHROW(demux.resizeInputs(2));

}

TEST_CASE("Demultiplexer Resize Outputs", "[demux][resize][unit]")
{
    Demultiplexer demux;
    REQUIRE_THROWS(demux.resizeOutputs(2));
}
