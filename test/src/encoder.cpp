#include "plexer/encoder.hpp"

#include "catch.hpp"

TEST_CASE("Encoder constructor", "[enc][init][unit]")
{

    SECTION("Default", "Make a 2 input encoder")
    {
        Encoder enc;
        REQUIRE(enc.getInputSize() == 2);
        REQUIRE(enc.getOutputSize() == 1);
    }
}

TEST_CASE("Encoder Update", "[enc][update][unit]")
{
    Encoder enc;

    REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One or both inputs have an error signal")
    {
        enc.setInput(0, Signal::State::ERROR);

        enc.setInput(1, Signal::State::FALSE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(1, Signal::State::TRUE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::TRUE);

        enc.setInput(1, Signal::State::UNDEFINED);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(1, Signal::State::ERROR);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(0, Signal::State::FALSE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(0, Signal::State::TRUE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::FALSE);

        enc.setInput(0, Signal::State::UNDEFINED);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Input undefined", "One or both inputs are undefined")
    {
        enc.setInput(0, Signal::State::UNDEFINED);

        enc.setInput(1, Signal::State::FALSE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(1, Signal::State::TRUE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::TRUE);

        enc.setInput(1, Signal::State::UNDEFINED);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(0, Signal::State::FALSE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

        enc.setInput(0, Signal::State::TRUE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::FALSE);
    }

    SECTION("Positive output", "Output should be true")
    {
        enc.setInput(0, Signal::State::FALSE);
        enc.setInput(1, Signal::State::TRUE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::TRUE);
    }

    SECTION("Negative output", "Output should be false")
    {
        enc.setInput(0, Signal::State::TRUE);
        enc.setInput(1, Signal::State::FALSE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::FALSE);
    }

    SECTION("Error output", "Output should be error")
    {
        enc.setInput(0, Signal::State::TRUE);
        enc.setInput(1, Signal::State::TRUE);
        enc.notifyStateChanged();
        REQUIRE(enc.getOutput(0).getState() == Signal::State::ERROR);
    }
}

TEST_CASE("Encoder Resize Inputs", "[enc][resize][unit]")
{
    Encoder enc;
    enc.setInput(0, Signal::State::FALSE);
    enc.setInput(1, Signal::State::FALSE);
    enc.notifyStateChanged();

    enc.resizeInputs(4);
    REQUIRE(enc.getInputSize() == 4);
    REQUIRE(enc.getOutputSize() == 2);
    REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(enc.getOutput(1).getState() == Signal::State::UNDEFINED);

    enc.setInput(2, Signal::State::TRUE);
    enc.setInput(3, Signal::State::FALSE);
    enc.notifyStateChanged();
    REQUIRE(enc.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(enc.getOutput(1).getState() == Signal::State::TRUE);

    enc.resizeInputs(2);
    REQUIRE(enc.getInputSize() == 2);
    REQUIRE(enc.getOutput(0).getState() == Signal::State::UNDEFINED);

    REQUIRE_NOTHROW(enc.resizeInputs(2));
}

TEST_CASE("Encoder Resize Outputs", "[enc][resize][unit]")
{
    Encoder enc;
    REQUIRE_THROWS(enc.resizeOutputs(2));
}
