#include "memory/tflipflop.hpp"

#include "catch.hpp"

TEST_CASE("TFlipFlop constructor", "[tff][init][unit]")
{

    SECTION("Default", "Make a T flip-flop")
    {
        TFlipFlop tff;
        REQUIRE(tff.getInputSize() == 2);
        REQUIRE(tff.getOutputSize() == 2);
    }
}

TEST_CASE("TFlipFlop Update", "[tff][update][unit]")
{
    TFlipFlop tff;

    REQUIRE(tff.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(tff.getOutput(1).getState() == Signal::State::TRUE);

    SECTION("Input error", "Both outputs should be error")
    {
        tff.setInput(0, Signal::State::ERROR);
        tff.setInput(1, Signal::State::FALSE);

        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::ERROR);

        tff.setInput(1, Signal::State::TRUE);

        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::ERROR);

        tff.setInput(0, Signal::State::FALSE);
        tff.setInput(1, Signal::State::ERROR);

        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::ERROR);

        tff.setInput(0, Signal::State::TRUE);

        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Unchanged output", "Output should stay the same whenever at least one input is false")
    {
        tff.setInput(0, Signal::State::FALSE);
        tff.setInput(1, Signal::State::TRUE);
        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::TRUE);

        tff.setInput(1, Signal::State::FALSE);
        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::TRUE);

        tff.setInput(0, Signal::State::TRUE);
        tff.setInput(1, Signal::State::FALSE);
        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::TRUE);

        tff.setInput(0, Signal::State::UNDEFINED);
        tff.setInput(1, Signal::State::UNDEFINED);
        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::TRUE);
    }

    SECTION("Changing output", "Output should only change when both inputs have a positive signal")
    {
        tff.setInput(0, Signal::State::TRUE);
        tff.setInput(1, Signal::State::TRUE);
        tff.notifyStateChanged();
        REQUIRE(tff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(tff.getOutput(1).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("TFlipFlop Resize Inputs", "[tff][resize][unit]")
{
    TFlipFlop tff;
    REQUIRE_THROWS(tff.resizeInputs(6));
    REQUIRE_NOTHROW(tff.resizeInputs(2));

}

TEST_CASE("TFlipFlop Resize Outputs", "[tff][resize][unit]")
{
    TFlipFlop tff;
    REQUIRE_THROWS(tff.resizeOutputs(4));
    REQUIRE_NOTHROW(tff.resizeOutputs(2));
}
