#include "arithmetic/divider.hpp"

#include "catch.hpp"

TEST_CASE("Divider Constructor", "[divider][init][unit]")
{

    SECTION("Default", "Make a 6 input divider")
    {
        Divider div;
        REQUIRE(div.getInputSize() == 6);
        REQUIRE(div.getOutputSize() == 3);
    }
}

TEST_CASE("Divider Update", "[divider][update][unit]")
{
    Divider div;

    REQUIRE(div.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(div.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(div.getOutput(2).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        div.setInput(1, Signal::State::ERROR);

        div.setInput(0, Signal::State::FALSE);
        div.setInput(2, Signal::State::FALSE);
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(2).getState() == Signal::State::ERROR);

        div.setInput(0, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(2).getState() == Signal::State::ERROR);

        div.setInput(0, Signal::State::UNDEFINED);
        div.setInput(2, Signal::State::FALSE);
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(2).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        div.setInput(1, Signal::State::UNDEFINED);

        div.setInput(0, Signal::State::FALSE);
        div.setInput(2, Signal::State::FALSE);
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(2).getState() == Signal::State::UNDEFINED);

        div.setInput(0, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(2).getState() == Signal::State::UNDEFINED);

        div.setInput(0, Signal::State::UNDEFINED);
        div.setInput(2, Signal::State::FALSE);
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(2).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Both numbers on input are zero", "Output should be undefined")
    {
        // 0
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::FALSE);
        div.setInput(2, Signal::State::FALSE);
        // 0
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(div.getOutput(2).getState() == Signal::State::UNDEFINED);
    }
    SECTION("Division by zero", "Output should be error") {
        // -2
        div.setInput(0, Signal::State::TRUE);
        div.setInput(1, Signal::State::FALSE);
        div.setInput(2, Signal::State::TRUE);
        // 0
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(2).getState() == Signal::State::ERROR);

        // 1
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        // 0
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(div.getOutput(2).getState() == Signal::State::ERROR);
    }

    SECTION("Two positive numbers on input", "Output should be a positive number")
    {
        // 3
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::TRUE);
        // 2
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);
    }

    SECTION("Two negative numbers on input", "Output should be a positive number")
    {
        // -1
        div.setInput(0, Signal::State::TRUE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        // -1
        div.setInput(3, Signal::State::TRUE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);
    }


    SECTION("Numbers on input have opposite signs", "Output should be negative")
    {
        // -3
        div.setInput(0, Signal::State::TRUE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::TRUE);
        // 2
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);

        // 2
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::FALSE);
        div.setInput(2, Signal::State::TRUE);
        // -1
        div.setInput(3, Signal::State::TRUE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::TRUE);
    }

    SECTION("Zero output", "First number is zero or the divident is smaller than the divisor.")
    {
        // 0
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::FALSE);
        div.setInput(2, Signal::State::FALSE);
        // 1
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::FALSE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);

        // 0
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::FALSE);
        div.setInput(2, Signal::State::FALSE);
        // -2
        div.setInput(3, Signal::State::TRUE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);

        // 1
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        // 3
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);

        // -1
        div.setInput(0, Signal::State::TRUE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        // -2
        div.setInput(3, Signal::State::TRUE);
        div.setInput(4, Signal::State::FALSE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);

        // -1
        div.setInput(0, Signal::State::TRUE);
        div.setInput(1, Signal::State::TRUE);
        div.setInput(2, Signal::State::FALSE);
        // 3
        div.setInput(3, Signal::State::FALSE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);

        // 2
        div.setInput(0, Signal::State::FALSE);
        div.setInput(1, Signal::State::FALSE);
        div.setInput(2, Signal::State::TRUE);
        // -3
        div.setInput(3, Signal::State::TRUE);
        div.setInput(4, Signal::State::TRUE);
        div.setInput(5, Signal::State::TRUE);
        div.notifyStateChanged();
        REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(div.getOutput(2).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("Divider Resize Inputs", "[divider][resize][unit]")
{
    Divider div;
    div.setInput(0, Signal::State::TRUE);
    div.setInput(1, Signal::State::TRUE);
    div.setInput(2, Signal::State::TRUE);
    div.setInput(3, Signal::State::TRUE);
    div.setInput(4, Signal::State::TRUE);
    div.setInput(5, Signal::State::TRUE);

    div.resizeInputs(8);
    REQUIRE(div.getInputSize() == 8);
    REQUIRE(div.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(div.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(div.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(div.getOutput(3).getState() == Signal::State::UNDEFINED);

    div.resizeInputs(4);
    REQUIRE(div.getInputSize() == 4);
    REQUIRE(div.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(div.getOutput(1).getState() == Signal::State::TRUE);

    REQUIRE_THROWS(div.resizeInputs(5));
}

TEST_CASE("Divider Resize Outputs", "[divider][resize][unit]")
{
    Divider div;
    REQUIRE_THROWS(div.resizeOutputs(2));
}
