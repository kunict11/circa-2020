#include "plexer/multiplexer.hpp"

#include "catch.hpp"

TEST_CASE("Multiplexer constructor", "[mux][init][unit]")
{

    SECTION("Default", "Make a 3 input multiplexer where one is the select input")
    {
        Multiplexer mux;
        REQUIRE(mux.getInputSize() == 3);
        REQUIRE(mux.getOutputSize() == 1);
    }
}

TEST_CASE("Multiplexer Update", "[mux][update][unit]")
{
    Multiplexer mux;

    REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "First input is error")
    {
        mux.setInput(0, Signal::State::ERROR);

        mux.setInput(1, Signal::State::FALSE);
        mux.setInput(2, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);

        mux.setInput(1, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);

        mux.setInput(1, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(2, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(2, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(1, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(1, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Input error", "Second input is error")
    {
        mux.setInput(1, Signal::State::ERROR);

        mux.setInput(0, Signal::State::FALSE);
        mux.setInput(2, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);

        mux.setInput(0, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);

        mux.setInput(0, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(2, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(0, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(0, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(2, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Input error", "One select input is error")
    {
        mux.setInput(2, Signal::State::ERROR);

        mux.setInput(0, Signal::State::TRUE);
        mux.setInput(1, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(0, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(0, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);

        mux.setInput(1, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::ERROR);
    }


    SECTION("Input undefined", "Second input is undefined")
    {
        mux.setInput(1, Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::FALSE);
        mux.setInput(2, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);

        mux.setInput(0, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);

        mux.setInput(0, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(2, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(2, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

        mux.setInput(0, Signal::State::UNDEFINED);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Positive output", "First input is true, select input is false")
    {
        mux.setInput(0, Signal::State::TRUE);
        mux.setInput(1, Signal::State::TRUE);
        mux.setInput(2, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);

        mux.setInput(1, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);
    }

    SECTION("Positive output", "Second input is true, select input is true")
    {
        mux.setInput(0, Signal::State::TRUE);
        mux.setInput(1, Signal::State::TRUE);
        mux.setInput(2, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);

        mux.setInput(0, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);
    }

    SECTION("Negative output", "Output should be false")
    {
        mux.setInput(0, Signal::State::FALSE);
        mux.setInput(1, Signal::State::FALSE);
        mux.setInput(2, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);

        mux.setInput(2, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);

        mux.setInput(0, Signal::State::TRUE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);

        mux.setInput(0, Signal::State::FALSE);
        mux.setInput(1, Signal::State::TRUE);
        mux.setInput(2, Signal::State::FALSE);
        mux.update();
        REQUIRE(mux.getOutput(0).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("Multiplexer Resize Inputs", "[mux][resize][unit]")
{
    Multiplexer mux;
    mux.setInput(0, Signal::State::TRUE);
    mux.setInput(1, Signal::State::TRUE);
    mux.setInput(2, Signal::State::TRUE);

    mux.resizeInputs(4);
    REQUIRE(mux.getInputSize() == 6);
    REQUIRE(mux.getOutput(0).getState() == Signal::State::UNDEFINED);

    mux.resizeInputs(2);
    REQUIRE(mux.getInputSize() == 3);
    REQUIRE(mux.getOutput(0).getState() == Signal::State::TRUE);

    REQUIRE_NOTHROW(mux.resizeInputs(2));

}

TEST_CASE("Multiplexer Resize Outputs", "[mux][resize][unit]")
{
    Multiplexer mux;
    REQUIRE_THROWS(mux.resizeOutputs(2));
}
