#include "memory/ram.hpp"

#include "catch.hpp"

TEST_CASE("RAM Constructor", "[ram][init][unit]")
{

    SECTION("Default", "Make a 6 input ram")
    {
        RAM ram;
        REQUIRE(ram.getInputSize() == 6);
        REQUIRE(ram.getOutputSize() == 1);
    }
}

TEST_CASE("RAM Update", "[ram][update][unit]")
{
    RAM ram;

    REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        ram.setInput(1, Signal::State::ERROR);

        ram.setInput(0, Signal::State::FALSE);
        ram.setInput(2, Signal::State::FALSE);
        ram.setInput(3, Signal::State::FALSE);
        ram.setInput(4, Signal::State::FALSE);
        ram.setInput(5, Signal::State::FALSE);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::ERROR);

        ram.setInput(0, Signal::State::TRUE);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::ERROR);

        ram.setInput(0, Signal::State::UNDEFINED);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        ram.setInput(1, Signal::State::UNDEFINED);

        ram.setInput(0, Signal::State::FALSE);
        ram.setInput(2, Signal::State::FALSE);
        ram.setInput(3, Signal::State::FALSE);
        ram.setInput(4, Signal::State::FALSE);
        ram.setInput(5, Signal::State::FALSE);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);

        ram.setInput(0, Signal::State::TRUE);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);

        ram.setInput(0, Signal::State::UNDEFINED);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Update contents", "Selected address should be updated when the load signal is negative and clock is positive")
    {
        ram.setInput(0, Signal::State::TRUE);
        ram.setInput(1, Signal::State::TRUE);
        ram.setInput(2, Signal::State::TRUE);
        ram.setInput(3, Signal::State::FALSE);
        ram.setInput(4, Signal::State::FALSE);
        ram.setInput(5, Signal::State::FALSE);
        ram.notifyStateChanged();
        REQUIRE(ram.getContents().at(3).getState() == Signal::State::FALSE);
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);

        ram.setInput(3, Signal::State::TRUE);
        ram.notifyStateChanged();
        REQUIRE(ram.getContents().at(3).getState() == Signal::State::TRUE);
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Positive output", "Output should be true when the selected address has a positive signal")
    {
        ram.setInput(0, Signal::State::TRUE);
        ram.setInput(1, Signal::State::TRUE);
        ram.setInput(2, Signal::State::TRUE);
        ram.setInput(3, Signal::State::TRUE);
        ram.setInput(4, Signal::State::FALSE);
        ram.setInput(5, Signal::State::FALSE);
        ram.notifyStateChanged();
        REQUIRE(ram.getContents().at(3).getState() == Signal::State::TRUE);
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);

        ram.setInput(4, Signal::State::TRUE);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::TRUE);
    }

    SECTION("Negative output", "Output should be false when the selected address has a negative signal")
    {
        ram.setInput(0, Signal::State::FALSE);
        ram.setInput(1, Signal::State::FALSE);
        ram.setInput(2, Signal::State::FALSE);
        ram.setInput(3, Signal::State::FALSE);
        ram.setInput(4, Signal::State::FALSE);
        ram.setInput(5, Signal::State::FALSE);
        ram.notifyStateChanged();
        REQUIRE(ram.getContents().at(0).getState() == Signal::State::FALSE);
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);

        ram.setInput(2, Signal::State::TRUE);
        ram.setInput(4, Signal::State::TRUE);
        ram.notifyStateChanged();
        REQUIRE(ram.getOutput(0).getState() == Signal::State::FALSE);
    }

    SECTION("Reset contents", "All contents should have a negative signal if the reset input is active")
    {
        ram.setInput(5, Signal::State::TRUE);
        ram.notifyStateChanged();
        REQUIRE(ram.getContents().at(0).getState() == Signal::State::FALSE);
        REQUIRE(ram.getContents().at(1).getState() == Signal::State::FALSE);
        REQUIRE(ram.getContents().at(2).getState() == Signal::State::FALSE);
        REQUIRE(ram.getContents().at(3).getState() == Signal::State::FALSE);
        REQUIRE(ram.getOutput(0).getState() == Signal::State::UNDEFINED);
    }
}

TEST_CASE("RAM Resize Inputs", "[ram][resize][unit]")
{
    RAM ram;
    ram.setInput(0, Signal::State::FALSE);
    ram.setInput(1, Signal::State::FALSE);
    ram.setInput(2, Signal::State::FALSE);
    ram.setInput(3, Signal::State::FALSE);
    ram.setInput(4, Signal::State::FALSE);
    ram.setInput(5, Signal::State::FALSE);

    ram.resizeInputs(5);
    REQUIRE(ram.getInputSize() == 9);
    REQUIRE(ram.getRowCount() == 4);
    REQUIRE(ram.getColCount() == 8);

    ram.resizeInputs(2);
    REQUIRE(ram.getInputSize() == 6);
    REQUIRE(ram.getRowCount() == 2);
    REQUIRE(ram.getColCount() == 2);

    ram.resizeInputs(7);
    REQUIRE(ram.getInputSize() == 11);
    REQUIRE(ram.getRowCount() == 8);
    REQUIRE(ram.getColCount() == 16);

    REQUIRE_THROWS(ram.resizeInputs(1));

    REQUIRE_THROWS(ram.resizeInputs(11));
}

TEST_CASE("RAM Resize Outputs", "[ram][resize][unit]")
{
    RAM ram;
    REQUIRE_THROWS(ram.resizeOutputs(2));

    REQUIRE_NOTHROW(ram.resizeOutputs(1));
}
