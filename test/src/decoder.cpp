#include "plexer/decoder.hpp"

#include "catch.hpp"

TEST_CASE("Decoder constructor", "[dec][init][unit]")
{

    SECTION("Default", "Make a 1 input decoder")
    {
        Decoder dec;
        REQUIRE(dec.getInputSize() == 1);
        REQUIRE(dec.getOutputSize() == 2);
    }
}

TEST_CASE("Decoder Update", "[dec][update][unit]")
{
    Decoder dec;

    REQUIRE(dec.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(1).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "Both outputs should be error")
    {
        dec.setInput(0, Signal::State::ERROR);

        dec.notifyStateChanged();
        REQUIRE(dec.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(dec.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Positive output", "First output should be true")
    {
        dec.setInput(0, Signal::State::FALSE);
        dec.notifyStateChanged();
        REQUIRE(dec.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dec.getOutput(1).getState() == Signal::State::FALSE);
    }

    SECTION("Positive output", "Second output should be true")
    {
        dec.setInput(0, Signal::State::TRUE);
        dec.notifyStateChanged();
        REQUIRE(dec.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dec.getOutput(1).getState() == Signal::State::TRUE);
    }
}

TEST_CASE("Decoder Resize Inputs", "[dec][resize][unit]")
{
    Decoder dec;
    dec.setInput(0, Signal::State::TRUE);

    dec.resizeInputs(3);
    REQUIRE(dec.getInputSize() == 3);
    REQUIRE(dec.getOutputSize() == 8);
    REQUIRE(dec.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(3).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(4).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(5).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(6).getState() == Signal::State::UNDEFINED);
    REQUIRE(dec.getOutput(7).getState() == Signal::State::UNDEFINED);

    dec.setInput(1, Signal::State::FALSE);
    dec.setInput(2, Signal::State::TRUE);
    dec.notifyStateChanged();
    REQUIRE(dec.getOutput(1).getState() == Signal::State::FALSE);
    REQUIRE(dec.getOutput(5).getState() == Signal::State::TRUE);

    dec.resizeInputs(2);
    REQUIRE(dec.getInputSize() == 2);
    REQUIRE(dec.getOutput(1).getState() == Signal::State::TRUE);

    REQUIRE_NOTHROW(dec.resizeInputs(dec.getInputSize()));

}

TEST_CASE("Decoder Resize Outputs", "[dec][resize][unit]")
{
    Decoder dec;
    REQUIRE_THROWS(dec.resizeOutputs(2));
}
