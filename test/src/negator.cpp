#include "arithmetic/negator.hpp"

#include "catch.hpp"

TEST_CASE("Negator Constructor", "[negator][init][unit]")
{

    SECTION("Default", "Make a 3 input negator")
    {
        Negator neg;
        REQUIRE(neg.getInputSize() == 3);
        REQUIRE(neg.getOutputSize() == 3);
    }
}

TEST_CASE("Negator Update", "[negator][update][unit]")
{
    Negator neg;

    REQUIRE(neg.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(2).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        neg.setInput(1, Signal::State::ERROR);

        neg.setInput(0, Signal::State::FALSE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::ERROR);

        neg.setInput(0, Signal::State::TRUE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::ERROR);

        neg.setInput(0, Signal::State::UNDEFINED);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        neg.setInput(1, Signal::State::UNDEFINED);

        neg.setInput(0, Signal::State::FALSE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::UNDEFINED);

        neg.setInput(0, Signal::State::TRUE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::UNDEFINED);

        neg.setInput(0, Signal::State::UNDEFINED);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Positive output", "Output number should be positive")
    {
        // -1
        neg.setInput(0, Signal::State::TRUE);
        neg.setInput(1, Signal::State::TRUE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::FALSE);

        // -2
        neg.setInput(0, Signal::State::TRUE);
        neg.setInput(1, Signal::State::FALSE);
        neg.setInput(2, Signal::State::TRUE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::TRUE);

        // -3
        neg.setInput(0, Signal::State::TRUE);
        neg.setInput(1, Signal::State::TRUE);
        neg.setInput(2, Signal::State::TRUE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::TRUE);
    }

    SECTION("Negative output", "Output number should be negative")
    {
        // 1
        neg.setInput(0, Signal::State::FALSE);
        neg.setInput(1, Signal::State::TRUE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::FALSE);

        // 2
        neg.setInput(0, Signal::State::FALSE);
        neg.setInput(1, Signal::State::FALSE);
        neg.setInput(2, Signal::State::TRUE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::TRUE);

        // 3
        neg.setInput(0, Signal::State::FALSE);
        neg.setInput(1, Signal::State::TRUE);
        neg.setInput(2, Signal::State::TRUE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::TRUE);
    }

    SECTION("Zero output", "Output number is zero")
    {
        neg.setInput(0, Signal::State::TRUE);
        neg.setInput(1, Signal::State::FALSE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::FALSE);

        neg.setInput(0, Signal::State::FALSE);
        neg.setInput(1, Signal::State::FALSE);
        neg.setInput(2, Signal::State::FALSE);
        neg.notifyStateChanged();
        REQUIRE(neg.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(neg.getOutput(2).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("Negator Resize Inputs", "[negator][resize][unit]")
{
    Negator neg;
    neg.setInput(0, Signal::State::TRUE);
    neg.setInput(1, Signal::State::TRUE);
    neg.setInput(2, Signal::State::TRUE);

    neg.resizeInputs(6);
    REQUIRE(neg.getInputSize() == 6);
    REQUIRE(neg.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(3).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(4).getState() == Signal::State::UNDEFINED);
    REQUIRE(neg.getOutput(5).getState() == Signal::State::UNDEFINED);

    neg.resizeInputs(3);
    REQUIRE(neg.getInputSize() == 3);
    REQUIRE(neg.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(neg.getOutput(1).getState() == Signal::State::TRUE);
    REQUIRE(neg.getOutput(2).getState() == Signal::State::TRUE);
}

TEST_CASE("Negator Resize Outputs", "[negator][resize][unit]")
{
    Negator neg;
    REQUIRE_THROWS(neg.resizeOutputs(2));
}
