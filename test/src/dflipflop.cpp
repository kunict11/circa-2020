#include "memory/dflipflop.hpp"

#include "catch.hpp"

TEST_CASE("DFlipFlop constructor", "[dff][init][unit]")
{

    SECTION("Default", "Make a D flip-flop")
    {
        DFlipFlop dff;
        REQUIRE(dff.getInputSize() == 4);
        REQUIRE(dff.getOutputSize() == 2);
    }
}

TEST_CASE("DFlipFlop Update", "[dff][update][unit]")
{
    DFlipFlop dff;

    REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

    SECTION("Input error", "Both outputs should be error")
    {
        dff.setInput(0, Signal::State::ERROR);
        dff.setInput(1, Signal::State::FALSE);
        dff.setInput(2, Signal::State::FALSE);
        dff.setInput(3, Signal::State::FALSE);

        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Positive output", "Output should be true, inverse output is false")
    {
        dff.setInput(0, Signal::State::TRUE);
        dff.setInput(1, Signal::State::FALSE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(0, Signal::State::FALSE);
        dff.setInput(2, Signal::State::TRUE);
        dff.setInput(3, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(3, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(0, Signal::State::UNDEFINED);
        dff.setInput(1, Signal::State::UNDEFINED);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(0, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(0, Signal::State::FALSE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(0, Signal::State::UNDEFINED);
        dff.setInput(1, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);

        dff.setInput(1, Signal::State::FALSE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::FALSE);
    }

    SECTION("Negative output", "Inverse output should be true")
    {
        dff.setInput(0, Signal::State::FALSE);
        dff.setInput(1, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(1, Signal::State::FALSE);
        dff.setInput(2, Signal::State::FALSE);
        dff.setInput(3, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(3, Signal::State::FALSE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(0, Signal::State::UNDEFINED);
        dff.setInput(1, Signal::State::UNDEFINED);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(0, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(0, Signal::State::FALSE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(0, Signal::State::UNDEFINED);
        dff.setInput(1, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);

        dff.setInput(1, Signal::State::FALSE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);
    }

    SECTION("Undefined behavior", "Set and reset inputs have a positive signal")
    {
        dff.setInput(0, Signal::State::TRUE);
        dff.setInput(1, Signal::State::TRUE);
        dff.notifyStateChanged();
        REQUIRE(dff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(dff.getOutput(1).getState() == Signal::State::TRUE);
    }
}

TEST_CASE("DFlipFlop Resize Inputs", "[dff][resize][unit]")
{
    DFlipFlop dff;
    REQUIRE_THROWS(dff.resizeInputs(6));
    REQUIRE_NOTHROW(dff.resizeInputs(4));

}

TEST_CASE("DFlipFlop Resize Outputs", "[dff][resize][unit]")
{
    DFlipFlop dff;
    REQUIRE_THROWS(dff.resizeOutputs(4));
    REQUIRE_NOTHROW(dff.resizeOutputs(2));
}
