#include "arithmetic/subtractor.hpp"

#include "catch.hpp"

TEST_CASE("Subtractor Constructor", "[subtractor][init][unit]")
{

    SECTION("Default", "Make a 6 input subtractor")
    {
        Subtractor sub;
        REQUIRE(sub.getInputSize() == 6);
        REQUIRE(sub.getOutputSize() == 4);
    }
}

TEST_CASE("Subtractor Update", "[subtractor][update][unit]")
{
    Subtractor sub;

    REQUIRE(sub.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(3).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        sub.setInput(1, Signal::State::ERROR);

        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(2, Signal::State::FALSE);
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::ERROR);

        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(2, Signal::State::FALSE);
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::ERROR);

        sub.setInput(0, Signal::State::UNDEFINED);
        sub.setInput(2, Signal::State::FALSE);
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        sub.setInput(1, Signal::State::UNDEFINED);

        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(2, Signal::State::FALSE);
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::UNDEFINED);

        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(2, Signal::State::FALSE);
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::UNDEFINED);

        sub.setInput(0, Signal::State::UNDEFINED);
        sub.setInput(2, Signal::State::FALSE);
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Positive inputs, first number is larger", "Output should be a positive number")
    {
        // 2
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::FALSE);
        sub.setInput(2, Signal::State::TRUE);
        // 1
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);

        // 3
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::TRUE);
        // 1
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Positive inputs, second number is larger", "Output should be a negative number")
    {
        // 1
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::FALSE);
        // 2
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);

        // 1
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::FALSE);
        // 3
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Two negative numbers on input, first number is smaller", "Output should be a negative number")
    {
        // -3
        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::TRUE);
        // -2
        sub.setInput(3, Signal::State::TRUE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);

        // -3
        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::TRUE);
        // -1
        sub.setInput(3, Signal::State::TRUE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Two negative numbers on input, second number is smaller", "Output should be a positive number")
    {
        // -2
        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(1, Signal::State::FALSE);
        sub.setInput(2, Signal::State::TRUE);
        // -3
        sub.setInput(3, Signal::State::TRUE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);

        // -1
        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::FALSE);
        // -3
        sub.setInput(3, Signal::State::TRUE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Numbers on input are equal", "Output should be zero" )
    {
        // 0
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::FALSE);
        sub.setInput(2, Signal::State::FALSE);
        // 0
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);

        // 2
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::FALSE);
        sub.setInput(2, Signal::State::TRUE);
        // 2
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::FALSE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);

        // -1
        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(1, Signal::State::TRUE);
        sub.setInput(2, Signal::State::FALSE);
        // -1
        sub.setInput(3, Signal::State::TRUE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::FALSE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("First number is negative, second is positive", "Output should be negative")
    {
        // -2
        sub.setInput(0, Signal::State::TRUE);
        sub.setInput(1, Signal::State::FALSE);
        sub.setInput(2, Signal::State::TRUE);
        // 3
        sub.setInput(3, Signal::State::FALSE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::TRUE);
    }

    SECTION("First number is positive, second is negative", "Output should be positive")
    {
        // 2
        sub.setInput(0, Signal::State::FALSE);
        sub.setInput(1, Signal::State::FALSE);
        sub.setInput(2, Signal::State::TRUE);
        // -3
        sub.setInput(3, Signal::State::TRUE);
        sub.setInput(4, Signal::State::TRUE);
        sub.setInput(5, Signal::State::TRUE);
        sub.notifyStateChanged();
        REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(sub.getOutput(3).getState() == Signal::State::TRUE);
    }

}

TEST_CASE("Subtractor Resize Inputs", "[subtractor][resize][unit]")
{
    Subtractor sub;
    sub.setInput(0, Signal::State::TRUE);
    sub.setInput(1, Signal::State::TRUE);
    sub.setInput(2, Signal::State::TRUE);
    sub.setInput(3, Signal::State::TRUE);
    sub.setInput(4, Signal::State::TRUE);
    sub.setInput(5, Signal::State::TRUE);

    sub.resizeInputs(8);
    REQUIRE(sub.getInputSize() == 8);
    REQUIRE(sub.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(3).getState() == Signal::State::UNDEFINED);
    REQUIRE(sub.getOutput(4).getState() == Signal::State::UNDEFINED);

    sub.resizeInputs(4);
    REQUIRE(sub.getInputSize() == 4);
    REQUIRE(sub.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(sub.getOutput(1).getState() == Signal::State::FALSE);
    REQUIRE(sub.getOutput(2).getState() == Signal::State::FALSE);

    REQUIRE_THROWS(sub.resizeInputs(5));
}

TEST_CASE("Subtractor Resize Outputs", "[subtractor][resize][unit]")
{
    Subtractor sub;
    REQUIRE_THROWS(sub.resizeOutputs(2));
}
