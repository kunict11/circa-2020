#include "memory/rom.hpp"

#include "catch.hpp"

TEST_CASE("ROM Constructor", "[rom][init][unit]")
{

    SECTION("Default", "Make a 3 input rom")
    {
        ROM rom;
        REQUIRE(rom.getInputSize() == 3);
        REQUIRE(rom.getOutputSize() == 1);
    }
}

TEST_CASE("ROM Contents Update", "[rom][update_contents][unit]")
{
    ROM rom;
    REQUIRE(rom.getContents().size() == 4);
    REQUIRE(rom.getContents().at(0).getState() == Signal::State::FALSE);
    REQUIRE(rom.getContents().at(1).getState() == Signal::State::FALSE);
    REQUIRE(rom.getContents().at(2).getState() == Signal::State::FALSE);
    REQUIRE(rom.getContents().at(3).getState() == Signal::State::FALSE);

    SECTION("Update contents", "Selected address should be updated")
    {
        REQUIRE(rom.getContents().at(2).getState() == Signal::State::FALSE);
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

        rom.updateContents(2);
        REQUIRE(rom.getContents().at(2).getState() == Signal::State::TRUE);
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

        rom.updateContents(2);
        REQUIRE(rom.getContents().at(2).getState() == Signal::State::FALSE);
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);
    }
}

TEST_CASE("ROM Update", "[rom][update][unit]")
{
    ROM rom;

    REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        rom.setInput(1, Signal::State::ERROR);

        rom.setInput(0, Signal::State::FALSE);
        rom.setInput(2, Signal::State::FALSE);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::ERROR);

        rom.setInput(0, Signal::State::TRUE);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::ERROR);

        rom.setInput(0, Signal::State::UNDEFINED);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        rom.setInput(1, Signal::State::UNDEFINED);

        rom.setInput(0, Signal::State::FALSE);
        rom.setInput(2, Signal::State::FALSE);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

        rom.setInput(0, Signal::State::TRUE);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

        rom.setInput(0, Signal::State::UNDEFINED);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Positive output", "Output should be true when the selected address has a positive signal")
    {
        rom.setInput(0, Signal::State::TRUE);
        rom.setInput(1, Signal::State::TRUE);
        rom.setInput(2, Signal::State::FALSE);
        rom.updateContents(3);
        rom.notifyStateChanged();
        REQUIRE(rom.getContents().at(3).getState() == Signal::State::TRUE);
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

        rom.setInput(2, Signal::State::TRUE);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::TRUE);
    }

    SECTION("Negative output", "Output should be false when the selected address has a negative signal")
    {
        rom.setInput(0, Signal::State::FALSE);
        rom.setInput(1, Signal::State::FALSE);
        rom.setInput(2, Signal::State::FALSE);
        rom.notifyStateChanged();
        REQUIRE(rom.getContents().at(0).getState() == Signal::State::FALSE);
        REQUIRE(rom.getOutput(0).getState() == Signal::State::UNDEFINED);

        rom.setInput(2, Signal::State::TRUE);
        rom.notifyStateChanged();
        REQUIRE(rom.getOutput(0).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("ROM Resize Inputs", "[rom][resize][unit]")
{
    ROM rom;
    rom.setInput(0, Signal::State::FALSE);
    rom.setInput(1, Signal::State::FALSE);
    rom.setInput(2, Signal::State::FALSE);

    rom.resizeInputs(5);
    REQUIRE(rom.getInputSize() == 5);
    REQUIRE(rom.getRowCount() == 4);
    REQUIRE(rom.getColCount() == 4);

    rom.resizeInputs(2);
    REQUIRE(rom.getInputSize() == 2);
    REQUIRE(rom.getRowCount() == 1);
    REQUIRE(rom.getColCount() == 2);

    rom.resizeInputs(8);
    REQUIRE(rom.getInputSize() == 8);
    REQUIRE(rom.getRowCount() == 8);
    REQUIRE(rom.getColCount() == 16);

    REQUIRE_THROWS(rom.resizeInputs(1));

    REQUIRE_THROWS(rom.resizeInputs(11));
}

TEST_CASE("ROM Resize Outputs", "[rom][resize][unit]")
{
    ROM rom;
    REQUIRE_THROWS(rom.resizeOutputs(2));

    REQUIRE_NOTHROW(rom.resizeOutputs(1));
}
