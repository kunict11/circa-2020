#include "wiring/pin.hpp"

#include "catch.hpp"

TEST_CASE("Pin Constructor", "[pin][init][unit]")
{
	SECTION("Default", "Unspecified input pin")
	{
		Pin pin;
		REQUIRE(pin.getInputSize() == 0);
		REQUIRE(pin.getOutputSize() == 1);
		REQUIRE(pin.getState() == Signal::State::UNDEFINED);
	}

	SECTION("Specified state", "Specified input pin")
	{
		Pin pin1 = Pin(Signal::State::FALSE);
		REQUIRE(pin1.getState() == Signal::State::FALSE);

		Pin pin2 = Pin(Signal::State::TRUE);
		REQUIRE(pin2.getState() == Signal::State::TRUE);

		Pin pin3 = Pin(Signal::State::ERROR);
		REQUIRE(pin3.getState() == Signal::State::ERROR);
	}
}

TEST_CASE("Pin Set State", "[pin][update][unit]")
{
	Pin pin = Pin(Signal::State::UNDEFINED);

	pin.setState(Signal::State::ERROR);
	REQUIRE(pin.getState() == Signal::State::ERROR);

	pin.setState(Signal::State::FALSE);
	REQUIRE(pin.getState() == Signal::State::FALSE);

	pin.setState(Signal::State::TRUE);
	REQUIRE(pin.getState() == Signal::State::TRUE);
}

TEST_CASE("Pin Flip", "[pin][update][unit]")
{
	SECTION("Flip error", "Flip from error is error")
	{
		Pin pin = Pin(Signal::State::ERROR);
		pin.flip();
		REQUIRE(pin.getState() == Signal::State::ERROR);
	}

	SECTION("Flip undefined", "Flip from undefined is undefined")
	{
		Pin pin = Pin(Signal::State::UNDEFINED);
		pin.flip();
		REQUIRE(pin.getState() == Signal::State::UNDEFINED);
	}

	SECTION("Flip boolean", "Flip boolean value")
	{
		Pin pin = Pin(Signal::State::FALSE);
		pin.flip();
		REQUIRE(pin.getState() == Signal::State::TRUE);
		pin.flip();
		REQUIRE(pin.getState() == Signal::State::FALSE);
	}
}

TEST_CASE("Pin Update", "[pin][update][unit]")
{
    Pin pin = Pin(Signal::State::TRUE);
    pin.update();
    REQUIRE(pin.getOutput(0).getState() == Signal::State::TRUE);
}

TEST_CASE("Pin Resize Inputs", "[pin][resize][unit]")
{
	Pin pin;
	REQUIRE_THROWS(pin.resizeInputs(1));
}

TEST_CASE("Pin Resize Outputs", "[pin][resize][unit]")
{
	Pin pin;
	REQUIRE_THROWS(pin.resizeOutputs(2));
}
