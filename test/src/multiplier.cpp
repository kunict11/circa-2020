#include "arithmetic/multiplier.hpp"

#include "catch.hpp"

TEST_CASE("Multiplier Constructor", "[multiplier][init][unit]")
{

    SECTION("Default", "Make a 6 input multiplier")
    {
        Multiplier mul;
        REQUIRE(mul.getInputSize() == 6);
        REQUIRE(mul.getOutputSize() == 5);
    }
}

TEST_CASE("Multiplier Update", "[multiplier][update][unit]")
{
    Multiplier mul;

    REQUIRE(mul.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(3).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(4).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        mul.setInput(1, Signal::State::ERROR);

        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(2, Signal::State::FALSE);
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::ERROR);

        mul.setInput(0, Signal::State::TRUE);
        mul.setInput(2, Signal::State::FALSE);
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::ERROR);

        mul.setInput(0, Signal::State::UNDEFINED);
        mul.setInput(2, Signal::State::FALSE);
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::ERROR);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        mul.setInput(1, Signal::State::UNDEFINED);

        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(2, Signal::State::FALSE);
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::UNDEFINED);

        mul.setInput(0, Signal::State::TRUE);
        mul.setInput(2, Signal::State::FALSE);
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::UNDEFINED);

        mul.setInput(0, Signal::State::UNDEFINED);
        mul.setInput(2, Signal::State::FALSE);
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::UNDEFINED);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Two positive numbers on input", "Output should be a positive number")
    {
        // 1
        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(1, Signal::State::TRUE);
        mul.setInput(2, Signal::State::FALSE);
        // 2
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::TRUE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);

        // 3
        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(1, Signal::State::TRUE);
        mul.setInput(2, Signal::State::TRUE);
        // 3
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::TRUE);
        mul.setInput(5, Signal::State::TRUE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::TRUE);
    }

    SECTION("Two negative numbers on input", "Output should be a positive number")
    {
        // -2
        mul.setInput(0, Signal::State::TRUE);
        mul.setInput(1, Signal::State::FALSE);
        mul.setInput(2, Signal::State::TRUE);
        // -3
        mul.setInput(3, Signal::State::TRUE);
        mul.setInput(4, Signal::State::TRUE);
        mul.setInput(5, Signal::State::TRUE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);

        // -1
        mul.setInput(0, Signal::State::TRUE);
        mul.setInput(1, Signal::State::TRUE);
        mul.setInput(2, Signal::State::FALSE);
        // -1
        mul.setInput(3, Signal::State::TRUE);
        mul.setInput(4, Signal::State::TRUE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);
    }

    SECTION("At least one number on input is zero", "Output should be zero")
    {
        // 0
        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(1, Signal::State::FALSE);
        mul.setInput(2, Signal::State::FALSE);
        // 0
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);

        // -2
        mul.setInput(0, Signal::State::TRUE);
        mul.setInput(1, Signal::State::FALSE);
        mul.setInput(2, Signal::State::TRUE);
        // 0
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::FALSE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);

        // 0
        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(1, Signal::State::FALSE);
        mul.setInput(2, Signal::State::FALSE);
        // 1
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::TRUE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);
    }

    SECTION("Numbers on input have opposite signs", "Output should be negative")
    {
        // -2
        mul.setInput(0, Signal::State::TRUE);
        mul.setInput(1, Signal::State::FALSE);
        mul.setInput(2, Signal::State::TRUE);
        // 3
        mul.setInput(3, Signal::State::FALSE);
        mul.setInput(4, Signal::State::TRUE);
        mul.setInput(5, Signal::State::TRUE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);

        // 2
        mul.setInput(0, Signal::State::FALSE);
        mul.setInput(1, Signal::State::FALSE);
        mul.setInput(2, Signal::State::TRUE);
        // -1
        mul.setInput(3, Signal::State::TRUE);
        mul.setInput(4, Signal::State::TRUE);
        mul.setInput(5, Signal::State::FALSE);
        mul.notifyStateChanged();
        REQUIRE(mul.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(mul.getOutput(3).getState() == Signal::State::FALSE);
        REQUIRE(mul.getOutput(4).getState() == Signal::State::FALSE);
    }
}

TEST_CASE("Multiplier Resize Inputs", "[multiplier][resize][unit]")
{
    Multiplier mul;
    mul.setInput(0, Signal::State::TRUE);
    mul.setInput(1, Signal::State::TRUE);
    mul.setInput(2, Signal::State::TRUE);
    mul.setInput(3, Signal::State::TRUE);
    mul.setInput(4, Signal::State::TRUE);
    mul.setInput(5, Signal::State::TRUE);

    mul.resizeInputs(8);
    REQUIRE(mul.getInputSize() == 8);
    REQUIRE(mul.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(3).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(4).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(5).getState() == Signal::State::UNDEFINED);
    REQUIRE(mul.getOutput(6).getState() == Signal::State::UNDEFINED);

    mul.resizeInputs(4);
    REQUIRE(mul.getInputSize() == 4);
    REQUIRE(mul.getOutput(0).getState() == Signal::State::FALSE);
    REQUIRE(mul.getOutput(1).getState() == Signal::State::TRUE);
    REQUIRE(mul.getOutput(2).getState() == Signal::State::FALSE);

    REQUIRE_THROWS(mul.resizeInputs(5));
}

TEST_CASE("Multiplier Resize Outputs", "[multiplier][resize][unit]")
{
    Multiplier mul;
    REQUIRE_THROWS(mul.resizeOutputs(2));
}
