#include "memory/jkflipflop.hpp"

#include "catch.hpp"

TEST_CASE("JKFlipFlop constructor", "[jkff][init][unit]")
{

    SECTION("Default", "Make a JK flip-flop")
    {
        JKFlipFlop jkff;
        REQUIRE(jkff.getInputSize() == 2);
        REQUIRE(jkff.getOutputSize() == 2);
    }
}

TEST_CASE("JKFlipFlop Update", "[jkff][update][unit]")
{
    JKFlipFlop jkff;

    REQUIRE(jkff.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(jkff.getOutput(1).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "Both outputs should be error")
    {
        jkff.setInput(0, Signal::State::ERROR);
        jkff.setInput(1, Signal::State::FALSE);

        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::ERROR);

        jkff.setInput(1, Signal::State::TRUE);

        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::ERROR);

        jkff.setInput(0, Signal::State::FALSE);
        jkff.setInput(1, Signal::State::ERROR);

        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::ERROR);

        jkff.setInput(0, Signal::State::TRUE);

        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Output unchanged", "Output should stay the same whenever both inputs are false")
    {
        jkff.setInput(0, Signal::State::FALSE);
        jkff.setInput(1, Signal::State::FALSE);
        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Positive output", "Output should be positive when the first input is positive")
    {
        jkff.setInput(0, Signal::State::TRUE);
        jkff.setInput(1, Signal::State::FALSE);
        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::FALSE);
    }

    SECTION("Negative output", "Output should be negative when the second input is positive")
    {
        jkff.setInput(0, Signal::State::FALSE);
        jkff.setInput(1, Signal::State::TRUE);
        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::TRUE);
    }

    SECTION("Output toggle", "Output should toggle when both inputs are positive")
    {
        jkff.setInput(0, Signal::State::FALSE);
        jkff.setInput(1, Signal::State::TRUE);
        jkff.notifyStateChanged();
        jkff.setInput(0, Signal::State::TRUE);
        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::FALSE);


        jkff.setInput(1, Signal::State::FALSE);
        jkff.notifyStateChanged();
        jkff.setInput(1, Signal::State::TRUE);
        jkff.notifyStateChanged();
        REQUIRE(jkff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(jkff.getOutput(1).getState() == Signal::State::TRUE);
    }
}

TEST_CASE("JKFlipFlop Resize Inputs", "[jkff][resize][unit]")
{
    JKFlipFlop jkff;
    REQUIRE_THROWS(jkff.resizeInputs(6));

    REQUIRE_NOTHROW(jkff.resizeInputs(2));
}

TEST_CASE("JKFlipFlop Resize Outputs", "[jkff][resize][unit]")
{
    JKFlipFlop jkff;
    REQUIRE_THROWS(jkff.resizeOutputs(4));

    REQUIRE_NOTHROW(jkff.resizeOutputs(2));
}
