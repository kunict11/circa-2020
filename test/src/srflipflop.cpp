#include "memory/srflipflop.hpp"

#include "catch.hpp"

TEST_CASE("SRFlipFlop constructor", "[srff][init][unit]")
{

    SECTION("Default", "Make a SR flip-flop")
    {
        SRFlipFlop srff;
        REQUIRE(srff.getInputSize() == 2);
        REQUIRE(srff.getOutputSize() == 2);
    }
}

TEST_CASE("SRFlipFlop Update", "[srff][update][unit]")
{
    SRFlipFlop srff;

    REQUIRE(srff.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(srff.getOutput(1).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "Both outputs should be error")
    {
        srff.setInput(0, Signal::State::ERROR);
        srff.setInput(1, Signal::State::FALSE);

        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::ERROR);

        srff.setInput(1, Signal::State::TRUE);

        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::ERROR);

        srff.setInput(0, Signal::State::FALSE);
        srff.setInput(1, Signal::State::ERROR);

        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::ERROR);

        srff.setInput(0, Signal::State::TRUE);

        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::ERROR);
    }

    SECTION("Undefined output", "Output should be undefined when both inputs have the same signal")
    {
        srff.setInput(0, Signal::State::FALSE);
        srff.setInput(1, Signal::State::FALSE);
        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::UNDEFINED);

        srff.setInput(0, Signal::State::TRUE);
        srff.setInput(1, Signal::State::TRUE);
        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::UNDEFINED);

    }

    SECTION("Positive output", "Output should be positive when the set input is positive")
    {
        srff.setInput(0, Signal::State::TRUE);
        srff.setInput(1, Signal::State::FALSE);
        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::FALSE);
    }

    SECTION("Negative output", "Output should be negative when the reset input is positive")
    {
        srff.setInput(0, Signal::State::FALSE);
        srff.setInput(1, Signal::State::TRUE);
        srff.notifyStateChanged();
        REQUIRE(srff.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(srff.getOutput(1).getState() == Signal::State::TRUE);
    }
}

TEST_CASE("SRFlipFlop Resize Inputs", "[srff][resize][unit]")
{
    SRFlipFlop srff;
    REQUIRE_THROWS(srff.resizeInputs(6));

    REQUIRE_NOTHROW(srff.resizeInputs(2));
}

TEST_CASE("SRFlipFlop Resize Outputs", "[srff][resize][unit]")
{
    SRFlipFlop srff;
    REQUIRE_THROWS(srff.resizeOutputs(4));

    REQUIRE_NOTHROW(srff.resizeOutputs(2));
}
