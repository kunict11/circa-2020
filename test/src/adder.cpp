#include "arithmetic/adder.hpp"

#include "catch.hpp"

TEST_CASE("Adder Constructor", "[adder][init][unit]")
{

    SECTION("Default", "Make a 6 input adder")
    {
        Adder add;
        REQUIRE(add.getInputSize() == 6);
        REQUIRE(add.getOutputSize() == 4);
    }
}

TEST_CASE("Adder Update", "[adder][update][unit]")
{
    Adder add;

    REQUIRE(add.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(3).getState() == Signal::State::UNDEFINED);

    SECTION("Input error", "One input is error")
    {
        add.setInput(1, Signal::State::ERROR);

        add.setInput(0, Signal::State::FALSE);
        add.setInput(2, Signal::State::FALSE);
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(3).getState() == Signal::State::ERROR);

        add.setInput(0, Signal::State::TRUE);
        add.setInput(2, Signal::State::FALSE);
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(3).getState() == Signal::State::ERROR);

        add.setInput(0, Signal::State::UNDEFINED);
        add.setInput(2, Signal::State::FALSE);
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(1).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(2).getState() == Signal::State::ERROR);
        REQUIRE(add.getOutput(3).getState() == Signal::State::ERROR);
    }

    SECTION("Input undefined", "One input is undefined")
    {
        add.setInput(1, Signal::State::UNDEFINED);

        add.setInput(0, Signal::State::FALSE);
        add.setInput(2, Signal::State::FALSE);
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(3).getState() == Signal::State::UNDEFINED);

        add.setInput(0, Signal::State::TRUE);
        add.setInput(2, Signal::State::FALSE);
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(3).getState() == Signal::State::UNDEFINED);

        add.setInput(0, Signal::State::UNDEFINED);
        add.setInput(2, Signal::State::FALSE);
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(1).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(2).getState() == Signal::State::UNDEFINED);
        REQUIRE(add.getOutput(3).getState() == Signal::State::UNDEFINED);
    }

    SECTION("Two positive numbers on input", "Output should be a positive number")
    {
        // 1
        add.setInput(0, Signal::State::FALSE);
        add.setInput(1, Signal::State::TRUE);
        add.setInput(2, Signal::State::FALSE);
        // 2
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::TRUE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);

        // 3
        add.setInput(0, Signal::State::FALSE);
        add.setInput(1, Signal::State::TRUE);
        add.setInput(2, Signal::State::TRUE);
        // 3
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::TRUE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::TRUE);
    }

    SECTION("Two negative numbers on input", "Output should be a negative number")
    {
        // -2
        add.setInput(0, Signal::State::TRUE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::TRUE);
        // -3
        add.setInput(3, Signal::State::TRUE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::TRUE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::TRUE);

        // -1
        add.setInput(0, Signal::State::TRUE);
        add.setInput(1, Signal::State::TRUE);
        add.setInput(2, Signal::State::FALSE);
        // -1
        add.setInput(3, Signal::State::TRUE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Output is zero", "Both numbers are zero or equal absolute numbers have opposite signs")
    {
        // 0
        add.setInput(0, Signal::State::FALSE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::FALSE);
        // 0
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);

        // -2
        add.setInput(0, Signal::State::TRUE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::TRUE);
        // 2
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::FALSE);
        add.setInput(5, Signal::State::TRUE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);

        // 1
        add.setInput(0, Signal::State::FALSE);
        add.setInput(1, Signal::State::TRUE);
        add.setInput(2, Signal::State::FALSE);
        // -1
        add.setInput(3, Signal::State::TRUE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Positive output, one negative input", "One of the numbers on the input is negative")
    {
        // -2
        add.setInput(0, Signal::State::TRUE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::TRUE);
        // 3
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::TRUE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);

        // 2
        add.setInput(0, Signal::State::FALSE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::TRUE);
        // -1
        add.setInput(3, Signal::State::TRUE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);
    }

    SECTION("Negative output, one negative input", "One of the numbers on the input is negative")
    {
        // 2
        add.setInput(0, Signal::State::FALSE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::TRUE);
        // -3
        add.setInput(3, Signal::State::TRUE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::TRUE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);

        // -2
        add.setInput(0, Signal::State::TRUE);
        add.setInput(1, Signal::State::FALSE);
        add.setInput(2, Signal::State::TRUE);
        // 1
        add.setInput(3, Signal::State::FALSE);
        add.setInput(4, Signal::State::TRUE);
        add.setInput(5, Signal::State::FALSE);
        add.notifyStateChanged();
        REQUIRE(add.getOutput(0).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(1).getState() == Signal::State::TRUE);
        REQUIRE(add.getOutput(2).getState() == Signal::State::FALSE);
        REQUIRE(add.getOutput(3).getState() == Signal::State::FALSE);
    }

}

TEST_CASE("Adder Resize Inputs", "[adder][resize][unit]")
{
    Adder add;
    add.setInput(0, Signal::State::TRUE);
    add.setInput(1, Signal::State::TRUE);
    add.setInput(2, Signal::State::TRUE);
    add.setInput(3, Signal::State::TRUE);
    add.setInput(4, Signal::State::TRUE);
    add.setInput(5, Signal::State::TRUE);

    add.resizeInputs(8);
    REQUIRE(add.getInputSize() == 8);
    REQUIRE(add.getOutput(0).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(1).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(2).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(3).getState() == Signal::State::UNDEFINED);
    REQUIRE(add.getOutput(4).getState() == Signal::State::UNDEFINED);

    add.resizeInputs(4);
    REQUIRE(add.getInputSize() == 4);
    REQUIRE(add.getOutput(0).getState() == Signal::State::TRUE);
    REQUIRE(add.getOutput(1).getState() == Signal::State::FALSE);
    REQUIRE(add.getOutput(2).getState() == Signal::State::TRUE);

    REQUIRE_THROWS(add.resizeInputs(5));
}

TEST_CASE("Adder Resize Outputs", "[adder][resize][unit]")
{
    Adder add;
    REQUIRE_THROWS(add.resizeOutputs(2));
}
