#include "gate/buffer.hpp"

#include "catch.hpp"

TEST_CASE("Buffer Constructor", "[buffer][init][unit]")
{
	Buffer gate;
	REQUIRE(gate.getInputSize() == 1);
	REQUIRE(gate.getOutputSize() == 1);
}

TEST_CASE("Buffer Update", "[buffer][update][unit]")
{
	Buffer gate;

	REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

	SECTION("Input error", "Input is error")
	{
		gate.setInput(0, Signal::State::ERROR);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);
	}

	SECTION("Input undefined", "Input is undefined")
	{
		gate.setInput(0, Signal::State::UNDEFINED);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);
	}

	SECTION("Positive output", "Output should be true")
	{
		gate.setInput(0, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);
	}

	SECTION("Negative output", "Output should be false")
	{
		gate.setInput(0, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);
	}
}

TEST_CASE("Buffer Resize Inputs", "[buffer][resize][unit]")
{
	Buffer gate;
	REQUIRE_THROWS(gate.resizeInputs(2));

    REQUIRE_NOTHROW(gate.resizeInputs(1));
}

TEST_CASE("Buffer Resize Outputs", "[buffer][resize][unit]")
{
	Buffer gate;
	REQUIRE_THROWS(gate.resizeOutputs(2));

    REQUIRE_NOTHROW(gate.resizeOutputs(1));
}
