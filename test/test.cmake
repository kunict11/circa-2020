set(SRC_PATH "${TEST_PATH}/src")

Include(FetchContent)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v2.13.3)

FetchContent_MakeAvailable(Catch2)

list(APPEND CMAKE_MODULE_PATH ${catch2_SOURCE_DIR}/contrib)
list(APPEND CMAKE_MODULE_PATH ${CURRENT_SOURCE_DIR}/CodeCoverage.cmake)

set(SOURCES
    ${SRC_PATH}/buffer.cpp
    ${SRC_PATH}/constant.cpp
    ${SRC_PATH}/main.cpp
    ${SRC_PATH}/andgate.cpp
    ${SRC_PATH}/nandgate.cpp
    ${SRC_PATH}/norgate.cpp
    ${SRC_PATH}/notgate.cpp
    ${SRC_PATH}/orgate.cpp
    ${SRC_PATH}/xorgate.cpp
    ${SRC_PATH}/xnorgate.cpp
    ${SRC_PATH}/pin.cpp
    ${SRC_PATH}/lightbulb.cpp
    ${SRC_PATH}/wire.cpp
    ${SRC_PATH}/compositecomponent.cpp
    ${SRC_PATH}/multiplexer.cpp
    ${SRC_PATH}/demultiplexer.cpp
    ${SRC_PATH}/encoder.cpp
    ${SRC_PATH}/decoder.cpp
    ${SRC_PATH}/adder.cpp
    ${SRC_PATH}/subtractor.cpp
    ${SRC_PATH}/multiplier.cpp
    ${SRC_PATH}/divider.cpp
    ${SRC_PATH}/comparator.cpp
    ${SRC_PATH}/negator.cpp
    ${SRC_PATH}/dflipflop.cpp
    ${SRC_PATH}/tflipflop.cpp
    ${SRC_PATH}/srflipflop.cpp
    ${SRC_PATH}/jkflipflop.cpp
    ${SRC_PATH}/rom.cpp
    ${SRC_PATH}/ram.cpp
)

set(TEST_NAME ${PROJECT_NAME}_test)
add_executable(${TEST_NAME} ${SOURCES})
target_include_directories(${TEST_NAME} PRIVATE
    "${catch2_SOURCE_DIR}/single_include/catch2"
)
target_link_libraries(${TEST_NAME} PRIVATE
    ${LIB_NAME}
    Catch2::Catch2
)

enable_testing()
include(CTest)
include(Catch)
catch_discover_tests(${TEST_NAME})

if(CMAKE_BUILD_TYPE STREQUAL "Coverage")
    include(CodeCoverage)
    set(COVERAGE_EXCLUDES
        "/usr/*"
        "/opt/*"
        "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}_autogen/*"
        "${CMAKE_CURRENT_BINARY_DIR}/_deps/*")

    setup_target_for_coverage_lcov(NAME coverage EXECUTABLE ${TEST_NAME})

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -fprofile-arcs -ftest-coverage")
endif() #CMAKE_BUILD_TYPE STREQUAL "Coverage"
