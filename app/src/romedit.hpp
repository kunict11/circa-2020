#ifndef ROMEDIT_HPP
#define ROMEDIT_HPP

#include <QDialog>
#include<QTableWidget>

#include <vector>
#include "memory/rom.hpp"

namespace Ui
{
class ROMEdit;
}
class ROMEdit : public QDialog
{
    Q_OBJECT
public:
    explicit ROMEdit(ROM* rom, QWidget *parent=nullptr);
    ~ROMEdit() override;

private:
    Ui::ROMEdit *ui;
    std::vector<bool> contentsUpdated;
    ROM* m_rom;

protected slots:
    void onTableItemDoubleClick(QTableWidgetItem* item);
    void acceptChanges();
};

#endif // ROMEDIT_HPP
