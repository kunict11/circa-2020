#include <iostream>

#include <QApplication>

#include "mainwindow.hpp"
#include "../src/component/componentfactory.hpp"
#include "../src/connection/connectionmanager.hpp"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();
}
