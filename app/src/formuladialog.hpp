#ifndef FORMULADIALOG_H
#define FORMULADIALOG_H

#include <QDialog>

namespace Ui
{
class FormulaDialog;
}

class FormulaDialog : public QDialog
{
	Q_OBJECT

public:
	explicit FormulaDialog(std::string formulae, QWidget *parent = nullptr);
	~FormulaDialog() override;

protected slots:
	void onCopyTriggered();
	void onSaveTriggered();
	void onCloseTriggered();

private:
	Ui::FormulaDialog *ui;
};

#endif // FORMULADIALOG_H
