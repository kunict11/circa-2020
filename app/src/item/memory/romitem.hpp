#ifndef ROMITEM_HPP
#define ROMITEM_HPP

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"

class ROMItem : public ComponentGraphicsItem
{
public:
    ROMItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
    ROMItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void resizeInputs(unsigned n) override;
    void drawOrientation() override;
protected:
    int calculateWidth() const;
    int calculateHeight() const;

    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // ROMITEM_HPP
