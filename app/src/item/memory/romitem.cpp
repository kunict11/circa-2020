#include "romitem.hpp"

#include "util/colorutil.hpp"
#include "romedit.hpp"

ROMItem::ROMItem(int posX, int posY, QGraphicsItem *parent)
    :ROMItem(ComponentFactory::instance().makeROM(), posX, posY, parent)
{

}

ROMItem::ROMItem(Component *component, int posX, int posY, QGraphicsItem *parent)
    :ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
    if ((component == nullptr) || component->getType() != ComponentTypes::ROM)
        throw "Incompatible component for this item";
    m_isResizable = true;
    this->resizeInputs(m_component->getInputSize());
}

int ROMItem::calculateHeight() const
{
    auto *rom = static_cast<ROM*>(m_component);
    if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
        return rom->getRowCount() * 4;
    } else {
        return rom->getColCount() * 4;
    }
}

int ROMItem::calculateWidth() const
{
    auto *rom = static_cast<ROM*>(m_component);
    if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
        return rom->getColCount() * 4;
    } else {
        return rom->getRowCount() * 4;
    }
}

void ROMItem::drawOrientation()
{
    prepareGeometryChange();

    m_height = calculateHeight() * Scene::UNIT;
    m_width = calculateWidth() * Scene::UNIT;

    m_inputPoints = {};
    m_outputPoints = {};

    int point_spacing = 3;
    int point_offset = 1;

    if (m_component->getInputSize() % 2 == 1) {
        point_spacing = 4;
        point_offset = 2;
    }

    if (m_orientation == Direction::RIGHT) {
        for (unsigned i = 0; i < m_component->getInputSize()-1; i++) {
            m_inputPoints.append(ConnectionPoint(0, (point_spacing * i + point_offset) * Scene::UNIT, Direction::LEFT));
        }
        m_inputPoints.append(ConnectionPoint(m_width/2, m_height, Direction::DOWN, QString("select")));
        m_outputPoints.append(ConnectionPoint(m_width, m_height/2, Direction::RIGHT));
    } else if (m_orientation == Direction::DOWN) {
        for (unsigned i = 0; i < m_component->getInputSize()-1; i++) {
            m_inputPoints.append(ConnectionPoint((point_spacing * i + point_offset) * Scene::UNIT, 0, Direction::UP));
        }
        m_inputPoints.append(ConnectionPoint(0, m_height/2, Direction::LEFT, QString("select")));
        m_outputPoints.append(ConnectionPoint(m_width/2, m_height, Direction::DOWN));
    } else if (m_orientation == Direction::LEFT) {
        for (unsigned i = 0; i < m_component->getInputSize()-1; i++) {
            m_inputPoints.append(ConnectionPoint(m_width, (point_spacing * i + point_offset) * Scene::UNIT, Direction::RIGHT));
        }
        m_inputPoints.append(ConnectionPoint(m_width/2, 0, Direction::UP, QString("select")));
        m_outputPoints.append(ConnectionPoint(0, m_height/2, Direction::LEFT));
    } else {
        for (unsigned i = 0; i < m_component->getInputSize()-1; i++) {
            m_inputPoints.append(ConnectionPoint((point_spacing * i + point_offset) * Scene::UNIT, m_height, Direction::DOWN));
        }
        m_inputPoints.append(ConnectionPoint(m_width, m_height/2, Direction::RIGHT, QString("select")));
        m_outputPoints.append(ConnectionPoint(m_width/2, 0, Direction::UP));
    }
}

void ROMItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen p = QPen();
    p.setWidth(2);
    painter->setPen(p);
    painter->drawRect(0,0,m_width,m_height);
    auto *rom = static_cast<ROM*>(m_component);
    QBrush b = QBrush(ColorUtil::forState(rom->getContents()[0].getState()));

    for(unsigned i=0; i<rom->getRowCount();i++){
        for(unsigned j=0; j<rom->getColCount();j++){
            b = QBrush(ColorUtil::forState(rom->getContents()[rom->getColCount()*i+j].getState()));
            painter->setBrush(b);
            if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
                painter->drawEllipse(4*j*Scene::UNIT+Scene::UNIT, 4*i*Scene::UNIT+Scene::UNIT, 2*Scene::UNIT, 2*Scene::UNIT);
            else
                painter->drawEllipse(4*i*Scene::UNIT+Scene::UNIT, 4*j*Scene::UNIT+Scene::UNIT, 2*Scene::UNIT, 2*Scene::UNIT);
        }
    }

    ComponentGraphicsItem::paint(painter, option, widget);
}

void ROMItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    auto *rom = static_cast<ROM*>(m_component);

    auto *editWindow = new ROMEdit(rom);
    editWindow->setModal(true);
    editWindow->exec();

    delete editWindow;

    update();
}

void ROMItem::resizeInputs(unsigned n)
{
    m_component->resizeInputs(n);
    drawOrientation();
}
