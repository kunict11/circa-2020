#include "ramitem.hpp"
#include "util/colorutil.hpp"

RAMItem::RAMItem(int posX, int posY, QGraphicsItem *parent)
    :RAMItem(ComponentFactory::instance().makeRAM(), posX, posY, parent)
{

}

RAMItem::RAMItem(Component *component, int posX, int posY, QGraphicsItem *parent)
    :ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
    if ((component == nullptr) || component->getType() != ComponentTypes::RAM)
        throw "Incompatible component for this item";
    m_isResizable = true;
//    this->resizeInputs(m_component->getInputSize());
    drawOrientation();
}

int RAMItem::calculateHeight() const
{
    auto *ram = static_cast<RAM*>(m_component);
    if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
        return ram->getRowCount() * 4;
    } else {
        return ram->getColCount() * 4;
    }
}

int RAMItem::calculateWidth() const
{
    auto *ram = static_cast<RAM*>(m_component);
    if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
        return ram->getColCount() * 4;
    } else {
        return ram->getRowCount() * 4;
    }
}

void RAMItem::drawOrientation()
{
    prepareGeometryChange();

    m_height = calculateHeight() * Scene::UNIT;
    m_width = calculateWidth() * Scene::UNIT;

    m_inputPoints = {};
    m_outputPoints = {};

    const QVector<QString> point_labels {"select", "clock", "load", "reset"};

    int point_spacing = 3;
    int point_offset = 1;

    if (m_component->getInputSize() % 2 == 0) {
        point_spacing = 4;
        point_offset = 2;
    }

    unsigned n_inputs = m_component->getInputSize();
    if (m_orientation == Direction::RIGHT) {
        for (unsigned i = 0; i < n_inputs-4; i++) {
            m_inputPoints.append(ConnectionPoint(0, (point_spacing * i + point_offset) * Scene::UNIT, Direction::LEFT));
        }
        for (unsigned i = 0; i < 4; i++) {
            m_inputPoints.append(ConnectionPoint((2 * i +1) * Scene::UNIT, m_height, Direction::DOWN, point_labels.at(i)));
        }
        m_outputPoints.append(ConnectionPoint(m_width, m_height/2, Direction::RIGHT));
    } else if (m_orientation == Direction::DOWN) {
        for (unsigned i = 0; i <n_inputs-4; i++) {
            m_inputPoints.append(ConnectionPoint((point_spacing * i + point_offset) * Scene::UNIT, 0, Direction::UP));
        }
        for (unsigned i = 0; i < 4; i++) {
            m_inputPoints.append(ConnectionPoint(0, (2 * i +1) * Scene::UNIT, Direction::LEFT, point_labels.at(i)));
        }
        m_outputPoints.append(ConnectionPoint(m_width/2, m_height, Direction::DOWN));
    } else if (m_orientation == Direction::LEFT) {
        for (unsigned i = 0; i < n_inputs-4; i++) {
            m_inputPoints.append(ConnectionPoint(m_width, (point_spacing * i + point_offset) * Scene::UNIT, Direction::RIGHT));
        }
        for (unsigned i = 0; i < 4; i++) {
            m_inputPoints.append(ConnectionPoint((2 * i +1) * Scene::UNIT, 0, Direction::UP, point_labels.at(i)));
        }
        m_outputPoints.append(ConnectionPoint(0, m_height/2, Direction::LEFT));
    } else {
        for (unsigned i = 0; i < n_inputs-4; i++) {
            m_inputPoints.append(ConnectionPoint((point_spacing * i + point_offset) * Scene::UNIT, m_height, Direction::DOWN));
        }
        for (unsigned i = 0; i < 4; i++) {
            m_inputPoints.append(ConnectionPoint(m_width, (2 * i +1) * Scene::UNIT, Direction::RIGHT, point_labels.at(i)));
        }
        m_outputPoints.append(ConnectionPoint(m_width/2, 0, Direction::UP));
    }
}

void RAMItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen p = QPen();
    p.setWidth(2);
    painter->setPen(p);
    painter->drawRect(0,0,m_width,m_height);
    auto *ram = static_cast<RAM*>(m_component);
    QBrush b = QBrush(ColorUtil::forState(ram->getContents()[0].getState()));

    for(unsigned i=0; i<ram->getRowCount();i++){
        for(unsigned j=0; j<ram->getColCount();j++){
            b = QBrush(ColorUtil::forState(ram->getContents()[ram->getColCount()*i+j].getState()));
            painter->setBrush(b);
            if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
                painter->drawEllipse(4*j*Scene::UNIT+Scene::UNIT, 4*i*Scene::UNIT+Scene::UNIT, 2*Scene::UNIT, 2*Scene::UNIT);
            else
                painter->drawEllipse(4*i*Scene::UNIT+Scene::UNIT, 4*j*Scene::UNIT+Scene::UNIT, 2*Scene::UNIT, 2*Scene::UNIT);
        }
    }

    ComponentGraphicsItem::paint(painter, option, widget);
}


void RAMItem::resizeInputs(unsigned n)
{
    m_component->resizeInputs(n);
    drawOrientation();
}
