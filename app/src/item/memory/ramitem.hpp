#ifndef RAMITEM_HPP
#define RAMITEM_HPP

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"

class RAMItem : public ComponentGraphicsItem
{
public:
    RAMItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
    RAMItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void resizeInputs(unsigned n) override;
    void drawOrientation() override;
protected:
    int calculateWidth() const;
    int calculateHeight() const;
};

#endif // RAMITEM_HPP
