#include "item/memory/dflipflopitem.hpp"

DFlipFlopItem::DFlipFlopItem(int posX, int posY, QGraphicsItem *parent)
	: DFlipFlopItem(ComponentFactory::instance().makeDFlipFlop(), posX, posY, parent)
{
}

DFlipFlopItem::DFlipFlopItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::DFLIPFLOP)
		throw "Incompatible component for this item";
	m_isResizable = false;
	this->resizeInputs(m_component->getInputSize());
}

int DFlipFlopItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return (m_component->getInputSize() - 2) * 2;
	else
		return 4;
}

int DFlipFlopItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return 4;
	else
		return (m_component->getInputSize() - 2) * 2;
}

void DFlipFlopItem::drawOrientation()
{
	prepareGeometryChange();

	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();
	m_inputPoints = {};
	m_outputPoints = {};

	if (m_orientation == Direction::RIGHT) {
		m_inputPoints = {
            ConnectionPoint(m_width / 2, 0, Direction::UP, "Set"),
            ConnectionPoint(m_width / 2, m_height, Direction::DOWN, "Reset"),
            ConnectionPoint(0, Scene::UNIT, Direction::LEFT, "Clock"),
			ConnectionPoint(0, 3 * Scene::UNIT, Direction::LEFT)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));
		}
	} else if (m_orientation == Direction::DOWN) {
		m_inputPoints = {
            ConnectionPoint(0, m_height / 2, Direction::LEFT, "Set"),
            ConnectionPoint(m_width, m_height / 2, Direction::RIGHT, "Reset"),
            ConnectionPoint(Scene::UNIT, 0, Direction::UP, "Clock"), ConnectionPoint(3 * Scene::UNIT, 0, Direction::UP)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));
		}
	} else if (m_orientation == Direction::LEFT) {
		m_inputPoints = {
            ConnectionPoint(m_width / 2, 0, Direction::UP, "Set"),
            ConnectionPoint(m_width / 2, m_height, Direction::DOWN, "Reset"),
            ConnectionPoint(m_width, Scene::UNIT, Direction::RIGHT, "Clock"),
			ConnectionPoint(m_width, 3 * Scene::UNIT, Direction::RIGHT)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));
		}
	} else {
		m_inputPoints = {
            ConnectionPoint(0, m_height / 2, Direction::LEFT, "Set"),
            ConnectionPoint(m_width, m_height / 2, Direction::RIGHT, "Reset"),
            ConnectionPoint(Scene::UNIT, m_height, Direction::DOWN, "Clock"),
			ConnectionPoint(3 * Scene::UNIT, m_height, Direction::DOWN)};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));
		}
	}
}

void DFlipFlopItem::resizeInputs(unsigned n)
{
	this->m_component->resizeInputs(n);
	drawOrientation();
}

void DFlipFlopItem::calculatePath()
{
	m_path = QPainterPath(QPoint(0, 0));
	m_path.lineTo(0, m_height);
	m_path.lineTo(m_width, m_height);
	m_path.lineTo(m_width, 0);
	m_path.lineTo(0, 0);
	m_path.addText(QPoint(Scene::UNIT, Scene::UNIT * 2), QFont("Times", 20), QString("D"));
	if (m_orientation == Direction::RIGHT) {
		m_path.addText(QPoint(Scene::UNIT / 2, Scene::UNIT), QFont("Times", 15), QString("."));
	} else if (m_orientation == Direction::DOWN) {
		m_path.addText(QPoint(Scene::UNIT / 2, Scene::UNIT), QFont("Times", 15), QString("."));
	} else if (m_orientation == Direction::LEFT) {
		m_path.addText(QPoint(m_width - Scene::UNIT, Scene::UNIT), QFont("Times", 15), QString("."));
	} else {
		m_path.addText(QPoint(Scene::UNIT / 2, m_height - Scene::UNIT), QFont("Times", 15), QString("."));
	}
}

void DFlipFlopItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	painter->setBrush(b);

	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
