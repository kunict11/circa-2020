#include "multiplexeritem.hpp"

MultiplexerItem::MultiplexerItem(int posX, int posY, QGraphicsItem *parent)
	: MultiplexerItem(ComponentFactory::instance().makeMultiplexer(), posX, posY, parent)
{
}

MultiplexerItem::MultiplexerItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::MULTIPLEXER)
		throw "Incompatible component for this item";
	m_isResizable = true;
	auto *mux = static_cast<Multiplexer *>(this->m_component);
	this->resizeInputs(m_component->getInputSize() - mux->m_num_of_select_inputs);
}

int MultiplexerItem::calculateHeight() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
		auto *mux = static_cast<Multiplexer *>(this->m_component);
		return (m_component->getInputSize() - mux->m_num_of_select_inputs) * 2;
	} else {
		auto *mux = static_cast<Multiplexer *>(this->m_component);
		if (mux->m_num_of_select_inputs * 2 < 5)
			return 5;
		return mux->m_num_of_select_inputs * 2;
	}
}

int MultiplexerItem::calculateWidth() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
		auto *mux = static_cast<Multiplexer *>(this->m_component);
		if (mux->m_num_of_select_inputs * 2 < 5)
			return 5;
		return mux->m_num_of_select_inputs * 2;
	} else {
		auto *mux = static_cast<Multiplexer *>(this->m_component);
		return (m_component->getInputSize() - mux->m_num_of_select_inputs) * 2;
	}
}

void MultiplexerItem::drawOrientation()
{
	prepareGeometryChange();

	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();
	m_inputPoints = {};
	m_outputPoints = {};

	auto *mux = static_cast<Multiplexer *>(this->m_component);

	if (m_orientation == Direction::RIGHT) {
		for (unsigned i = 0; i < m_component->getInputSize() - mux->m_num_of_select_inputs; i++)
			m_inputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));

		for (unsigned i = 0; i < mux->m_num_of_select_inputs; i++)
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

		m_outputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};

	} else if (m_orientation == Direction::DOWN) {
		for (int i = m_component->getInputSize() - mux->m_num_of_select_inputs - 1; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));

		for (int i = mux->m_num_of_select_inputs - 1; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));

		m_outputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};

	} else if (m_orientation == Direction::LEFT) {
		for (int i = m_component->getInputSize() - mux->m_num_of_select_inputs - 1; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));

		for (int i = mux->m_num_of_select_inputs - 1; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

		m_outputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};

	} else {
		for (int i = m_component->getInputSize() - mux->m_num_of_select_inputs - 1; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

		for (int i = mux->m_num_of_select_inputs - 1; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));

		m_outputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};
	}
}

void MultiplexerItem::resizeInputs(unsigned n)
{
	this->m_component->resizeInputs(n);
	drawOrientation();
}

void MultiplexerItem::calculatePath()
{
	m_path = QPainterPath(QPoint(0, 0));
	m_path.lineTo(0, m_height);
	m_path.lineTo(m_width, m_height);
	m_path.lineTo(m_width, 0);
	m_path.lineTo(0, 0);
    m_path.addText(QPoint(Scene::UNIT, Scene::UNIT * 2), QFont("Times", 9), QString("MUX"));
}

void MultiplexerItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	painter->setBrush(b);

	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
