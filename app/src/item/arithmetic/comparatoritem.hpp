#ifndef COMPARATORITEM_H
#define COMPARATORITEM_H

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"

class ComparatorItem : public ComponentGraphicsItem
{
public:
	ComparatorItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	ComparatorItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n);
	void drawOrientation() override;

protected:
	int calculateWidth() const;
	int calculateHeight() const;
	void calculatePath();

	QPainterPath m_path;
};

#endif // COMPARATORITEM_H
