#include "item/gate/xnorgateitem.hpp"

XnorGateItem::XnorGateItem(int posX, int posY, QGraphicsItem *parent)
	: XnorGateItem(ComponentFactory::instance().makeXnorGate(), posX, posY, parent)
{
}

XnorGateItem::XnorGateItem(Component *component, int posX, int posY, QGraphicsItem *parent)
    : GateItem(component, posX, posY, 0, 0, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::XNORGATE)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getInputSize());
}

int XnorGateItem::calculateHeight() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 0.75) / Scene::UNIT + 3;
}

int XnorGateItem::calculateWidth() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
		return int(m_height * 0.75) / Scene::UNIT + 3;
	else
		return m_component->getInputSize() * 2;
}

void XnorGateItem::calculatePath()
{
}

void XnorGateItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	QPainterPath arc(QPoint(0, 0));

	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
		arc.arcTo(-m_width / 4, 0, m_width / 2, m_height, 90, -180);

		m_path = QPainterPath(QPoint(0, 0));
		m_path.moveTo(m_width / 4, 0);
		m_path.arcTo(-m_width / 8, 0, m_width / 2, m_height, 90, -180);
		m_path.arcTo(0, 0, m_width - Scene::UNIT - 5, m_height, -90, 180);
		m_path.lineTo(m_width / 4, 0);
		m_path.addEllipse(m_width - Scene::UNIT - 5, m_height / 2 - Scene::UNIT / 2, Scene::UNIT, Scene::UNIT);
	} else {
		m_path = QPainterPath(QPoint(0, m_height));
		arc.moveTo(0, m_height);
		arc.arcTo(0, 3 * (int)m_height / 4, m_width, m_height / 2, 180, -180);

		m_path.moveTo(0, 3 * (int)m_height / 4);
		m_path.arcTo(0, 5 * (int)m_height / 8, m_width, m_height / 2, 180, -180);
		m_path.arcTo(0, 3 * Scene::UNIT / 2, m_width, m_height, 0, 180);
		m_path.lineTo(0, 3 * (int)m_height / 4);
		m_path.addEllipse(m_width / 2 - Scene::UNIT / 2, Scene::UNIT / 2, Scene::UNIT, Scene::UNIT);
	}

	if (m_orientation == Direction::LEFT || m_orientation == Direction::DOWN) {
		QTransform t;
		t.rotate(180);
		t.translate(-m_width, -m_height);
		arc = t.map(arc);
		m_path = t.map(m_path);
	}

	painter->drawPath(arc);
	painter->setBrush(b);
	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
