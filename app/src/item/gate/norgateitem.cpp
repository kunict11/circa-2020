#include "item/gate/norgateitem.hpp"

NorGateItem::NorGateItem(int posX, int posY, QGraphicsItem *parent)
	: NorGateItem(ComponentFactory::instance().makeNorGate(), posX, posY, parent)
{
}

NorGateItem::NorGateItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: GateItem(component, posX, posY, 0, 0, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::NORGATE)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(this->m_component->getInputSize());
}

int NorGateItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 0.75) / Scene::UNIT + 2;
}

int NorGateItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return int(m_height * 0.75) / Scene::UNIT + 2;
	else
		return m_component->getInputSize() * 2;
}

void NorGateItem::calculatePath()
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.arcTo(-(int)m_width / 4, 0, m_width / 2, m_height, 90, -180);
		m_path.arcTo(-m_width + 3 * Scene::UNIT / 2, 0, 2 * m_width - 3 * Scene::UNIT, m_height, -90, 180);
		m_path.addEllipse(
			m_width - (3.0 / 2) * Scene::UNIT, m_height / 2 - Scene::UNIT / 2, Scene::UNIT, Scene::UNIT);

	} else {
		m_path = QPainterPath(QPoint(0, m_height));
		m_path.arcTo(0, 3 * (int)m_height / 4, m_width, m_height / 2, 180, -180);
		m_path.arcTo(m_width, 3 * Scene::UNIT / 2, -m_width, m_height, -180, -180);
		m_path.lineTo(0, m_height);
		m_path.addEllipse(m_width / 2 - Scene::UNIT / 2, Scene::UNIT / 2, Scene::UNIT, Scene::UNIT);
	}

	if (m_orientation == Direction::LEFT || m_orientation == Direction::DOWN) {
		QTransform t;
		t.rotate(180);
		t.translate(-m_width, -m_height);
		m_path = t.map(m_path);
	}
}
