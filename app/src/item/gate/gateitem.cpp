#include "item/gate/gateitem.hpp"

GateItem::GateItem(
	Component *component, int posX, int posY, int width, int height, QGraphicsItem *parent, Direction orient)
	: ComponentGraphicsItem(posX, posY, width, height, component, parent, orient)
{
}

void GateItem::drawOrientation()
{
	prepareGeometryChange();

	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();

	m_inputPoints = {};
	m_outputPoints = {};

	if (m_orientation == Direction::RIGHT) {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));
		}
		m_outputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};

	} else if (m_orientation == Direction::DOWN) {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));
		}
		m_outputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};

	} else if (m_orientation == Direction::LEFT) {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));
		}
		m_outputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};

	} else {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));
		}
		m_outputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};
	}
}

void GateItem::resizeInputs(unsigned n)
{
	this->m_component->resizeInputs(n);
	drawOrientation();
}

void GateItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	painter->setBrush(b);

	// Doesn't work
	//    if(m_orientation == Direction::RIGHT){
	//        painter->rotate(0);
	//    }else if(m_orientation == Direction::DOWN){
	//        painter->rotate(-90);
	//    }else if(m_orientation == Direction::LEFT){
	//        painter->rotate(-180);
	//    }else{
	//        painter->rotate(-270);
	//    }

	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
