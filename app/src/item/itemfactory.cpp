#include "itemfactory.hpp"

#include "item/arithmetic/adderitem.hpp"
#include "item/arithmetic/comparatoritem.hpp"
#include "item/arithmetic/divideritem.hpp"
#include "item/arithmetic/multiplieritem.hpp"
#include "item/arithmetic/negatoritem.hpp"
#include "item/arithmetic/subtractoritem.hpp"
#include "item/compositeitem.hpp"
#include "item/gate/andgateitem.hpp"
#include "item/gate/bufferitem.hpp"
#include "item/gate/nandgateitem.hpp"
#include "item/gate/norgateitem.hpp"
#include "item/gate/notgateitem.hpp"
#include "item/gate/orgateitem.hpp"
#include "item/gate/xnorgateitem.hpp"
#include "item/gate/xorgateitem.hpp"
#include "item/memory/dflipflopitem.hpp"
#include "item/memory/jkflipflopitem.hpp"
#include "item/memory/srflipflopitem.hpp"
#include "item/memory/tflipflopitem.hpp"
#include "item/memory/romitem.hpp"
#include "item/memory/ramitem.hpp"
#include "item/plexer/decoderitem.hpp"
#include "item/plexer/demultiplexeritem.hpp"
#include "item/plexer/encoderitem.hpp"
#include "item/plexer/multiplexeritem.hpp"
#include "item/wiring/clockitem.hpp"
#include "item/wiring/constantitem.hpp"
#include "item/wiring/lightbulbitem.hpp"
#include "item/wiring/pinitem.hpp"
#include "item/wiring/splitteritem.hpp"
#include "item/wiring/wireitem.hpp"

ItemFactory &ItemFactory::Instance()
{
	static ItemFactory Instance;
	return Instance;
}

ItemFactory::ItemFactory() = default;

ComponentGraphicsItem *ItemFactory::make(ComponentTypes type)
{
	switch (type) {
	case ComponentTypes::PIN:
		return new PinItem();
	case ComponentTypes::LIGHTBULB:
		return new LightBulbItem();
	case ComponentTypes::WIRE:
		return new WireItem();
	case ComponentTypes::CLOCK:
		return new ClockItem();
	case ComponentTypes::NOTGATE:
		return new NotGateItem();
	case ComponentTypes::ANDGATE:
		return new AndGateItem();
	case ComponentTypes::ORGATE:
		return new OrGateItem();
	case ComponentTypes::XORGATE:
		return new XorGateItem();
	case ComponentTypes::CONSTANT:
		return new ConstantItem();
	case ComponentTypes::NANDGATE:
		return new NandGateItem();
	case ComponentTypes::NORGATE:
		return new NorGateItem();
	case ComponentTypes::XNORGATE:
		return new XnorGateItem();
	case ComponentTypes::BUFFER:
		return new BufferItem();
	case ComponentTypes::MULTIPLEXER:
		return new MultiplexerItem();
	case ComponentTypes::DEMULTIPLEXER:
		return new DemultiplexerItem();
	case ComponentTypes::ENCODER:
		return new EncoderItem();
	case ComponentTypes::DECODER:
		return new DecoderItem();
	case ComponentTypes::COMPARATOR:
		return new ComparatorItem();
	case ComponentTypes::ADDER:
		return new AdderItem();
	case ComponentTypes::SUBTRACTOR:
		return new SubtractorItem();
	case ComponentTypes::MULTIPLIER:
		return new MultiplierItem();
	case ComponentTypes::DIVIDER:
		return new DividerItem();
	case ComponentTypes::NEGATOR:
		return new NegatorItem();
	case ComponentTypes::SPLITTER:
		return new SplitterItem();
	case ComponentTypes::SRFLIPFLOP:
		return new SRFlipFlopItem();
	case ComponentTypes::JKFLIPFLOP:
		return new JKFlipFlopItem();
	case ComponentTypes::TFLIPFLOP:
		return new TFlipFlopItem();
	case ComponentTypes::DFLIPFLOP:
		return new DFlipFlopItem();
    case ComponentTypes::ROM:
        return new ROMItem();
    case ComponentTypes::RAM:
        return new RAMItem();
	case ComponentTypes::COMPOSITE:
		return new CompositeItem(0, 0, std::vector<Pin *>(), std::vector<LightBulb *>(), "");
	}
	throw "Not implemented";
}

ComponentGraphicsItem *ItemFactory::make(Component *component)
{
	switch (component->getType()) {
	case ComponentTypes::PIN:
		return new PinItem(0, 0, 2, component);
	case ComponentTypes::LIGHTBULB:
		return new LightBulbItem(0, 0, 2, component);
	case ComponentTypes::WIRE:
		return new WireItem(0, 0, 0, 0, static_cast<Wire *>(component), Direction::RIGHT);
	case ComponentTypes::CLOCK:
		return new ClockItem();
	case ComponentTypes::NOTGATE:
		return new NotGateItem(component);
	case ComponentTypes::ANDGATE:
		return new AndGateItem(component);
	case ComponentTypes::ORGATE:
		return new OrGateItem(component);
	case ComponentTypes::XORGATE:
		return new XorGateItem(component);
	case ComponentTypes::CONSTANT:
		return new ConstantItem(0, 0, 2, component);
	case ComponentTypes::NANDGATE:
		return new NandGateItem(component);
	case ComponentTypes::NORGATE:
		return new NorGateItem(component);
	case ComponentTypes::XNORGATE:
		return new XnorGateItem(component);
	case ComponentTypes::BUFFER:
		return new BufferItem(component);
	case ComponentTypes::MULTIPLEXER:
		return new MultiplexerItem(component);
	case ComponentTypes::DEMULTIPLEXER:
		return new DemultiplexerItem(component);
	case ComponentTypes::ENCODER:
		return new EncoderItem(component);
	case ComponentTypes::DECODER:
		return new DecoderItem(component);
	case ComponentTypes::COMPARATOR:
		return new ComparatorItem(component);
	case ComponentTypes::ADDER:
		return new AdderItem(component);
	case ComponentTypes::SUBTRACTOR:
		return new SubtractorItem(component);
	case ComponentTypes::MULTIPLIER:
		return new MultiplierItem(component);
	case ComponentTypes::DIVIDER:
		return new DividerItem(component);
	case ComponentTypes::NEGATOR:
		return new NegatorItem(component);
	case ComponentTypes::SPLITTER:
		return new SplitterItem(component);
	case ComponentTypes::SRFLIPFLOP:
		return new SRFlipFlopItem(component);
	case ComponentTypes::JKFLIPFLOP:
		return new JKFlipFlopItem(component);
	case ComponentTypes::TFLIPFLOP:
		return new TFlipFlopItem(component);
	case ComponentTypes::DFLIPFLOP:
		return new DFlipFlopItem(component);
    case ComponentTypes::ROM:
        return new ROMItem(component);
    case ComponentTypes::RAM:
        return new RAMItem(component);
	case ComponentTypes::COMPOSITE:
		return new CompositeItem(component);
	}
	throw "Not implemented";
}
