#include "componentgraphicsitem.hpp"

#include <QMessageBox>
#include <QToolTip>

#include "item/wiring/wireitem.hpp"
#include "mainwindow.hpp"

ComponentGraphicsItem::~ComponentGraphicsItem() = default;

ComponentGraphicsItem::ComponentGraphicsItem(
	int posX, int posY, int width, int height, Component *component, QGraphicsItem *parent, Direction orient)
	: QGraphicsItem(parent), m_component(component), m_width(width * Scene::UNIT), m_height(height * Scene::UNIT),
	  m_isHolding(false), m_isHovering(false), m_orientation(orient)
{
	outlinePen.setColor(Qt::black);
	outlinePen.setWidth(2);
	fillBrush.setStyle(Qt::SolidPattern);
	fillBrush.setColor(Qt::white);

	setAcceptHoverEvents(true);

	setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges);

	setPos(posX * Scene::UNIT, posY * Scene::UNIT);

	m_wireItemCache = {};
	m_wireItemCacheValid = false;
	m_isInSelection = false;
}

QRectF ComponentGraphicsItem::boundingRect() const
{
	return QRectF(
		-Scene::POINT_SIZE, -Scene::POINT_SIZE, m_width + 2 * Scene::POINT_SIZE, m_height + 2 * Scene::POINT_SIZE);
}

QRectF ComponentGraphicsItem::overlapBoundingRect() const
{
	return QRectF(0, 0, m_width, m_height);
}

QRectF ComponentGraphicsItem::overlapBoundingRectSc() const
{
	return overlapBoundingRect().translated(scenePos());
}

void ComponentGraphicsItem::resizeInputs(unsigned n)
{
	Q_UNUSED(n);
}

void ComponentGraphicsItem::safeResize(unsigned n)
{
	try {

		unsigned currentNumInputs = m_component->getInputSize();
		if (currentNumInputs == n)
			return;
		ConnectionManager::Instance().disconnectInputs(m_component);
		ConnectionManager::Instance().disconnectOutputs(m_component);
		resizeInputs(n);
		auto safePosOpt = MiscUtils::getClosestAvailablePosition(
			getScene(), overlapBoundingRectSc(), {MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(this)});
		if (!safePosOpt) {
			resizeInputs(currentNumInputs);
			QMessageBox::critical(
				getScene()->getMainWindow(), "Resizing component inputs.",
				"Couldn't find a free spot on the board.");
			return;
		}
		setPos(safePosOpt.value());

	} catch (const char *) {
		// Shouldn't be possible from the UI perspective, but doesn't hurt
	}
}

void ComponentGraphicsItem::safeRotate(const Direction &orient)
{
	Direction currentOrient = m_orientation;
	changeOrientation(orient);

	auto safePos = MiscUtils::getClosestAvailablePosition(
		getScene(), overlapBoundingRectSc(), {MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(this)});
	if (!safePos) {
		// Eh: This will kill all connections even if rotation is unsucessful :(
		QMessageBox::critical(
			getScene()->getMainWindow(), "Rotating a component", "Couldn't find a free spot on the board.");
		changeOrientation(currentOrient);
		return;
	}

	setPos(safePos.value());
}

void ComponentGraphicsItem::fillWireItemCache()
{
	if (m_wireItemCacheValid)
		return;

	m_wireItemCache.clear(); // just in case

	auto inputsBegin = std::begin(m_component->getInputConnections());
	auto inputsEnd = std::end(m_component->getInputConnections());
	auto outputsBegin = std::begin(m_component->getOutputConnections());
	auto outputsEnd = std::end(m_component->getOutputConnections());

    for (const auto& scitem : getScene()->getItems()) {
        if (scitem->m_component->getType() != ComponentTypes::WIRE)
			continue;

		auto searchInputs = std::find_if(
            inputsBegin, inputsEnd, [scitem](auto conn) { return conn && conn.to_component == scitem->m_component; });

		if (searchInputs != inputsEnd) {
			m_wireItemCache.emplace_back(
                static_cast<unsigned>(searchInputs - inputsBegin), false, static_cast<WireItem *>(scitem));
			continue;
		}
        auto searchOutputs = std::find_if(outputsBegin, outputsEnd, [scitem](auto conn) {
            return conn && conn.to_component == scitem->m_component;
		});
		if (searchOutputs != outputsEnd) {
			m_wireItemCache.emplace_back(
                static_cast<unsigned>(searchOutputs - outputsBegin), true, static_cast<WireItem *>(scitem));
		}
	}

	m_wireItemCacheValid = true;
}

void ComponentGraphicsItem::processMousePressed(QGraphicsSceneMouseEvent *event)
{
	std::string tag = "[" + m_component->short_identify() + "][DESIGN]";
	std::string nl = '\n' + std::string(tag.size(), ' ');

	this->setCursor(Qt::ClosedHandCursor);
	m_isHolding = true;
	m_previousClickPoint = event->scenePos();
}

void ComponentGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
	Q_UNUSED(event);
	m_isHovering = true;
	update(boundingRect());
}

void ComponentGraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
	Q_UNUSED(event);
	m_isHovering = false;
	update(boundingRect());
}

void ComponentGraphicsItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{

	for (auto &point : m_inputPoints) {
		if (point.getBounds().contains(event->pos())) {
			if (!point.hasFocus()) {
				point.setFocus(true);
                QToolTip::showText(event->screenPos(),point.getLabel());
				update(point.getBounds());
			}
		} else {
			if (point.hasFocus()) {
				point.setFocus(false);
                QToolTip::hideText();
				update(point.getBounds());
			}
		}
	}

	for (auto &point : m_outputPoints) {
		if (point.getBounds().contains(event->pos())) {
			if (!point.hasFocus()) {
				point.setFocus(true);
				update(point.getBounds());
			}
		} else {
			if (point.hasFocus()) {
				point.setFocus(false);
				update(point.getBounds());
			}
		}
	}
}

void ComponentGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	auto mode = getScene()->controlMode();
	if (mode == ControlMode::DESIGN) {
		designModeMousePressEvent(event);
	} else if (mode == ControlMode::SELECTION) {
		selectionModeMousePressEvent(event);
	}
}

void ComponentGraphicsItem::designModeMousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (!getScene()->selectedItems().empty()) {
		getScene()->selectedItems().first()->setSelected(false);
	}
	setSelected(true);
	if (m_component->getType() != ComponentTypes::COMPOSITE) {
		emit getScene()->toggleBox(
			false, "orientation", m_component->getInputSize(), DirectionUtil::toInt(m_orientation));
	} else {
		emit getScene()->toggleBox(true, "orientation", 0, 0);
	}
	if (m_isResizable) {
		emit getScene()->toggleBox(
			false, "input", m_component->getInputSize(), DirectionUtil::toInt(m_orientation));
	} else {
		emit getScene()->toggleBox(true, "input", 0, 0);
	}

	processMousePressed(event);
}

void ComponentGraphicsItem::selectionModeMousePressEvent(QGraphicsSceneMouseEvent *event)
{
	Q_UNUSED(event);
	auto *scene = getScene();
	auto tag = "[" + m_component->short_identify() + "][SELECTION]";
	if (!scene->m_select_info.holding_ctrl) {
		if (scene->m_select_info.selection.empty() || !m_isInSelection) {
			std::cerr << tag << " Clicking a non-selected item without holding ctrl - nothing." << std::endl;
			scene->m_select_info.handled = true;
			return;
		}
		std::cerr << tag << " Clicking on a selected item without holding ctrl - initiate movement" << std::endl;
		scene->m_select_info.selection_is_moving = true;
		scene->m_select_info.outside_connections_terminated = false;
		m_previousClickPoint = scenePos();

		scene->m_select_info.handled = true;
		return;
	}

	if (!m_isInSelection) {
		std::cerr << tag << " Adding to selection" << std::endl;
		scene->m_select_info.selection.insert(this);
	} else {
		std::cerr << tag << " Removing from selection" << std::endl;
		scene->m_select_info.selection.erase(this);
	}
	scene->m_select_info.handled = true;
	;
	m_isInSelection = !m_isInSelection;
	update(boundingRect());
}

void ComponentGraphicsItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

	auto mode = getScene()->controlMode();
	if (mode == ControlMode::DESIGN) {
		designModeMouseMoveEvent(event);
	} else if (mode == ControlMode::SELECTION) {
		selectionModeMouseMoveEvent(event);
	}
}

bool ComponentGraphicsItem::inCache(ComponentGraphicsItem *item)
{
	auto cacheBegin = std::begin(m_wireItemCache);
	auto cacheEnd = std::end(m_wireItemCache);

	auto find = std::find_if(
		cacheBegin, cacheEnd, [item](WireItemCacheEntry entry) { return entry.m_to_wireitem == item; });
	return find != cacheEnd;
}

void ComponentGraphicsItem::designModeMouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if (!m_isHolding)
		return;
	auto tag = "[" + m_component->short_identify() + "]";

	fillWireItemCache();
	if (m_wireItemCache.empty()) {
		int p_posX = Scene::mapToGrid(event->scenePos().x() - m_width / 2);
		int p_posY = Scene::mapToGrid(event->scenePos().y() - m_height / 2);

		if (!MiscUtils::checkPotentialPosition(
				getScene(), this, event->scenePos().x() - m_width / 2 - scenePos().x(),
				event->scenePos().y() - m_height / 2 - scenePos().y(),
				MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(this))) {
			std::cerr << tag << " Detected overlap with some items, rejecting movement." << std::endl;
			return;
		}

		setPos(p_posX, p_posY);
		getScene()->setChanged(true);
		return;
	}

	auto gridDelta = MiscUtils::getGridDelta(m_previousClickPoint, event->scenePos());
	if (!gridDelta) {
		return;
	}
	int gridDeltaX = gridDelta->x();
	int gridDeltaY = gridDelta->y();
	Direction movingDirection;
	if (std::abs(gridDeltaX) >= std::abs(gridDeltaY)) {
		movingDirection = gridDeltaX > 0 ? Direction::RIGHT : Direction::LEFT;
	} else {
		movingDirection = gridDeltaY > 0 ? Direction::DOWN : Direction::UP;
	}

	if (!MiscUtils::checkPotentialPosition(
			getScene(), this, gridDeltaX, gridDeltaY,
			{MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(this), [this](auto it) { return !this->inCache(it); }})) {
		std::cerr << tag << " Detected overlap with some (non-connected) items, rejecting movement." << std::endl;
		return;
	}

	std::vector<WireItemCacheEntryDelayedAction> actions;
	auto cache_current = std::begin(m_wireItemCache);
	while (cache_current != std::end(m_wireItemCache)) {
		auto mIndex = cache_current->m_idx;
		auto mIsOutput = cache_current->m_is_output;
		auto *mWireItem = cache_current->m_to_wireitem;

		if (!DirectionUtil::sameOrientation(mWireItem->getDirection(), movingDirection)) {
			if (mIsOutput)
				ConnectionManager::Instance().disconnectOutput(m_component, mIndex);
			else
				ConnectionManager::Instance().disconnectInput(m_component, mIndex);
			std::cerr << tag << " Erasing " << cache_current->m_to_wireitem->m_component->short_identify()
					  << std::endl;
			cache_current = m_wireItemCache.erase(cache_current);
			continue;
		}
		int mWireNewPosX = mWireItem->pos().x();
		int mWireNewPosY = mWireItem->pos().y();
		int mWireNewWidth = mWireItem->m_width;
		int mWireNewHeight = mWireItem->m_height;

		bool checkWidth = movingDirection == Direction::LEFT || movingDirection == Direction::RIGHT;
		if (movingDirection == Direction::RIGHT) {
			if (scenePos().x() <= mWireItem->scenePos().x()) {
				// Moving right INTO a wire (that's right of me)
				mWireNewPosX += Scene::UNIT;
				mWireNewWidth -= Scene::UNIT;
			} else {
				// Moving right AWAY from a wire (that's left of me)
				mWireNewWidth += Scene::UNIT;
			}
		} else if (movingDirection == Direction::LEFT) {
			if (scenePos().x() <= mWireItem->scenePos().x()) {
				// Moving left AWAY from a wire (that's right of me)
				mWireNewPosX -= Scene::UNIT;
				mWireNewWidth += Scene::UNIT;
			} else {
				// Moving left INTO a wire (that's left of me)
				mWireNewWidth -= Scene::UNIT;
			}
		} else if (movingDirection == Direction::UP) {
			if (scenePos().y() <= mWireItem->scenePos().y()) {
				// Moving up AWAY from a wire (that's below me)
				mWireNewPosY -= Scene::UNIT;
				mWireNewHeight += Scene::UNIT;
			} else {
				// Moving UP into a wire (that's above me)
				mWireNewHeight -= Scene::UNIT;
			}
		} else if (movingDirection == Direction::DOWN) {
			if (scenePos().y() <= mWireItem->scenePos().y()) {
				// Moving dow INTO a wire (that's below me)
				mWireNewPosY += Scene::UNIT;
				mWireNewHeight -= Scene::UNIT;
			} else {
				// Moving down AWAY from a wire (that's above me)
				mWireNewHeight += Scene::UNIT;
			}
		}
		if (checkWidth && mWireNewWidth < Scene::UNIT)
			return;
		if (!checkWidth && mWireNewHeight < Scene::UNIT)
			return;
		actions.emplace_back(mWireItem, mWireNewPosX, mWireNewPosY, mWireNewWidth, mWireNewHeight);
		cache_current++;
	}

	for (auto &action : actions) {
		auto *affectedWireItem = action.m_wireitem;
		auto nx = action.m_new_x;
		auto ny = action.m_new_y;
		auto nw = action.m_new_width;
		auto nh = action.m_new_height;
		affectedWireItem->prepareGeometryChange();
		affectedWireItem->setPos(nx, ny);
		affectedWireItem->m_width = nw;
		affectedWireItem->m_height = nh;
		affectedWireItem->setPos(nx, ny);
		affectedWireItem->refreshConnectionPoints();
	}

	m_previousClickPoint = event->scenePos();
	setPos(pos().x() + gridDeltaX, pos().y() + gridDeltaY);
	getScene()->setChanged(true);
	getScene()->update();
}

void ComponentGraphicsItem::selectionModeMouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

	auto *scene = getScene();
	if (!scene->m_select_info.selection_is_moving)
		return;
	const auto *tag = "[CGI][SELECTION]";

	auto gridDelta = MiscUtils::getGridDelta(m_previousClickPoint, event->scenePos());
	if (!gridDelta) {
		return;
	}
	auto deltaX = gridDelta->x();
	auto deltaY = gridDelta->y();

	if (!scene->m_select_info.outside_connections_terminated) {
		std::cerr << tag << "Terminating connections." << std::endl;
		auto allItems = scene->getItems();
		for (auto *selectedItem : scene->m_select_info.selection) {
			std::cerr << "\t" << tag << " Checking for extra connections of "
					  << selectedItem->m_component->short_identify() << std::endl;
			auto input_conns = selectedItem->m_component->getInputConnections();
			auto output_conns = selectedItem->m_component->getOutputConnections();
            for (const auto& ci : {std::make_pair(input_conns, false), std::make_pair(output_conns, true)}) {
				bool isOutput = ci.second;
				auto conns = ci.first;
				for (size_t i = 0; i < conns.size(); i++) {
					if (!conns[i])
						continue;

					auto *c = conns[i].to_component;

					ComponentGraphicsItem *connectedWireItem = nullptr;
					for (auto *item : allItems) {
						if (item->m_component == c) {
							connectedWireItem = item;
							break;
						}
					}

					if (connectedWireItem == nullptr) {
						std::cerr << "=======================" << std::endl;
						std::cerr << "!!! AKO NEKO OVO VIDI ZNACI DA POSTOJI KOMPONENTA KOJA NEMA SVOJ ITEM !!!"
								  << std::endl;
						std::cerr << "\tKonkretno " << c->short_identify() << std::endl;
						std::cerr << "=======================" << std::endl;
						continue;
					}

					if (connectedWireItem->m_isInSelection)
						continue;
					std::cerr << "\t\t" << tag << " Found " << (isOutput ? "output" : "input")
							  << " connection to a non-selected item "
							  << connectedWireItem->m_component->short_identify() << std::endl;
					if (selectedItem->m_component->getType() == ComponentTypes::WIRE) {
						ConnectionManager::Instance().disconnectInput(selectedItem->m_component, i);
						ConnectionManager::Instance().disconnectOutput(selectedItem->m_component, i);
					} else {
						if (isOutput)
							ConnectionManager::Instance().disconnectOutput(selectedItem->m_component, i);
						else
							ConnectionManager::Instance().disconnectInput(selectedItem->m_component, i);
					}
				}
			}
		}
		scene->m_select_info.outside_connections_terminated = true;
	}

	// ------------------------------------------------
	// If ANY_OF the following iterations FAIL then FAIL
	// If ANY_OF the following iterations return FALSE FOR OKAY_POSITION
	auto selection_begin = std::begin(scene->m_select_info.selection);
	auto selection_end = std::end(scene->m_select_info.selection);
	bool allPositionsGood =
		!std::any_of(selection_begin, selection_end, [scene, deltaX, deltaY](ComponentGraphicsItem *selectedItem) {
			return !MiscUtils::checkPotentialPosition(
				scene, selectedItem, deltaX, deltaY,
				{MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(selectedItem),
				 [](ComponentGraphicsItem *sceneItem) { return !sceneItem->m_isInSelection; }});
		});

	if (allPositionsGood) {
		for (auto *selected_item : scene->m_select_info.selection) {
			selected_item->setPos(selected_item->pos().x() + deltaX, selected_item->pos().y() + deltaY);
		}
		scene->update();
	} else {
		std::cerr << tag << " Cannot move selection, overlaps found." << std::endl;
	}

	m_previousClickPoint = event->scenePos();
	// ---------------------------------------------
}

Scene *ComponentGraphicsItem::getScene()
{
	return static_cast<Scene *>(scene());
}

// Mouse release on a component item
// Used to stop moving the item around and
// resetting the cursor
void ComponentGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//    std::cerr << "[" << m_component->short_identify() << "] mouse released." << std::endl;
	auto mode = getScene()->controlMode();
	if (mode == ControlMode::DESIGN) {
		designModeMouseReleaseEvent(event);
	} else if (mode == ControlMode::SELECTION) {
		selectionModeMouseReleaseEvent(event);
	}
}

void ComponentGraphicsItem::designModeMouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() != Qt::MouseButton::LeftButton)
		return;

	this->setCursor(Qt::ArrowCursor);
	m_wireItemCache.clear();
	m_wireItemCacheValid = false;
	m_isHolding = false;
}

void ComponentGraphicsItem::selectionModeMouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	Q_UNUSED(event);
	auto *scene = getScene();
	if (!scene->m_select_info.selection_is_moving)
		return;
	const auto *tag = "[CGI][SELECTION]";
	std::cerr << tag << " Stop moving" << std::endl;
	scene->m_select_info.selection_is_moving = false;
	scene->m_select_info.outside_connections_terminated = false;
}

void ComponentGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(option);
	Q_UNUSED(widget);

	if (m_isInSelection) {
		painter->fillRect(boundingRect(), QBrush(QColor(128, 128, 255, 128)));
	}
	//    painter->fillRect(overlapBoundingRect(), QBrush(QColor(128, 128, 255, 128)));
	//    painter->fillRect(boundingRect(), QBrush(QColor(128, 128, 255, 128)));

	painter->setPen(Qt::lightGray);
	QBrush b = QBrush(Qt::blue);
	painter->setBrush(b);

	for (auto &point : m_inputPoints) {
		if (point.hasFocus())
			b.setColor(Qt::red);
		else
			b.setColor(Qt::blue);
		painter->setBrush(b);
		painter->drawEllipse(point.getBounds());
	}

	for (auto &point : m_outputPoints) {
		if (point.hasFocus())
			b.setColor(Qt::red);
		else
			b.setColor(Qt::blue);
		painter->setBrush(b);
		painter->drawEllipse(point.getBounds());
	}
}

QVector<ConnectionPoint> ComponentGraphicsItem::getOutputPoints() const
{
	return m_outputPoints;
}

QVector<ConnectionPoint> ComponentGraphicsItem::getInputPoints() const
{
	return m_inputPoints;
}

bool ComponentGraphicsItem::getIsResizable() const
{
	return m_isResizable;
}

Direction ComponentGraphicsItem::getOrientation()
{
	return m_orientation;
}

void ComponentGraphicsItem::serialize(std::ostream &os) const
{
	int id = m_component->getId();
	os.write((char *)(&id), sizeof(id));

	int posX = pos().x();
	int posY = pos().y();

	os.write((char *)(&posX), sizeof(posX));
	os.write((char *)(&posY), sizeof(posY));
	os.write((char *)(&m_width), sizeof(m_width));
	os.write((char *)(&m_height), sizeof(m_height));
	DirectionUtil::serialize(os, m_orientation);
}

void ComponentGraphicsItem::deserialize(std::istream &is)
{
	int posX;
	int posY;
	is.read((char *)(&posX), sizeof(posX));
	is.read((char *)(&posY), sizeof(posY));
	is.read((char *)(&m_width), sizeof(m_width));
	is.read((char *)(&m_height), sizeof(m_height));
	setPos(posX, posY);
	m_orientation = DirectionUtil::deserialize(is);
	drawOrientation();
}

void ComponentGraphicsItem::changeOrientation(Direction orient)
{
	if (orient != m_orientation) {
		ConnectionManager::Instance().disconnectInputs(m_component);
		ConnectionManager::Instance().disconnectOutputs(m_component);
		this->m_orientation = orient;
		drawOrientation();
	}
}
