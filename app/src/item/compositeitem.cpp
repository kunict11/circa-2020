#include "compositeitem.hpp"

CompositeItem::CompositeItem(
	int posX, int posY, std::vector<Pin *> pins, std::vector<LightBulb *> lbs, std::string name,
	QGraphicsItem *parent)
	: CompositeItem(ComponentFactory::instance().makeComposite(pins, lbs), posX, posY, name, parent)
{
}

CompositeItem::CompositeItem(Component *component, int posX, int posY, std::string name, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
	m_isResizable = false;
	m_name = name;
	this->resizeInputs(component->getInputSize());
}

int CompositeItem::calculateHeight() const
{
	return std::max(m_component->getInputSize(), m_component->getOutputSize()) * 2;
}

int CompositeItem::calculateWidth() const
{
	auto nameWidth = m_name.size() * 8;
	return std::max(ceil(nameWidth / Scene::UNIT), 2.0) + 1;
}

void CompositeItem::resizeInputs(unsigned n)
{
	prepareGeometryChange();
	this->m_component->resizeInputs(n);
	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();
	m_inputPoints = {};
	m_outputPoints = {};

	for (unsigned i = 0; i < m_component->getInputSize(); i++) {
		m_inputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));
	}
	for (unsigned i = 0; i < m_component->getOutputSize(); i++)
		m_outputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));
}

void CompositeItem::calculatePath()
{
	m_path = QPainterPath(QPoint(0, 0));
	m_path.addRect(QRectF(0, 0, m_width, m_height));
}

void CompositeItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	painter->setBrush(b);

	painter->drawPath(m_path);

	painter->drawText(QPointF(Scene::UNIT, Scene::UNIT), QString::fromStdString(m_name));

	ComponentGraphicsItem::paint(painter, option, widget);
}

void CompositeItem::setName(const std::string &name)
{
	m_name = name;
	resizeInputs(m_component->getInputSize());
}

void CompositeItem::serialize(std::ostream &os) const
{
	ComponentGraphicsItem::serialize(os);
	auto nameSize = m_name.size();
	os.write((char *)(&nameSize), sizeof(nameSize));
	os.write(m_name.c_str(), m_name.size());
}

void CompositeItem::deserialize(std::istream &is)
{
	ComponentGraphicsItem::deserialize(is);
	size_t nameSize;
	std::string name;
	is.read((char *)(&nameSize), sizeof(nameSize));
	name.resize(nameSize);
	is.read(&name[0], nameSize);
	setName(name);
}
