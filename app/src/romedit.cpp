#include "romedit.hpp"
#include "ui_romedit.h"

#include<QDebug>
#include<QDialogButtonBox>

ROMEdit::ROMEdit(ROM* rom, QWidget* parent)
    :QDialog(parent), ui(new Ui::ROMEdit()), m_rom(rom)
{
    setWindowTitle("ROM edit");
    int rows = rom->getRowCount();
    int cols = rom->getColCount();

    ui->setupUi(this);
    ui->memoryTable->setRowCount(rows);
    ui->memoryTable->setColumnCount(cols);

    for(unsigned i=0;i<rows;i++) {
        for(unsigned j=0;j<cols;j++) {
            unsigned val = rom->getContents()[rows*i+j].getState() == Signal::State::TRUE ? 1 : 0;
            auto *item = new QTableWidgetItem(QString::number(val));
            item->setTextAlignment(Qt::AlignCenter);
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            ui->memoryTable->setItem(i,j, item);
        }
    }
    QObject::connect(ui->memoryTable, &QTableWidget::itemDoubleClicked,
                     this, &ROMEdit::onTableItemDoubleClick);
    ui->memoryTable->resizeColumnsToContents();
    ui->memoryTable->resizeRowsToContents();
    adjustSize();

    contentsUpdated.resize(rows*cols);
    std::fill(std::begin(contentsUpdated), std::end(contentsUpdated), false);

    QObject::connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &ROMEdit::acceptChanges);
}

ROMEdit::~ROMEdit()
{
    delete ui;
}

void ROMEdit::onTableItemDoubleClick(QTableWidgetItem* item)
{
    auto itemValue = item->text();
    item->setText(itemValue == QString::number(1)
                  ? QString::number(0) : QString::number(1));
    int pos = ui->memoryTable->columnCount()*item->row()+item->column();

    contentsUpdated[pos] = !contentsUpdated[pos];
}

void ROMEdit::acceptChanges()
{
    for(unsigned i=0;i<contentsUpdated.size();i++){
        if(contentsUpdated[i]){
            m_rom->updateContents(i);
        }
    }
}
