#include "scene.hpp"

#include <QMessageBox>

#include "item/arithmetic/adderitem.hpp"
#include "item/arithmetic/comparatoritem.hpp"
#include "item/arithmetic/divideritem.hpp"
#include "item/arithmetic/multiplieritem.hpp"
#include "item/arithmetic/negatoritem.hpp"
#include "item/arithmetic/subtractoritem.hpp"
#include "item/compositeitem.hpp"
#include "item/gate/andgateitem.hpp"
#include "item/gate/bufferitem.hpp"
#include "item/gate/nandgateitem.hpp"
#include "item/gate/norgateitem.hpp"
#include "item/gate/notgateitem.hpp"
#include "item/gate/orgateitem.hpp"
#include "item/gate/xnorgateitem.hpp"
#include "item/gate/xorgateitem.hpp"
#include "item/itemfactory.hpp"
#include "item/memory/dflipflopitem.hpp"
#include "item/memory/jkflipflopitem.hpp"
#include "item/memory/srflipflopitem.hpp"
#include "item/memory/tflipflopitem.hpp"
#include "item/plexer/decoderitem.hpp"
#include "item/plexer/demultiplexeritem.hpp"
#include "item/plexer/encoderitem.hpp"
#include "item/plexer/multiplexeritem.hpp"
#include "item/wiring/constantitem.hpp"
#include "item/wiring/lightbulbitem.hpp"
#include "item/wiring/pinitem.hpp"
#include "item/wiring/splitteritem.hpp"
#include "item/wiring/wireitem.hpp"
#include "mainwindow.hpp"

const std::pair<unsigned, unsigned> Scene::directionLookup[4][4] = {
	{{3, 0}, {3, 3}, {4, 1}, {5, 4}},
	{{0, 0}, {0, 3}, {1, 2}, {2, 5}},
	{{5, 2}, {4, 5}, {3, 0}, {3, 3}},
	{{2, 1}, {1, 4}, {0, 0}, {0, 3}}};
Scene::Scene(QObject *parent) : QGraphicsScene(parent), m_changed(false)
{
    setBackgroundBrush(QColor(65, 65, 65));

	m_controlMode = ControlMode::DESIGN;
	resetSelectInfo();
	m_clockTimer = new QTimer(this);
	m_clockTimer->setInterval(1000);
    connect(m_clockTimer, &QTimer::timeout, this, &Scene::tick);
}

Scene::~Scene()
{
	delete m_clockTimer;
	reset();
}

QPointF Scene::mapToGrid(const QPointF &p)
{
	return QPointF(round(p.x() / Scene::UNIT) * Scene::UNIT, round(p.y() / Scene::UNIT) * Scene::UNIT);
}

float Scene::mapToGrid(float x)
{
	return round(x / Scene::UNIT) * Scene::UNIT;
}

void Scene::drawBackground(QPainter *painter, const QRectF &rect)
{
	QGraphicsScene::drawBackground(painter, rect);

	QPen pen;
	pen.setWidth(2);
	painter->setPen(pen);

	qreal left = int(rect.left()) - (int(rect.left()) % UNIT);
	qreal top = int(rect.top()) - (int(rect.top()) % UNIT);
	QVector<QPointF> points;
	for (qreal x = left; x < rect.right(); x += UNIT) {
		for (qreal y = top; y < rect.bottom(); y += UNIT) {
			points.append(QPointF(x, y));
		}
	}
	painter->drawPoints(points.data(), points.size());
}

void Scene::drawForeground(QPainter *painter, const QRectF &rect)
{
	Q_UNUSED(rect);

	if (controlMode() == ControlMode::DESIGN && m_drawing_wire_info.drawing) {
		if (m_drawing_wire_info.current_direction == Direction::RIGHT ||
			m_drawing_wire_info.current_direction == Direction::LEFT) {
			painter->drawLine(
				m_drawing_wire_info.start_point, QPointF(m_endPoint.x(), m_drawing_wire_info.start_point.y()));
		} else {
			painter->drawLine(
				m_drawing_wire_info.start_point, QPointF(m_drawing_wire_info.start_point.x(), m_endPoint.y()));
		}
	} else if (controlMode() == ControlMode::SELECTION && m_select_info.in_manual_selection) {
		painter->drawRect(getManualSelectionRect());
	}

	std::ostringstream cm;
	cm << controlMode();
	painter->drawText(QPoint(15, 15), QString::fromStdString(cm.str()));
	if (!m_clipBoard.items.empty()) {
		painter->drawText(QPoint(15, 35), "Clipboard (ctrl+v to paste)");
	}
}

void Scene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	//    QGraphicsScene::dragEnterEvent(event);
	if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
		event->accept();
}

void Scene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
	//    QGraphicsScene::dragMoveEvent(event);
	if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
		event->accept();
}

void Scene::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	QGraphicsScene::dropEvent(event);
	if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist")) {
		auto data = event->mimeData()->data("application/x-qabstractitemmodeldatalist");
		QDataStream stream(&data, QIODevice::ReadOnly);
		QString text;
		int row;
		int col;
		QMap<int, QVariant> roleDataMap;
		stream >> row >> col >> roleDataMap;
		text = roleDataMap[Qt::WhatsThisRole].toString();

		QPointF p = event->scenePos();

		std::string name = text.toStdString();
		ComponentGraphicsItem *item;

		if (name == "NOT")
			item = ItemFactory::Instance().make(ComponentTypes::NOTGATE);
		else if (name == "Buffer")
			item = ItemFactory::Instance().make(ComponentTypes::BUFFER);
		else if (name == "AND")
			item = ItemFactory::Instance().make(ComponentTypes::ANDGATE);
		else if (name == "Pin")
			item = ItemFactory::Instance().make(ComponentTypes::PIN);
		else if (name == "Lightbulb")
			item = ItemFactory::Instance().make(ComponentTypes::LIGHTBULB);
		else if (name == "OR")
			item = ItemFactory::Instance().make(ComponentTypes::ORGATE);
		else if (name == "XOR")
			item = ItemFactory::Instance().make(ComponentTypes::XORGATE);
		else if (name == "NAND")
			item = ItemFactory::Instance().make(ComponentTypes::NANDGATE);
		else if (name == "NOR")
			item = ItemFactory::Instance().make(ComponentTypes::NORGATE);
		else if (name == "XNOR")
			item = ItemFactory::Instance().make(ComponentTypes::XNORGATE);
		else if (name == "Constant")
			item = ItemFactory::Instance().make(ComponentTypes::CONSTANT);
		else if (name == "Multiplexer")
			item = ItemFactory::Instance().make(ComponentTypes::MULTIPLEXER);
		else if (name == "Demultiplexer")
			item = ItemFactory::Instance().make(ComponentTypes::DEMULTIPLEXER);
		else if (name == "Encoder")
			item = ItemFactory::Instance().make(ComponentTypes::ENCODER);
		else if (name == "Decoder")
			item = ItemFactory::Instance().make(ComponentTypes::DECODER);
		else if (name == "Comparator")
			item = ItemFactory::Instance().make(ComponentTypes::COMPARATOR);
		else if (name == "Adder")
			item = ItemFactory::Instance().make(ComponentTypes::ADDER);
		else if (name == "Subtractor")
			item = ItemFactory::Instance().make(ComponentTypes::SUBTRACTOR);
		else if (name == "Multiplier")
			item = ItemFactory::Instance().make(ComponentTypes::MULTIPLIER);
		else if (name == "Divider")
			item = ItemFactory::Instance().make(ComponentTypes::DIVIDER);
		else if (name == "Negator")
			item = ItemFactory::Instance().make(ComponentTypes::NEGATOR);
		else if (name == "D")
			item = ItemFactory::Instance().make(ComponentTypes::DFLIPFLOP);
		else if (name == "T")
			item = ItemFactory::Instance().make(ComponentTypes::TFLIPFLOP);
		else if (name == "J-K")
			item = ItemFactory::Instance().make(ComponentTypes::JKFLIPFLOP);
		else if (name == "S-R")
			item = ItemFactory::Instance().make(ComponentTypes::SRFLIPFLOP);
        else if (name == "ROM")
            item = ItemFactory::Instance().make(ComponentTypes::ROM);
        else if (name == "RAM")
            item = ItemFactory::Instance().make(ComponentTypes::RAM);
		else if (name == "Splitter")
			item = ItemFactory::Instance().make(ComponentTypes::SPLITTER);
		else if (name == "Clock")
			item = ItemFactory::Instance().make(ComponentTypes::CLOCK);
		else {
			std::cerr << "unknown component" << std::endl;
			return;
		}

		item->setPos(mapToGrid(p));
		auto actualPos = MiscUtils::getClosestAvailablePosition(this, item->overlapBoundingRectSc());

		if (!actualPos) {
			QMessageBox::critical(mw, "Create a component", "Couldn't find a free spot on the board.");
			// TODO: Check if this is the correct way of deleting.
			delete item->m_component;
			delete item;
			return;
		}
		item->setPos(actualPos.value());
		addComponent(item);

		if (!selectedItems().empty())
            selectedItems().constFirst()->setSelected(false);
		item->setSelected(true);
		emit toggleBox(
			false, "orientation", item->m_component->getInputSize(), DirectionUtil::toInt(item->getOrientation()));

		if (static_cast<ComponentGraphicsItem *>(item)->getIsResizable())
			emit toggleBox(
				false, "input", item->m_component->getInputSize(), DirectionUtil::toInt(item->getOrientation()));
		else
			emit toggleBox(true, "input", 0, 0);
	}
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

	m_startPoint = QPointF(
		round(event->scenePos().x() / Scene::UNIT) * Scene::UNIT,
		round(event->scenePos().y() / Scene::UNIT) * Scene::UNIT);
	if (m_controlMode == ControlMode::DESIGN) {
		designModeMousePressEvent(event);
	} else if (m_controlMode == ControlMode::PINS) {
		QGraphicsScene::mousePressEvent(event);
	} else if (m_controlMode == ControlMode::SELECTION) {
		selectionModeMousePressEvent(event);
	}
}

void Scene::designModeMousePressEvent(QGraphicsSceneMouseEvent *event)
{
	std::string tag = "[SCENE][DESIGN] ";
	std::string nl = '\n' + std::string(tag.size(), ' ');
	std::string tab = std::string(tag.size(), ' ');
	if (event->button() == Qt::MouseButton::RightButton && m_drawing_wire_info.drawing) {
		resetDrawInfo();
        update();
	} else if (event->button() == Qt::MouseButton::RightButton && !m_drawing_wire_info.drawing) {
        int deletedItems = 0;
		for (auto *item : getItems()) {
			if (item->boundingRect().translated(item->scenePos()).contains(event->scenePos())) {
				deleteComponent(item);
                ++deletedItems;
			}
		}
        if (deletedItems > 0)
            update();
		return;
	} else if (event->button() == Qt::MouseButton::LeftButton) {

		QGraphicsItem *pressedItem = itemAt(event->scenePos(), QTransform());
		if (pressedItem == nullptr) {
			emit toggleBox(true, "input", 0, 0);
			emit toggleBox(true, "orientation", 0, 0);
		}

		auto clickPoint = modifiedScenePos(event->scenePos());
		auto childrenHandles = clickedChildren(clickPoint);
		processReport(childrenHandles, event, clickPoint);
		// No children caught the event and state is nothing, means this is a random scene click.
	}
}

void Scene::selectionModeMousePressEvent(QGraphicsSceneMouseEvent *event)
{

	if (event->button() == Qt::MouseButton::RightButton) {
		deleteSelection();
	}

	const auto *tag = "[SCENE][SELECTION]";
	if (m_select_info.in_manual_selection) {
		std::cerr << tag << " This should not be possible";
		resetSelectInfo();
		return;
	}
	if (m_select_info.holding_ctrl) {
		QGraphicsScene::mousePressEvent(event);
		return;
	}

	m_select_info.handled = false;
	QGraphicsScene::mousePressEvent(event);
	if (m_select_info.handled) {
		std::cerr << "\t" << tag << " Event handled by CGI, scene ignoring." << std::endl;
		return;
	}

	std::cerr << tag << " Event unhandled by any CGIs, scene beginning manual selection." << std::endl;
	clearSelection();
	m_select_info.in_manual_selection = true;
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

	QGraphicsScene::mouseMoveEvent(event);
	m_endPoint = QPointF(
		round(event->scenePos().x() / Scene::UNIT) * Scene::UNIT,
		round(event->scenePos().y() / Scene::UNIT) * Scene::UNIT);

	if (m_controlMode == ControlMode::DESIGN) {
		designModeMouseMoveEvent(event);
	} else if (m_controlMode == ControlMode::SELECTION) {
		selectionModeMouseMoveEvent(event);
	}
}

void Scene::designModeMouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if (m_drawing_wire_info.drawing) {
		QPointF f_endPoint = event->scenePos();

		auto delta_x = m_drawing_wire_info.start_point.x() - f_endPoint.x();
		auto delta_y = m_drawing_wire_info.start_point.y() - f_endPoint.y();
		if (fabs(delta_x) >= fabs(delta_y)) {
			if (delta_x > 0) {
				m_drawing_wire_info.current_direction = Direction::LEFT;
			} else {
				m_drawing_wire_info.current_direction = Direction::RIGHT;
			}
		} else {
			if (delta_y < 0) {
				m_drawing_wire_info.current_direction = Direction::DOWN;
			} else {
				m_drawing_wire_info.current_direction = Direction::UP;
			}
		}
		update();
	}
}

void Scene::selectionModeMouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	// const auto *tag = "[SCENE][SELECTION]";
	if (!m_select_info.in_manual_selection && !m_select_info.selection.empty()) {
		QGraphicsScene::mouseMoveEvent(event);
		return;
	} else if (m_select_info.in_manual_selection) {
        for (const auto &item : getItems()) {
            if (getManualSelectionRect().intersects(item->boundingRect().translated(item->scenePos()))) {
                m_select_info.selection.insert(item);
                item->m_isInSelection = true;
			} else {
                m_select_info.selection.erase(item);
                item->m_isInSelection = false;
			}
		}
		update();
	}
};

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if (controlMode() == ControlMode::SELECTION) {
		selectionModeMouseReleaseEvent(event);
    } else {
        update();
		QGraphicsScene::mouseReleaseEvent(event);
    }
}

void Scene::selectionModeMouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF p1 = QPointF(fmin(m_startPoint.x(), m_endPoint.x()), fmin(m_startPoint.y(), m_endPoint.y()));
    QPointF p2 = QPointF(fmax(m_startPoint.x(), m_endPoint.x()), fmax(m_startPoint.y(), m_endPoint.y()));
	if (m_select_info.in_manual_selection) {
		m_select_info.in_manual_selection = false;
        update(QRectF(p1, p2));
	} else {
		QGraphicsScene::mouseReleaseEvent(event);
	}
}

void Scene::deleteComponent(ComponentGraphicsItem *item)
{
	item->prepareGeometryChange();
	auto *component = item->m_component;
	m_cb.remove(component);
	this->removeItem(item);
	delete item;

	m_changed = true;
}

void Scene::deleteSelection()
{
	for (auto *item : m_select_info.selection) {
		deleteComponent(item);
	}
	m_select_info.handled = false;
	m_select_info.holding_ctrl = false;
	m_select_info.outside_connections_terminated = false;
	m_select_info.in_manual_selection = false;
	m_select_info.selection_is_moving = false;
	m_select_info.selection.clear();
	update();

	m_changed = true;
}

void Scene::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_S) {
		if (m_controlMode == ControlMode::PINS)
			m_clockTimer->stop();
		setControlMode(ControlMode::SELECTION);
	} else if (event->key() == Qt::Key_D) {
		if (m_controlMode == ControlMode::PINS)
			m_clockTimer->stop();
		setControlMode(ControlMode::DESIGN);
	} else if (event->key() == Qt::Key_P) {
		setControlMode(ControlMode::PINS);
		m_clockTimer->start();
	} else if (event->key() == Qt::Key_Control) {
		if (controlMode() == ControlMode::SELECTION) {
			m_select_info.holding_ctrl = true;
		}
		m_clipBoard.holding_ctrl = true;
	} else if (event->key() == Qt::Key_Delete && controlMode() == ControlMode::SELECTION) {
		deleteSelection();
	} else if (event->key() == Qt::Key_C && m_clipBoard.holding_ctrl) {
		if (m_select_info.selection.empty())
			return;
		// TODO: Check whether this is the right way to delete.
		for (auto *it : m_clipBoard.items) {
			delete it->m_component;
			delete it;
		}
		m_clipBoard.items.clear();
		m_clipBoard.items = ItemFactory::Instance().deepCopyItemSelection(
			std::begin(m_select_info.selection), std::end(m_select_info.selection));
		m_clipBoard.selectionRect = MiscUtils::getGroupRect(m_clipBoard.items);
		m_clipBoard.anchor = MiscUtils::getAnchor(m_clipBoard.items);
		m_clipBoard.holding_ctrl = false;

	} else if (event->key() == Qt::Key_V && m_clipBoard.holding_ctrl) {
		if (m_clipBoard.items.empty())
			return;
		auto pasteTopLeftOpt = MiscUtils::getClosestAvailablePosition(
			this, QRectF(15, 15, m_clipBoard.selectionRect.width(), m_clipBoard.selectionRect.height()));
		if (!pasteTopLeftOpt) {
			QMessageBox::critical(mw, "Pasting", "Couldn't find a free spot on the board.");
			// NOTE: Not sure why I need to do this but ctrl kind of gets 'stuck' after
			// message box. Probably mouseRelease not being called.
			m_clipBoard.holding_ctrl = false;
			m_select_info.holding_ctrl = false;
			return;
		}
		auto freshCopy = ItemFactory::Instance().deepCopyItemSelection(
			std::begin(m_clipBoard.items), std::end(m_clipBoard.items));
		auto pasteTopLeft = pasteTopLeftOpt.value();
		resetSelectInfo();
		setControlMode(ControlMode::SELECTION);
		for (auto *itemToPaste : freshCopy) {
			itemToPaste->setPos(itemToPaste->scenePos() - m_clipBoard.anchor + pasteTopLeft);
			// The component is not in any circuitboard
			// and the item is not in any scene so this method
			// will work perfectly.
			addComponent(itemToPaste);
			m_select_info.selection.insert(itemToPaste);
			itemToPaste->m_isInSelection = true;
		}

		update();

	} else if (event->key() == Qt::Key_A) {
		setControlMode(ControlMode::SELECTION);
		for (auto *i : getItems()) {
			m_select_info.selection.insert(i);
			i->m_isInSelection = true;
		}
	}

	update();
}

void Scene::keyReleaseEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Control) {
		m_select_info.holding_ctrl = false;
		m_clipBoard.holding_ctrl = false;
	}
}

void Scene::resetDrawInfo()
{
	m_drawing_wire_info.source_components.clear();
	m_drawing_wire_info.source_directions.clear();
	m_drawing_wire_info.source_is_output.clear();
	m_drawing_wire_info.source_indicies.clear();
	m_drawing_wire_info.drawing = false;
}

void Scene::resetSelectInfo()
{
	m_select_info.handled = false;
	m_select_info.holding_ctrl = false;
	m_select_info.outside_connections_terminated = false;
	m_select_info.in_manual_selection = false;
	m_select_info.selection_is_moving = false;
	clearSelection();
}

void Scene::changeOrientation(Direction orient)
{
	if (!selectedItems().empty()) {
        static_cast<ComponentGraphicsItem *>(selectedItems().constFirst())->safeRotate(orient);
		update();
	}
	m_changed = true;
}
QPointF Scene::modifiedScenePos(const QPointF &eventScenePos) const
{
	if (!m_drawing_wire_info.drawing) {
		return Scene::mapToGrid(eventScenePos);
	}

	auto sp = m_drawing_wire_info.start_point;
	auto dir = m_drawing_wire_info.current_direction;
	QPointF ep;
	if (dir == Direction::LEFT || dir == Direction::RIGHT) {
		ep.setX(eventScenePos.x());
		ep.setY(sp.y());
	} else {
		ep.setX(sp.x());
		ep.setY(eventScenePos.y());
	}

	return Scene::mapToGrid(ep);
}

void Scene::processReport(
	const std::vector<IndirectClickReport> &report, QGraphicsSceneMouseEvent *event, const QPointF &clickPoint)
{
	std::string tag = "[SCENE][DESIGN] ";
	std::string nl = '\n' + std::string(tag.size(), ' ');

	auto begin = std::begin(report);
	auto end = std::end(report);
	auto num_clicked = report.size();
	auto &info = m_drawing_wire_info;
	//    auto& state         = info.state;
	auto nothing = !info.drawing;
	auto drawing = !nothing;
	auto sourceBegin = std::begin(info.source_components);
	auto sourceEnd = std::end(info.source_components);
	//	auto sourceDirBegin = std::begin(info.source_directions);
	//	auto sourceDirEnd = std::end(info.source_directions);
	unsigned numNonCpClicks =
		std::count_if(begin, end, [](const IndirectClickReport &click) { return click.cpIndex == -1; });
	unsigned numCpClicks = num_clicked - numNonCpClicks;
	unsigned numWires = std::count_if(begin, end, [](const IndirectClickReport &click) {
		return click.clickedItem->m_component->getType() == ComponentTypes::WIRE;
	});
	unsigned numNonWires = num_clicked - numWires;
	unsigned numWiresNonCp = std::count_if(begin, end, [](const IndirectClickReport &click) {
		return click.cpIndex == -1 && click.clickedItem->m_component->getType() == ComponentTypes::WIRE;
	});
	unsigned numWiresCp = numWires - numWiresNonCp;

	// NOTE: Yes, a lot of these conditions could've been nested. However the spaghettiness of the code would've
	// been too great.
	//      This way it's more readable and easier to debug.

	// This is isolated because in case the user just clicked on scene randomly
	// I want it detected immediately instead of running a bunch of other unnecessary checks.
	// num_clicked == 0             -- you haven't clicked on any item
	// nothing                      -- you are not drawing wires
	if (num_clicked == 0 && nothing) {
		std::cerr << tag << "Drawing nothing and click on nothing." << nl << "\tDeduced random click, ignoring."
				  << std::endl;
		return;
	}

	// This is isolated because in case the user just wants to move an item
	// I want it detected immediately instead of running a bunch of unnecessary checks.
	// num_clicked == 1             -- clicked on exactly one item
	// report[0].cpIndex == -1      -- the click is NOT a connection point click
	// nothing                      -- you are not drawing wires
	if (num_clicked == 1 && report[0].cpIndex == -1 && nothing &&
		report[0].clickedItem->m_component->getType() != ComponentTypes::WIRE) {
		std::cerr << tag
				  << "Clicked on one item directly from nothing. Maybe the user wants to move. Pass to children."
				  << std::endl;
		QGraphicsScene::mousePressEvent(event);
		return;
	}

	if (numNonWires > 1) {
		std::cerr << tag << "Clicked on more than one non wire item. This is illegal. Ignoring" << std::endl;
		return;
	}

	// This is isolated because, even though it's very similar to what comes next, it's just different enough
	// that refactoring it in is way too complicated. Even if done right the code becomes barely understandable.
	// This is the case of chaining wires, that is
	// num_cliked == 0 means that you haven't clicked on any component
	// and drawing means you're drawing something.
	// This should also mean that your source is filled in correctly since other handlers should fill it for you,
	// therefore no other checks are necessary except for overlap and backingUpOnSource.
	if (num_clicked == 0 && drawing) {
		std::vector<ShouldConsiderPredicate> special_conditions = {
			MiscUtils::OVERLAP_EXCLUDE_WIRES_CROSS(info.current_direction)};
		for (auto *src : info.source_components) {
			special_conditions.push_back(MiscUtils::OVERLAP_EXCLUDE_THIS_COMPONENT(src));
		}
		bool overlap = MiscUtils::overlapsAnyItem(
			this,
			WireDrawUtils::getPotentialWireItemOverlapBoundsSc(
				info.start_point, m_endPoint, info.current_direction),

			special_conditions);
		if (overlap) {
			std::cerr << tag << "Drawing from source, attempting to create new wire. Overlaps found. Ignoring."
					  << std::endl;
			return;
		}

		// Couldn't be handled by regular overlaps because it would always be caught
		bool backingUpOnSource = false;
		if (drawing) {
			for (size_t i = 0; i < info.source_components.size(); i++) {
				if (info.source_components[i]->getType() != ComponentTypes::WIRE)
					continue;
				if (info.source_directions[i] == DirectionUtil::reverse(info.current_direction)) {
					backingUpOnSource = true;
					std::cerr << "\t\t\t -- source direction clash, source "
							  << info.source_components[i]->short_identify() << " directed "
							  << info.source_directions[i] << " but "
							  << "wire directed " << info.current_direction << std::endl;
					break;
				}
			}
		}
		if (backingUpOnSource) {
			std::cerr << tag << "Special overlap case. This is illegal. Ignoring." << std::endl;
			return;
		}

		bool singleSourceComponentBackwardsOverlap =
			drawing && info.source_components[0]->getType() != ComponentTypes::WIRE &&
			info.source_directions[0] == DirectionUtil::reverse(info.current_direction);
		if (singleSourceComponentBackwardsOverlap) {
			std::cerr << tag << "Single source component backwards overlap. This is illegal. Ignoring."
					  << std::endl;
			return;
		}

		std::cerr << tag << "Drawing from source, attempting to create new wire. Success. Continue." << std::endl;
		auto *w = ComponentFactory::instance().makeWire();

		// NOTE: Source to wire is now refactored so that it works both for single-source and multi-source
		// Meaning if source is a single connection point from a non wire component it will recognize that
		// and establish a regular connection. However if source is one or more wires it will establish
		// double connection.
		WireDrawUtils::sourceToWire(info, w);
		WireDrawUtils::makeWireItemAndAdd(this, w);
		info.start_point = clickPoint;
		info.source_components = {w};
		info.source_directions = {info.current_direction};
		// NOTE: State remains unchanged since you've just drawn a wire and you continue the chaining process
		update();
		return;
	}

	// This is a fuse of multiple other cases. Isolating every other case would result in way too much code.
	// Previous two isolated cases consider (num_cliked == 0 && nothing) and (num_cliked == 0 && drawing)
	// therefore at this point it is certain that num_clicked > 0. The `if` stays however since it makes it more
	// readable and in case I ever wish to refactor, it's just easier.
	if (num_clicked > 0) {
		// Gatthering info all relevant info for upcoming validations

		// The following checks are done independently of the state you're currently in; that is they are
		// conditions which are satisfied if you're trying to perform an action that is definitely invalid,
		// irregardless of whether you're drawing wires or not. Also, there are probably some redundancies,
		// but better safe than sorry.

		if (numNonCpClicks > 0 && numCpClicks > 0) {
			std::cerr << tag
					  << "Some of the clicks came from connection points, and some from items. This is illegal. "
						 "Ignoring."
					  << std::endl;
			return;
		}
		if (numWires > 0 && numNonWires > 0) {
			std::cerr << tag
					  << "Some of the clicks came from wires and some from non wires. This is illegal. Ignoring."
					  << std::endl;
			return;
		}
		if (numWiresCp > 0 && numWiresNonCp > 0) {
			std::cerr << tag << "Some wires are cps and some are not. This is illegal. Ignoring." << std::endl;
			return;
		}
		if (numWires > 3) {
			std::cerr << tag << "Attempt to draw from 3+ wires. This is illegal. Ignoring." << std::endl;
			return;
		}
		if (numWiresNonCp > 1) {
			std::cerr << tag << "Attempt to draw from wire crosspoint (probably). This is illegal. Ignoring."
					  << std::endl;
			return;
		}

		if (numWires == 0 && numCpClicks > 1) {
			std::cerr << tag
					  << "You've somehow clicked on multiple non-wire connection points. This is illegal. Ignoring"
					  << std::endl;
			return;
		}
		// The following are state dependent validations, that is the conditions that check for invalid actions
		// only if you're in a specific state. The following are only for state of drawing.
		bool connectingToYourself =
			drawing && std::any_of(begin, end, [info, sourceBegin, sourceEnd](const IndirectClickReport &click) {
				auto *clickedComponent = click.clickedItem->m_component;
				return std::any_of(sourceBegin, sourceEnd, [clickedComponent](Component *sourceComponent) {
					return clickedComponent == sourceComponent;
				});
			});
		if (connectingToYourself) {
			std::cerr << tag << "Either overlap or connecting to yourself. This is illegal. Ignoring."
					  << std::endl;
			return;
		}

		bool sameDirectionWireSplit =
			drawing && std::any_of(begin, end, [info](const IndirectClickReport &click) {
				auto *clickedItem = click.clickedItem;
				if (clickedItem->m_component->getType() != ComponentTypes::WIRE || click.cpIndex != -1)
					return false;
				auto *clickedWireItem = static_cast<WireItem *>(clickedItem);
				return DirectionUtil::sameOrientation(clickedWireItem->getDirection(), info.current_direction);
			});

		if (sameDirectionWireSplit) {
			std::cerr << tag
					  << "Attempting to split a wire that's the same direction as the new one. This is illegal. "
						 "Ignoring."
					  << std::endl;
			return;
		}
		// This would've been a one-liner if STL had had std::zip or if RANGES WORKED!
		// Equivalent to 'backingUpOnSource' in the case of chaining.
		bool extendingExistingWire = false;
		if (drawing) {
			for (size_t i = 0; i < info.source_components.size(); i++) {
				if (info.source_components[i]->getType() != ComponentTypes::WIRE)
					continue;
				if (info.source_directions[i] == DirectionUtil::reverse(info.current_direction)) {
					extendingExistingWire = true;
					break;
				}
			}
		}
		if (extendingExistingWire) {
			std::cerr << tag
					  << "I am way too lazy to implement extending and shrinking wires this way. So for now: This "
						 "is illegal. Ignoring."
					  << std::endl;
			return;
		}

		bool connectingToTakenCp =
			drawing && numCpClicks == 1 && numWires == 0 &&
			(report[0].cpIsOutput ? report[0].clickedItem->m_component->getOutputConnections()
								  : report[0].clickedItem->m_component->getInputConnections())[report[0].cpIndex];
		if (connectingToTakenCp) {
			std::cerr << tag << "Attempting to connect to a taken non wire cp. This is illegal. Ignoring."
					  << std::endl;
			return;
		}

		bool singleSourceComponentBackwardsOverlap =
			drawing && info.source_components[0]->getType() != ComponentTypes::WIRE &&
			info.source_directions[0] == DirectionUtil::reverse(info.current_direction);
		if (singleSourceComponentBackwardsOverlap) {
			std::cerr << tag << "Single source component backwards overlap. This is illegal. Ignoring."
					  << std::endl;
			return;
		}

		// Now we get to actual specifics of what's going on because, as mentioned at the beginning of this scope,
		// this is a fuse of many different cases.
		// The idea is the following:
		// The only thing we're actually certain of so far is that you've clicked on 1 or more
		// components and that the click is valid (this does not mean that the position of the potential wire
		// will be valid, it may cause overlaps).

		// First we'll consider the state being nothing
		// CASE N1: You click on exactly one non-wire connection point
		// CASE N2: You click on a few wire connection points
		// CASE N3: You click on exactly one wire
		if (nothing) {
			resetDrawInfo(); /*Just in case*/
			if (numWiresNonCp == 1) {
				// N3, now we need to split the wire
				auto *originalWireItem = static_cast<WireItem *>(report[0].clickedItem);
				auto splitRes = originalWireItem->splitMe(clickPoint);
				WireItem *newWireItem = std::get<0>(splitRes);
				Direction originalDirection = std::get<1>(splitRes);
				Direction newItemDirection = std::get<2>(splitRes);
				info.source_components = {originalWireItem->m_component, newWireItem->m_component};
				info.source_directions = {originalDirection, newItemDirection};
				info.source_indicies = {-1, -1};		// Meaningless
				info.source_is_output = {false, false}; // ----------
				info.start_point =
                    originalWireItem->getOutputPoints().at(0).getCenter() + originalWireItem->scenePos();
			} else if (numWires == 0 || numWiresCp > 0) {
				// N1 and N2
				for (const auto &click : report) {
					info.source_components.push_back(click.clickedItem->m_component);
					info.source_directions.push_back(click.cpDirection);
					//                    std::cerr << "\t\t\t -- source clash debug, source " <<
					//                    click.clickedItem->m_component->short_identify() << " directed " <<
					//                    click.cpDirection << std::endl;
					info.source_indicies.push_back(click.cpIndex);
					info.source_is_output.push_back(click.cpIsOutput);
				}
				auto first = report[0];
				if (first.cpIsOutput)
                    info.start_point = first.clickedItem->getOutputPoints().at(first.cpIndex).getCenter();
				else
                    info.start_point = first.clickedItem->getInputPoints().at(first.cpIndex).getCenter();
				info.start_point += first.clickedItem->scenePos();
			} else {
				std::cerr << tag << "#1 Validation case missed, analyze this." << std::endl;
				return;
			}

			info.drawing = true;
			update();
			return;
		}

		// Now we're in drawing state and we've clicked on something that's definitely not a scene (that's the
		// chaining case that's been handled separately). Therefore whatever we do we're gonna create a wire,
		// connect source (which already exists) to it, fetch our target/s and then connect wire to target/s. We
		// have to be careful here because of overlaps. In the previous cases we didn't need to check because we
		// weren't actually creating any new wires, but now we will. Whatever the case, after this we go to NOTHING
		// and we're good, so no need to worry about what needs to be written in the info structure because we'll
		// just flush it. CASE D1: Clicked on exactly one non-wire and non-taken connection point. CASE D2: Clicked
		// on one or more wire connection points. CASE D3: Clicked on exactly one wire (not cp) that we will split.
		if (drawing) {
			std::vector<ShouldConsiderPredicate> overlap_predicates = {
				MiscUtils::OVERLAP_EXCLUDE_WIRES_CROSS(info.current_direction)};
			for (auto *source : info.source_components) {
				overlap_predicates.push_back(MiscUtils::OVERLAP_EXCLUDE_THIS_COMPONENT(source));
			}
			std::vector<Component *> target_components;
			std::vector<Direction> target_directions;
			std::vector<bool> target_is_output;
			std::vector<int> target_indicies;
			if (numWires == 0 && numCpClicks == 1) {
				// D1
				auto *clickedItem = report[0].clickedItem;
				auto cpIsOutput = report[0].cpIsOutput;
				auto cpIndex = report[0].cpIndex;
				auto cpDirection = report[0].cpDirection;
				overlap_predicates.push_back(MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(clickedItem));
				target_components = {clickedItem->m_component};
				target_directions = {DirectionUtil::reverse(cpDirection)};
				target_is_output = {cpIsOutput};
				target_indicies = {cpIndex};
			} else if (numWiresCp > 0) { /*It should not be physically possible to click on multiple wire CPs that
											are not connected. If something weird happens add that check.*/
				// D2
				for (auto clickedWireReport : report) {
					auto *clickedItem = static_cast<WireItem *>(clickedWireReport.clickedItem);
					auto cpDirection = clickedWireReport.cpDirection;
					overlap_predicates.push_back(MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(clickedItem));
					target_components.push_back(clickedItem->m_component);
					target_directions.push_back(DirectionUtil::reverse(cpDirection));
				}
			} else if (numWiresNonCp == 1) {
				// Problem:
				// We cannot split and fill the targets because overlap gets checked later, and in case overlap
				// happens So here we will just add this one predicate, and then after checking overlap we'll come
				// back to this case.
				overlap_predicates.push_back(MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(report[0].clickedItem));
			} else {
				std::cerr << tag << "#2 Validation case missed, analyze this." << std::endl;
				resetDrawInfo();
				return;
			}
			bool specialTargetOverlap =
				numWiresCp > 0 && std::any_of(
									  std::begin(target_directions), std::end(target_directions),
									  [info](const Direction &targetDir) {
										  return DirectionUtil::reverse(targetDir) == info.current_direction;
									  });
			if (specialTargetOverlap) {
				std::cerr << "Special target overlap. This is illegal. Ignoring" << std::endl;
				return;
			}

			bool overlap = MiscUtils::overlapsAnyItem(
				this,
				WireDrawUtils::getPotentialWireItemOverlapBoundsSc(
					info.start_point, m_endPoint, info.current_direction),
				overlap_predicates);
			if (overlap) {
				std::cerr << tag << "[3] Overlaps detected. This is illegal. Ignoring." << std::endl;
				return;
			}
			// Now we come back if third case
			if (numWiresNonCp == 1) {
				auto *originalWireItem = static_cast<WireItem *>(report[0].clickedItem);
				auto splitRes = originalWireItem->splitMe(clickPoint);
				WireItem *newWireItem = std::get<0>(splitRes);
				Direction originalDirection = std::get<1>(splitRes);
				Direction newItemDirection = std::get<2>(splitRes);
				target_components = {originalWireItem->m_component, newWireItem->m_component};
				target_directions = {
					DirectionUtil::reverse(originalDirection), DirectionUtil::reverse(newItemDirection)};
			}

			auto *w = ComponentFactory::instance().makeWire();
			WireDrawUtils::sourceToWire(info, w);
			WireDrawUtils::wireToTarget(
				w, info.current_direction, target_components, target_directions, target_indicies,
				target_is_output);
			WireDrawUtils::makeWireItemAndAdd(this, w);
			resetDrawInfo();
		}
	}
}

std::vector<IndirectClickReport> Scene::clickedChildren(const QPointF &clickPoint)
{
	std::vector<IndirectClickReport> res;

	for (auto *item : getItems()) {
		if (!item->boundingRect().translated(item->scenePos()).contains(clickPoint))
			continue;
		auto p1 = std::make_pair(item->m_inputPoints, false);
		auto p2 = std::make_pair(item->m_outputPoints, true);
		auto ps = {p1, p2};

		int cpIndex = -1;
		bool cpIsOutput = false;
		Direction cpDirection = Direction::LEFT;

		bool done = false;
        for (const auto& p : ps) {
			auto points = p.first;
			auto isOutput = p.second;
			for (auto i = 0; i < points.size(); i++) {
				if (points[i].getBounds().translated(item->scenePos()).contains(clickPoint)) {
					cpIndex = i;
					//                    std::cerr << "\t\t -- source clash debug, adding cp direction " <<
					//                    points[i].getDirection() << " for comp " <<
					//                    item->m_component->short_identify() << std::endl;
					cpDirection = points[i].getDirection();
					cpIsOutput = isOutput;
					done = true;
				}
			}
			if (done)
				break;
		}
		res.emplace_back(item, cpIndex, cpIsOutput, cpDirection);
	}

	return res;
}

void Scene::resizeItemInputs(unsigned n)
{
	if (!selectedItems().empty()) {
		// only one item at a time can be selected (for now)
        static_cast<ComponentGraphicsItem *>(selectedItems().constFirst())->safeResize(n);
		update();
	}
}

void Scene::serialize(std::ostream &os) const
{
	m_cb.serializeBoard(os);
	int nItems = items().size();
	os.write((char *)(&nItems), sizeof(nItems));
	for (auto &item : items()) {
		static_cast<ComponentGraphicsItem *>(item)->serialize(os);
	}
	m_changed = false;
}

void Scene::deserialize(std::istream &is)
{
	m_cb.deserializeBoard(is);
	int nItems;
	is.read((char *)(&nItems), sizeof(nItems));
	for (int i = 0; i < nItems; i++) {
		int id;
		is.read((char *)(&id), sizeof(id));
		Component *c = m_cb.get(id);
		ComponentGraphicsItem *item = ItemFactory::Instance().make(c);
		item->deserialize(is);
		addItem(item);
	}
	m_changed = false;
}

void Scene::reset()
{
	clear();
	update();
	resetDrawInfo();
	m_cb.removeAll();
	m_changed = false;
}

ControlMode Scene::controlMode()
{
	return m_controlMode;
}

void Scene::setControlMode(const ControlMode &new_controlMode)
{
	if (m_controlMode == new_controlMode) {
		std::cerr << "You are already in " << m_controlMode << std::endl;
		return;
	}

	if (m_controlMode == ControlMode::DESIGN || new_controlMode == ControlMode::DESIGN) {
		resetDrawInfo();
	}
	if (m_controlMode == ControlMode::SELECTION /* && selection not empty*/) {
		resetSelectInfo();
	}

	std::cerr << "Switching from " << m_controlMode << " to " << new_controlMode << std::endl;
	m_controlMode = new_controlMode;
}

QRectF Scene::getManualSelectionRect() const
{
	auto minx = std::min(m_startPoint.x(), m_endPoint.x());
	auto miny = std::min(m_startPoint.y(), m_endPoint.y());
	auto maxx = std::max(m_startPoint.x(), m_endPoint.x());
	auto maxy = std::max(m_startPoint.y(), m_endPoint.y());
	auto topLeft = QPointF(minx, miny);
	auto botRight = QPointF(maxx, maxy);
	return QRectF(topLeft, botRight);
}

void Scene::clearSelection()
{
	const auto *tag = "[SCENE][SELECTION]";
	if (m_select_info.selection.empty())
		return;
	QPointF topLeft = QPointF(std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
	QPointF botRight = QPointF(std::numeric_limits<int>::min(), std::numeric_limits<int>::min());
	for (auto *sitem : m_select_info.selection) {
		auto *item = static_cast<ComponentGraphicsItem *>(sitem);
		auto x = item->scenePos().x();
		auto y = item->scenePos().y();
		if (item->m_isInSelection) {
			std::cerr << "\t\t" << tag << " Clearing selection from " << item->m_component->short_identify()
					  << std::endl;
			item->m_isInSelection = false;
			topLeft.setX(std::min(topLeft.x(), x));
			topLeft.setY(std::min(topLeft.y(), y));
			botRight.setX(std::max(botRight.x(), x));
			botRight.setY(std::max(botRight.y(), y));
			item->m_isInSelection = false;
		}
	}
	m_select_info.selection.clear();
	update(QRectF(topLeft, botRight));
	//    update();
}

std::vector<ComponentGraphicsItem *> Scene::getItems()
{
	auto itms = items();
	std::vector<ComponentGraphicsItem *> result;
	std::transform(std::begin(itms), std::end(itms), std::back_inserter(result), [](auto it) {
		return static_cast<ComponentGraphicsItem *>(it);
	});
	return result;
}

void Scene::tick()
{
	// TODO Keep a set of all clocks for faster iteration
	bool hasClock = false;
	for (auto pair : m_cb.getAll()) {
		if (pair.second->getType() == ComponentTypes::CLOCK) {
			hasClock = true;
			static_cast<Clock *>(pair.second)->tick();
		}
	}
	if (hasClock)
		update();
}

void Scene::addComponent(ComponentGraphicsItem *item)
{
	addItem(item);
	m_cb.add(item->m_component);
	m_changed = true;
}

void Scene::setChanged(bool flag)
{
	m_changed = flag;
}

bool Scene::isChanged() const
{
	return m_changed;
}

MainWindow *Scene::getMainWindow()
{
	return mw;
}
