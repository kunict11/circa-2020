#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>

#include <QCloseEvent>
#include <QComboBox>
#include <QDir>
#include <QFileDialog>
#include <QListWidget>
#include <QMainWindow>
#include <QRectF>

#include "component/componentfactory.hpp"
#include "scene.hpp"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow() override;

	void closeEvent(QCloseEvent *event) override;
	QRectF getVisibleSceneArea();

private slots:
	void on_actionMakeComposite_triggered();

	void on_actionExit_triggered();

	void on_actionMinimize_triggered();

	void on_actionMaximize_triggered();

	void on_actionClose_2_triggered();

	void on_actionExport_Image_triggered();

	void on_actionNew_triggered();
	void on_actionSave_triggered();
	void on_actionOpen_triggered();

    void on_select_box_activated(const QString &arg1);

	void on_actionGet_Logic_Expression_triggered();

    void on_comboBox_activated(const QString &arg1);

	void toggleBox(bool isScenePressed, const QString &boxName, unsigned input_size, int direction);

private:
	void setupLists(QListWidget *list);
	Ui::MainWindow *ui;
	Scene *scene;

	bool offerSave();
	bool openSaveDialog();
	void saveBoard(const std::string &path);

	bool m_isLoaded = false;
	std::string m_path;

protected:
	void keyPressEvent(QKeyEvent *event) override;
	void keyReleaseEvent(QKeyEvent *event) override;
};
#endif // MAINWINDOW_H
