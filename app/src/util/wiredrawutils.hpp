#ifndef WIREDRAWUTILS_H
#define WIREDRAWUTILS_H

#include <QRectF>

#include "util/direction.hpp"
#include "util/wiredrawinfo.hpp"

#define WIRE_DRAW_LOGGING

class Scene;
class Wire;
class WireItem;
class Component;
class ConnectionPoint;
class QPointF;

class WireDrawUtils
{
public:
	static std::tuple<int, int, int, int>
	getWireItemPosWH(const QPointF &start_point, const QPointF &scene_end_point, const Direction &direction);
	static QRectF getPotentialWireItemOverlapBoundsSc(
		const QPointF &start_point, const QPointF &scene_end_point, const Direction &direction);
	static QRectF getPotentialItemOverlapBoundsSc(int posx, int posy, int width, int height);

	static WireItem *
	makeWireItem(const QPointF &start_point, const QPointF &scene_end_point, const Direction &direction, Wire *w);
	static WireItem *makeWireItem(const WireDrawInfo &info, const QPointF &scene_end_point, Wire *w);
	static void makeWireItemAndAdd(Scene *scene, Wire *w);

	static void singleSourceToWire(const WireDrawInfo &info, Wire *w);
	static void multiSourceToWire(const WireDrawInfo &info, Wire *w);
	static void sourceToWire(const WireDrawInfo &info, Wire *w);
	static void wireToTarget(
		Wire *w, const Direction &wireDirection, const std::vector<Component *> &target_components,
		const std::vector<Direction> &target_directions, const std::vector<int> &target_indicies,
		const std::vector<bool> &target_is_output);
};

#endif // WIREDRAWUTILS_H
