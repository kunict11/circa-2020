#ifndef COLORUTIL_H
#define COLORUTIL_H

#include <QColor>

#include "connection/connectionsignal.hpp"

class ColorUtil
{
public:
	static const QColor UNDEFINED;
	static const QColor ERROR;
	static const QColor FALSE;
	static const QColor TRUE;

	static QColor forState(Signal::State state);

private:
	ColorUtil();
};

#endif // COLORUTIL_H
