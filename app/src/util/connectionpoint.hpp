#ifndef CONNECTIONPOINT_H
#define CONNECTIONPOINT_H

#include <QPointF>
#include <QRectF>

#include "scene.hpp"
#include "util/direction.hpp"

class ConnectionPoint
{
public:
	ConnectionPoint();
    ConnectionPoint(int x, int y, Direction direction);
    ConnectionPoint(int x, int y, Direction direction, QString label);
    ConnectionPoint(int x, int y, unsigned size, Direction direction);
    ConnectionPoint(int x, int y, unsigned size, Direction direction, QString label);

	QPointF getCenter() const;
	QRectF getBounds() const;
    Direction getDirection() const;
	bool hasFocus() const;
	void setFocus(bool focus);
	unsigned getSize() const;
    QString getLabel() const;

private:
	bool m_focus;
	QPointF m_center;
	QRectF m_bounds;
    Direction m_direction;
    QString m_label;

	unsigned m_size;
};

#endif // CONNECTIONPOINT_H
