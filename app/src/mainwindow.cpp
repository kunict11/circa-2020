#include "mainwindow.hpp"

#include <fstream>

#include <QInputDialog>
#include <QMessageBox>

#include "formula/converter.hpp"
#include "formuladialog.hpp"
#include "item/componentgraphicsitem.hpp"
#include "item/compositeitem.hpp"
#include "item/itemfactory.hpp"
#include "ui_mainwindow.h"
#include "util/direction.hpp"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	scene = new Scene(this);
	scene->mw = this;
	ui->graphicsView->setRenderHint(QPainter::Antialiasing);
	ui->graphicsView->setScene(scene);
	ui->graphicsView->setSceneRect(0, 0, ui->graphicsView->width(), ui->graphicsView->height());
	// ui->graphicsView->setSceneRect(QRectF(0, 0, 1000, 1000));
	ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAsNeeded);
	ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAsNeeded);
	setupLists(ui->gates_list);
	setupLists(ui->wiring_list);
	setupLists(ui->plexers_list);
	setupLists(ui->arithmetic_list);
	setupLists(ui->memory_list);
	QApplication::setWindowIcon(QIcon(":/img/icon.png"));

	ui->select_box->setStyleSheet("combobox-popup: 0;");
	ui->comboBox->setStyleSheet("combobox-popup: 0;");
	ui->input_size_selector->hide();
	ui->toolBox->setCurrentIndex(0);
    ui->orientation_selector->hide();

	QObject::connect(scene, &Scene::toggleBox, this, &MainWindow::toggleBox);
}

MainWindow::~MainWindow()
{
	delete ui;
	delete scene;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if (offerSave())
		event->accept();
	else
		event->ignore();
}

QRectF MainWindow::getVisibleSceneArea()
{
	QRect viewport_rect(0, 0, ui->graphicsView->viewport()->width(), ui->graphicsView->viewport()->height());
	QRectF visible_scene_rect = ui->graphicsView->mapToScene(viewport_rect).boundingRect();
	return visible_scene_rect;
}

void MainWindow::on_actionExit_triggered()
{
	MainWindow::close();
}

void MainWindow::on_actionMinimize_triggered()
{
	MainWindow::showMinimized();
}

void MainWindow::on_actionMaximize_triggered()
{
	MainWindow::showMaximized();
}

void MainWindow::on_actionClose_2_triggered()
{
	if (offerSave())
		MainWindow::close();
}

void MainWindow::setupLists(QListWidget *list)
{
	list->setAcceptDrops(false);
	for (int i = 0; i < list->count(); i++) {
		QListWidgetItem *item = list->item(i);

		auto text = item->text();
		auto name = text.split(" ").at(0);

		item->setWhatsThis(name);
	}
}

void MainWindow::toggleBox(
	bool isScenePressed, const QString &boxName, const unsigned input_size, const int direction)
{
	QWidget *box;
	if (boxName == "input") {
		box = ui->input_size_selector;
	} else if (boxName == "orientation") {
		box = ui->orientation_selector;
	} else {
		return;
	}

	if (isScenePressed) {
		if (!box->isHidden())
			box->hide();
		else
			return;
	} else {
		if (input_size >= 2)
			ui->select_box->setCurrentIndex(input_size - 2);
		ui->comboBox->setCurrentIndex(direction);

		if (box->isHidden()) {
			box->show();
			box->setEnabled(true);
		} else
			return;
	}
}

void MainWindow::on_actionSave_triggered()
{
	if (m_isLoaded) {
		saveBoard(m_path);
	} else {
		openSaveDialog();
	}
}

void MainWindow::on_actionNew_triggered()
{
	if (offerSave()) {
		m_isLoaded = false;
		setWindowTitle(QString("Circa2020"));
		scene->reset();
	}
}

void MainWindow::on_actionOpen_triggered()
{
	if (offerSave()) {
		QString fileName = QFileDialog::getOpenFileName(
			this, tr("Open circuit board"), QDir::currentPath(), tr("Circuit Board (*.cb)"));
		if (fileName.isEmpty())
			return;
		else {
			m_isLoaded = true;
			m_path = fileName.toStdString();
			setWindowTitle(QString::fromStdString(m_path + " - Circa2020"));

			scene->reset();
			std::ifstream is;
			is.open(fileName.toStdString(), std::ios::in | std::ios::binary);
			scene->deserialize(is);
			is.close();
		}
	}
}

bool MainWindow::offerSave()
{
	// Only open if scene changed
	if (!scene->isChanged() || (!m_isLoaded && scene->items().empty()))
		return true;
	QMessageBox::StandardButton response = QMessageBox::question(
		this, "Save", "Do you want to save this file?",
		QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel, QMessageBox::Cancel);
	if (response == QMessageBox::Save) {
		if (m_isLoaded) {
			saveBoard(m_path);
		} else {
			return openSaveDialog();
		}
		return true;
	} else if (response == QMessageBox::Discard) {
		return true;
	}
	return false;
}

bool MainWindow::openSaveDialog()
{
	QString defaultPath = QDir::currentPath() + "/untitled.cb";

	QString fileName =
		QFileDialog::getSaveFileName(this, tr("Save circuit board"), defaultPath, tr("Circuit Board (*.cb)"));
	if (fileName.isEmpty())
		return false;
	else {
		saveBoard(fileName.toStdString());
		return true;
	}
}

void MainWindow::saveBoard(const std::string &path)
{
	std::ofstream os;
	os.open(path, std::ios::out | std::ios::binary);
	scene->serialize(os);
	os.close();

	m_isLoaded = true;
	m_path = path;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
	scene->keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
	scene->keyReleaseEvent(event);
}

void MainWindow::on_actionExport_Image_triggered()
{
	QString defaultPath = QDir::currentPath() + "/untitled.png";

	QString imgPath = QFileDialog::getSaveFileName(
		this, tr("Export Image"), defaultPath, tr("JPEG (*.jpeg, *.jpg);;PNG (*.png)"));
	const QPixmap pm = ui->graphicsView->grab();

	if (!imgPath.isNull())
		pm.save(imgPath);
}

void MainWindow::on_select_box_activated(const QString &arg1)
{
	scene->resizeItemInputs(arg1.toUInt());
}

void MainWindow::on_actionMakeComposite_triggered()
{
	if (scene->items().empty()) {
		QMessageBox::information(this, "Composite component", "There are no components.");
		return;
	}

	QString name;
	while (true) {
		bool ok;
		name = QInputDialog::getText(
			this, tr("Composite component"), tr("Component name (max 15 characters)"), QLineEdit::Normal,
			"Composite", &ok);
		if (!ok) {
			return;
		}
		if (name.size() >= 1 && name.size() <= 15)
			break;
	}

	std::vector<ComponentGraphicsItem *> itemsForComposite;
	if (scene->controlMode() == ControlMode::SELECTION && !scene->m_select_info.selection.empty()) {
		std::copy(
			std::begin(scene->m_select_info.selection), std::end(scene->m_select_info.selection),
			std::back_inserter(itemsForComposite));
	} else {
		auto items = scene->getItems();
		std::copy(std::begin(items), std::end(items), std::back_inserter(itemsForComposite));
	}

	int pinCount = 0;
	int lbCount = 0;
	for (auto *p : itemsForComposite) {
		if (p->m_component->getType() == ComponentTypes::PIN)
			pinCount++;
		else if (p->m_component->getType() == ComponentTypes::LIGHTBULB)
			lbCount++;
	}
	if (pinCount == 0 || lbCount == 0) {
		QMessageBox::information(
			this, "Composite component", "The component must have at least one pin and at least on lightbulb.");
		return;
	}

	std::sort(std::begin(itemsForComposite), std::end(itemsForComposite), MiscUtils::ItemSortPredicate);

	std::vector<Component *> componentsForComposite;
	std::transform(
		std::begin(itemsForComposite), std::end(itemsForComposite), std::back_inserter(componentsForComposite),
		[](ComponentGraphicsItem *item) { return item->m_component; });

	CompositeComponent *composite = ComponentFactory::instance().makeCompositeFromSelection(
		std::begin(componentsForComposite), std::end(componentsForComposite));

	auto *compositeItem = new CompositeItem(composite, 15, 15, name.toStdString());

	auto itemPosition = MiscUtils::getClosestAvailablePosition(scene, compositeItem->overlapBoundingRectSc());
	if (!itemPosition) {
		delete composite;
		delete compositeItem;

		QMessageBox::critical(this, "Composite component", "Couldn't find a free spot on the board.");
		return;
	}

	compositeItem->setPos(itemPosition.value());
	scene->addComponent(compositeItem);
}

void MainWindow::on_actionGet_Logic_Expression_triggered()
{
	auto expressionTrees = Converter::toExpressionTrees(scene->m_cb);
	if (expressionTrees) {
		if (expressionTrees.value().empty()) {
			QMessageBox::information(this, "Formula", "There are no lightbulbs");
			return;
		}

		auto expressionFormulae = Converter::toFormulae(expressionTrees.value());
		auto simplifiedTrees = Converter::simplifyTrees(expressionTrees.value());
		auto simplifiedFormulae = Converter::toFormulae(simplifiedTrees);

		for (auto *tree : expressionTrees.value())
			delete tree;
		for (auto *tree : simplifiedTrees)
			delete tree;

		std::ostringstream os;
		os << "direct:" << std::endl;
        for (const auto& formula : expressionFormulae)
			os << formula << std::endl;
		os << "simplified:" << std::endl;
        for (const auto& formula : simplifiedFormulae)
			os << formula << std::endl;

		auto *dialog = new FormulaDialog(os.str());
		dialog->setModal(true);
		dialog->exec();
		delete dialog;
	} else {
		QMessageBox::critical(this, "Error", "This circuit does not support formula conversion");
	}
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
	if (arg1 == "EAST") {
		scene->changeOrientation(Direction::RIGHT);
	} else if (arg1 == "SOUTH") {
		scene->changeOrientation(Direction::DOWN);
	} else if (arg1 == "WEST") {
		scene->changeOrientation(Direction::LEFT);
	} else {
		scene->changeOrientation(Direction::UP);
	}
}
